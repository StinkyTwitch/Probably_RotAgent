ProbablyEngine.rotation.register_custom(260, "RA - Combat", {

  --------------------
  -- Start Rotation --
  --------------------
  { "pause", { "@RA.Pause()", }, },

  -- Buffs
  --{ "2823", { "!lastcast(2823)", "!player.buff(2823)" }}, -- Deadly Poison (player lvl < 98)
  { "2823", "!player.buff(157584)" }, --Instant Poison CastID + BuffID

  --{ "/targetenemy [noexists]", "!target.exists" },
  --{ "/targetenemy [dead]", { "target.exists", "target.dead"}},
  { "#5512", "player.health <= 30" }, -- Healthstone
  { "#109223", "player.health < 20" }, -- Health Potion
  { "74001", "player.health <= 50" }, -- Combat Readiness
  { "73651", { "player.health < 70", "player.combopoints >= 3", "!player.buff(73651)", "!player.party" }}, -- Recuperate

  -- Interrupts
  { "1766", "target.interruptAt(61)" }, -- Kick

  -- Cooldowns
  {{
  { "13750", "player.energy <= 40" }, -- Adrenaline Rush
  { "1856", { "toggle.vanish", "target.boss", "!player.buff(51690)", "player.buff(5171).duration >= 10", "player.buff(84747)" }}, -- Vanish
  { "#trinket1", {"target.range <= 5", "target.health.actual > 100000", "!target.movingfor > 1" }},
  }, "modifier.cooldowns" },

  --Hotkey bindings
  { "31224", "modifier.rshift" }, -- Cloak of Shadows
  { "1966", "modifier.rcontrol" }, -- Feint
  { "76577", "modifier.ralt" }, -- Smoke Bomb
  { "51690", "modifier.lalt" }, -- Killing Spree

  -- Blade Flurry
  { "13877", { "player.area(10).enemies >= 2", "!player.buff(13877)", "modifier.multitarget" }},
  { "13877", { "player.area(10).enemies < 2", "player.buff(13877)" }},

  -- Crimson Tempest
  { "121411", { "player.area(10).enemies >= 2", "player.combopoints = 5", "!target.debuff(122233)", "modifier.multitarget" }},
  { "121411", { "player.area(10).enemies >= 7", "player.combopoints = 5", "modifier.multitarget" }},

  -- Rotation
  { "137619", { "talent(6,2)", "target.range <= 5" }}, -- Marked for Death  "player.combopoints = 0",
    { "5171", { "player.buff(5171).duration <= 3", "player.combopoints >= 3" }}, -- Slice and Dice
  { "8676", { "player.behind" }}, -- Ambush

  { "84617", "target.debuff(84617).duration <= 6" }, -- Revealing Strike

  --{ "121411", { "player.combopoints = 5", "toggle.crimsontempest" }}, -- Crimson Tempest

  { "1752", {    -- Pools combo points if in Moderate Insight to reach Deep Insight
    "player.buff(84746)",
    "player.spell(114015).exists", --Anticipation
    "player.buff(115189).count < 4", --Anticipation buff
  }},

  { "2098", { -- Eviscerate
    "player.combopoints = 5",
    "player.buff(84746)", -- Moderate Insight
    "player.spell(114015).exists", --Anticipation
    "player.buff(115189).count = 5", --Anticipation buff
  }},

  { "2098", { -- Eviscerate
    "player.combopoints = 5",
    "!player.buff(84746)", -- Moderate Insight
  }},

  { "1752" }, -- Sinister Strike

  ------------------
  -- End Rotation --
  ------------------

},{

  ---------------
  -- OOC Begin --
  ---------------

  --{ "2823", { "!lastcast(2823)", "!player.buff(2823)" }}, -- Deadly Poison (player lvl < 98)
  { "2823", "!player.buff(157584)" }, --Instant Poison CastID + BuffID
  { "73651", { "player.health < 90", "!player.buff(73651)" }}, -- Recuperate

  -------------
  -- OOC End --
  -------------
},
function()
    RA.PEOverloads()
    RA.BaseStatsInit()
    RA.HunterPetSlots()

    -- Splash Logo
    RA.SplashInit()

    function RA.OptionsWindowRCOMBATBuild()
        RA.optionsWindowRCOMBAT = ProbablyEngine.interface.buildGUI({
            key = 'ra_config_r_combat',
            title = 'RotAgent',
            subtitle = 'Combat',
            profiles = true,
            width = 275,
            height = 500,
            color = "4e7300",
            config = {
                { type = 'rule', },
                { type = 'header', text = 'Basics', },
                { type = 'rule', },

                { type = 'dropdown', key = 'combatrotation', text = 'Rotation Logic', list = {
                    { key = 'svs', text = 'Svs' },
                }, default = 'svs', },
                { type = 'checkbox', key = 'autotarget', text = 'Auto Target Logic', default = true, },
                { type = 'dropdown', key = 'autotargetalgorithm', text = 'Auto Target Algorithm', list = {
                    { key = 'highest', text = 'Highest HP' },
                    { key = 'lowest', text = 'Lowest HP' },
                    { key = 'nearest', text = 'Nearest' },
                    { key = 'cascade', text = 'Cascade' },
                }, default = 'highest', },
                { type = 'checkbox', key = 'autocleartarget', text = 'Auto Clear Target if not Enemy Unit.', default = true, },
                { type = 'checkbox', key = 'agipotion', text = 'Agility Potion Logic', default = false, },
                { type = 'checkbox', key = 'bosslogic', text = 'Boss Logic', default = true, },
                { type = 'checkbox', key = 'defensives', text = 'Defensive Logic', default = true, },

                { type = 'checkbox', key = 'pause', text = 'Pause on Keybind', default = true, },

                { type = 'checkbox', key = 'noexecute', text = 'No raexecute() register', default = false, },
                { type = 'checkbox', key = 'noprioritize', text = 'No raprioritize() register', default = false, },

                { type = 'spinner', key = 'executevalue', text = 'Execute range percentage |cffaaaaaaHP < %|r', default = 35, },

                { type = 'rule', },
                { type = 'header', text = 'Advanced',},
                { type = 'rule', },

                { type = 'header', text = 'Boss Logic' },
                { text = "", },
                { type = 'rule', },

                { type = 'header', text = 'Defensive Logic' },
                { type = 'spinner', key = 'combatreadiness', text = 'Combat Readiness at |cffaaaaaaHP < %|r', default = 60, },
                { type = 'spinner', key = 'healthpot', text = 'Health Potion/Stone at |cffaaaaaaHP < %|r', default = 40, },
                { type = 'spinner', key = 'recuperate', text = 'Recuperate at |cffaaaaaaHP < %|r', default = 10 },
                { type = 'rule' },

                { type = 'header', text = 'Miscellaneous' },
                { text = "", },
                { type = 'rule', },

                { type = 'header', text = 'Tricks of the Trade' },
                { type = 'spinner', key = 'tricksfocusagro', text = 'TotT to Focus at |cffaaaaaaX% aggro|r', default = 50, },
                { type = 'dropdown', key = 'trickskeybind', text = 'TotT Keybind', list = {
                    { key = 'lalt', text = 'lalt' },
                    { key = 'lcontrol', text = 'lcontrol' },
                    { key = 'lshift', text = 'lshift' },
                    { key = 'ralt', text = 'ralt' },
                    { key = 'rcontrol', text = 'rcontrol' },
                    { key = 'rshift', text = 'rshift' },
                }, default = 'lalt', desc = 'Tricks of the Trade to Focus Target, if no Focus TotT to Mouseover Target', },
                { type = 'rule' },

                { type = 'header', text = 'Pause',},
                { type = 'dropdown', key = 'pausekeybind', text = 'Pause Keybind', list = {
                    { key = 'lalt', text = 'lalt' },
                    { key = 'lcontrol', text = 'lcontrol' },
                    { key = 'lshift', text = 'lshift' },
                    { key = 'ralt', text = 'ralt' },
                    { key = 'rcontrol', text = 'rcontrol' },
                    { key = 'rshift', text = 'rshift' },
                }, default = 'lshift' },
                { type = 'rule', },

                { type = 'header', text = 'Pool Energy',},
                { type = 'dropdown', key = 'poolenergykeybind', text = 'Energy pooling Keybind', list = {
                    { key = 'lalt', text = 'lalt' },
                    { key = 'lcontrol', text = 'lcontrol' },
                    { key = 'lshift', text = 'lshift' },
                    { key = 'ralt', text = 'ralt' },
                    { key = 'rcontrol', text = 'rcontrol' },
                    { key = 'rshift', text = 'rshift' },
                }, default = 'rcontrol' },
                { type = 'rule' },

                { type = 'header', text = 'Debug', },
                { type = 'checkspin', key = 'debug', text = 'Debug output, set time interval to the right.', min = 0, max = 5, step = .1, default = 0, },
                { type = 'rule' },

                { type = 'header', text = 'Extra' },
                { type = 'checkbox', key = 'autolfg', text = 'Auto LFG Accept', default = false, },
                { type = "checkbox", key = 'splash', text = "Splash Image", default = true, },
            }
        })
        RA.optionsWindowRCOMBAT.parent:Hide()
    end
    RA.OptionsWindowRCOMBATBuild()


    ProbablyEngine.toggle.create(
        'vanish', 'Interface\\Icons\\ability_vanish',
        'Vanish','Enable or Disable use of Vanish'
    )

    --[[
    ProbablyEngine.toggle.create(
        'crimsontempest', 'Interface\\Icons\\inv_knife_1h_cataclysm_c_05',
        'Crimson Tempest','Enable or Disable use of Crimson Tempest AOE'
    )
    ]]

end)