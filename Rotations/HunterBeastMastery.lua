--[[------------------------------------------------------------------------------------------------

HunterBeastMastery.lua

RotAgent (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

------------------------------------------------------------------------------------------------]]--


----------------------------------------------------------------------------------------------------
--  LOCAL VARIABLS/TABLES                                                                         --
----------------------------------------------------------------------------------------------------
local defensives = RA.hunterDefensives
local miscellaneousCombat = RA.hunterMiscellaneousCombat
local miscellaneousOOC = RA.hunterMiscellaneousOOC
local misdirection = RA.hunterMisdirection
local petAttack = RA.hunterPetAttack
local petManagementCombat = RA.hunterPetManagementCombat
local petManagementOOC = RA.hunterPetManagementOOC
local petSummon = RA.hunterPetSummon
local poolFocusCobraShot = RA.hunterPoolFocusCobraShot
local queueSpells = RA.hunterQueueSpellsBM


----------------------------------------------------------------------------------------------------
--  ROTATION OPENERS                                                                              --
----------------------------------------------------------------------------------------------------
local openerPackNoCDs = {
	{ {
		{ misdirection, { "!player.party", function() return RA.Fetch("misdirects", true) == true end, }, },
		{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
		{ "Bestial Wrath", },
		{ "Multi-Shot", {
			function() return RA.Fetch("nocleave", false) == false end,
			"!target.raccinarea(8)",
			"!pet.buff(Beast Cleave)",
		}, "pettarget", },
		{ "Kill Command", nil, "pettarget", },
		{ "Barrage", {
			function() return RA.Fetch("nobarrage", false) == false end,
			function() return RA.Fetch("nocleave", false) == false end,
			"!target.raccinarea(20)",
			"target.deathin > 10",
		}, },
	}, { "!player.casting", "!player.channeling", }, },
}

local openerPackCDs = {
	{ {
		{ misdirection, { "!player.party", function() return RA.Fetch("misdirects", true) == true end, }, },
		{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
		{ "Stampede", { "talent(5,3)", }, },
		{ {
			{ "#trinket1", },
			{ "#trinket2", },
			{ "Blood Fury", },
			{ "Berserking", },
			{ "Bestial Wrath", },
			{ "Multi-Shot", {
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(8)",
				"!pet.buff(Beast Cleave)",
			}, "pettarget", },
			{ "Kill Command", nil, "pettarget", },
			{ "Barrage", {
				function() return RA.Fetch("nobarrage", false) == false end,
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(20)",
				"modifier.multigarget",
				"target.deathin > 10",
			}, },
		}, "player.spell(Stampede).cooldown >= 260", },
	}, { "!player.casting", "!player.channeling", }, },
}

local openerNoCDs = {
	{ {
		{ misdirection, { "!player.party", function() return RA.Fetch("misdirects", true) == true end, }, },
		-- Steady Focus --
		{{
			{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
			{ "Cobra Shot", "!player.buff(Steady Focus)", },
			{ {
				{ "Bestial Wrath", },
				{ "Kill Command", nil, "pettarget", },
				{ "Barrage", {
					function() return RA.Fetch("nobarrage", false) == false end,
					function() return RA.Fetch("nocleave", false) == false end,
					"!target.ccinarea(20)",
					"modifier.multigarget",
					"target.deathin > 10",
				}, },
			}, "player.buff(Steady Focus)", },
		}, "talent(4,1)", },
		-- Dire Beast --
		{{
			{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
			{ "Dire Beast", },
			{ "Bestial Wrath", },
			{ "Kill Command", nil, "pettarget", },
			{ "Barrage", {
				function() return RA.Fetch("nobarrage", false) == false end,
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(20)",
				"modifier.multigarget",
				"target.deathin > 10",
			}, },
			{ "Cobra Shot", },
		}, "talent(4,2)", },
	}, { "!player.casting", "!player.channeling", }, },
}

local openerCDs = {
	{ {
		{ misdirection, { "!player.party", function() return RA.Fetch("misdirects", true) == true end, }, },
		-- Steady Focus --
		{{
			{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
			{ "Cobra Shot", "!player.buff(Steady Focus)", },
			{ "Stampede", },
			{ {
				{ "#trinket1", },
				{ "#trinket2", },
				{ "Blood Fury", },
				{ "Berserking", },
				{ "Bestial Wrath", },
				{ "Kill Command", nil, "pettarget", },
				{ "Barrage", {
					function() return RA.Fetch("nobarrage", false) == false end,
					function() return RA.Fetch("nocleave", false) == false end,
					"!target.raccinarea(20)",
					"modifier.multigarget",
					"target.deathin > 10",
				}, },
			}, "player.spell(Stampede).cooldown >= 260", "player.buff(Steady Focus)", },
		}, "talent(4,1)", },
		-- Dire Beast --
		{{
			{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
			{ "Stampede", },
			{ {
				{ "#trinket1", },
				{ "#trinket2", },
				{ "Blood Fury", },
				{ "Berserking", },
				{ "Dire Beast", },
				{ "Bestial Wrath", },
				{ "Kill Command", nil, "pettarget", },
				{ "Barrage", {
					function() return RA.Fetch("nobarrage", false) == false end,
					function() return RA.Fetch("nocleave", false) == false end,
					"!target.raccinarea(20)",
					"modifier.multigarget",
					"target.deathin > 10",
				}, },
			}, "player.spell(Stampede).cooldown >= 260", "player.buff(Steady Focus)", },
		}, "talent(4,2)", },
	}, { "!player.casting", "!player.channeling", }, },
}


----------------------------------------------------------------------------------------------------
--  ROTATION                                                                                      --
----------------------------------------------------------------------------------------------------
ProbablyEngine.rotation.register_custom(253, "RA - Beast Mastery",
-- Combat
{
	-- Pause ---------------------------------------------------------------------------------------
	{ "pause", { "@RA.Pause()" }, },

	{ "/cancelaura " .. select(1, GetSpellInfo(5118)), {
		function() return RA.Fetch("aspectincombat", true) == false end,
		"player.buff(Aspect of the Cheetah)",
		"!player.glyph(Glyph of Aspect of the Cheetah)",
	}, },
	{ queueSpells, },


	-- Openers -------------------------------------------------------------------------------------
	{ {
		{ openerPackNoCDs, { "!modifier.cooldowns", "target.raarea(10).enemies >= 2", }, },
		{ openerPackCDs, { "modifier.cooldowns", "target.raarea(10).enemies >= 2", }, },
		{ openerNoCDs, { "!modifier.cooldowns", "target.raarea(10).enemies = 1", }, },
		{ openerCDs, { "modifier.cooldowns", "target.raarea(10).enemies = 1", }, },
	}, "player.time < 5", },


	-- Rotation ------------------------------------------------------------------------------------
	{ {
		{ defensives, function() return RA.Fetch("defensives", true) == true end, },
		{ miscellaneousCombat, },
		{ misdirection, function() return RA.Fetch("misdirects", true) == true end, },
		{ petAttack, function() return RA.Fetch("petmanagement", true) == true end, },
		{ petManagementCombat, { function() return RA.Fetch("petmanagement", true) == true end, }, },
		{ poolFocusCobraShot, },


		-- Racials and Trinkets --------------------------------------------------------------------
		{ {
			{ "Arcane Torrent", { "player.rafocus.deficit >= 30", }, },

			{ "Blood Fury", { "player.spell(Stampede).cooldown = 0", }, },
			{ "Blood Fury", { "player.spell(Stampede).cooldown > 120", }, },

			{ "Berserking", { "player.spell(Stampede).cooldown = 0", }, },
			{ "Berserking", { "player.spell(Stampede).cooldown > 120", }, },

			{ "#trinket1", { "player.buff(Berserking)", }, },
			{ "#trinket1", { "player.buff(Bestial Wrath)", }, },
			{ "#trinket1", { "player.buff(Blood Fury)", }, },
			{ "#trinket1", { "player.buff(Focus Fire)", }, },

			{ "#trinket2", { "player.buff(Berserking)", }, },
			{ "#trinket2", { "player.buff(Bestial Wrath)", }, },
			{ "#trinket2", { "player.buff(Blood Fury)", }, },
			{ "#trinket2", { "player.buff(Focus Fire)", }, },
		}, "modifier.cooldowns", },


		-- Agility Potion --------------------------------------------------------------------------
		{ {
			{ "#109217", {
				"!talent(5,3)",
				"player.buff(Bestial Wrath)",
				"target.health <= 20",
			}, },
			{ "#109217", {
				"talent(5,3)",
				"player.spell(Stampede).cooldown >= 260",
				"player.rabursthaste",
			}, },
			{ "#109217", {
				"talent(5,3)",
				"player.spell(Stampede).cooldown >= 260",
				"player.buff(Focus Fire)",
			}, },
			{ "#109217", {
				"target.deathin <= 25",
			}, },
		}, { "modifier.cooldowns", function() return RA.Fetch("agipotion", false) == true end, }, },


		-- Stampede ------------------------------------------------------------------------------------
		{ {
			{ "Stampede", {
				"player.rabursthaste",
			}, },
			{ "Stampede", {
				"player.buff(Frenzy).count >= 5",
			}, },
			{ "Stampede", {
				"player.buff(Focus Fire)",
			}, },
		}, { "!player.channeling", "modifier.cooldowns", "talent(5,3)",	}, },


		-- A Murder Of Crows ---------------------------------------------------------------------------
		{ {
			{ "A Murder of Crows", {
				"target.deathin > 60",
				"modifier.cooldowns",
			}, },
			{ "A Murder of Crows", {
				"target.deathin < 12",
			}, },
		}, "!player.channeling", "talent(5,1)", },


		-- Focus Fire ----------------------------------------------------------------------------------
		{ {
			{ "Focus Fire", {
				"player.buff(Frenzy).count = 5",
				"player.spell(Bestial Wrath).cooldown <= 10",
			}, },
			{ "Focus Fire", {
				"player.buff(Frenzy).count = 5",
				"player.spell(Bestial Wrath).cooldown >= 19",
			}, },
			{ "Focus Fire", {
				"player.buff(Frenzy).count >= 1",
				"player.buff(Bestial Wrath).duration >= 3",
			}, },
			{ "!Focus Fire", {
				"player.buff(Frenzy).count >= 1",
				"player.buff(Frenzy).duration <= 1",
			}, },
			{ "Focus Fire", {
				"pet.buff(Frenzy).count >= 1",
				"player.spell(Stampede).cooldown >= 260",
			}, },
			{ "Focus Fire", {
				"player.buff(Frenzy).count >= 1",
				"player.spell(Bestia lWrath).cooldown = 0",
				"!player.buff(Bestial Wrath)",
			}, },
		}, {
			"pet.exists",
			"!player.buff(Focus Fire)",
			"!player.channeling",
		}, },


		-- Bestial Wrath -------------------------------------------------------------------------------
		{ "!Bestial Wrath", {
			"!player.channeling",
			"player.buff(Steady Focus).duration > 4",
			"player.focus > 35",
			"!player.buff(Bestia lWrath)",
			"player.spell(Bestial Wrath).cooldown = 0",
			"player.spell(Kill Command).cooldown <= 2",
			"talent(4,1)",
		}, },
		{ "!Bestial Wrath", {
			"!player.channeling",
			"!player.buff(Bestial Wrath)",
			"player.spell(Bestial Wrath).cooldown = 0",
			"player.spell(Dire Beast).cooldown < 2",
			"player.spell(Kill Command).cooldown <= 2",
			"talent(4,2)",
		}, },


		-- Single Target -----------------------------------------------------------------------
		{ {
			-- Cobra Shot 2nd cast for Steady Focus --
			{ "Cobra Shot", {
				"!player.channeling",
				"talent(4,1)",
				"lastcast(Cobra Shot)",
				"player.buff(Steady Focus).duration < 3",
				"!player.buff(Bestial Wrath)",
			}, },

			{ "Kill Command", {
				"!player.channeling",
			}, "pettarget", },

			{ "!Kill Shot", {
				"!player.channeling",
				"raexecute(Kill Shot)",
			}, },

			{ "Dire Beast", {
				"!player.channeling",
				"talent(4,2)",
			}, },

			{ "Barrage", {
				function() return RA.Fetch('nobarrage', false) == false end,
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(20)",
				"modifier.multigarget",
				"target.deathin > 10",
			}, },

			-- Cobra Shot 1st cast for Steady Focus --
			{ "Cobra Shot", {
				"!player.channeling",
				"talent(4,1)",
				"player.buff(Steady Focus).duration < 3",
				"!player.buff(Bestial Wrath)",
			}, },

			{ "Arcane Shot", {
				"!player.channeling",
				"player.focus >= 70", },
			},

			{ "Cobra Shot", {
				"!player.channeling",
			}, },

		}, "target.raarea(10).enemies = 1", },


		-- Multi-Target --------------------------------------------------------------------------------
		{ {
			-- Cobra Shot 2nd cast for Steady Focus --
			{ "Cobra Shot", {
				"!player.channeling",
				"talent(4,1)",
				"lastcast(Cobra Shot)",
				"player.buff(Steady Focus).duration < 3",
				"!player.buff(Bestial Wrath)",
			}, },

			{ "Barrage", {
				function() return RA.Fetch('nobarrage', false) == false end,
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(20)",
				"modifier.multigarget",
				"target.deathin > 10",
			}, },

			{ "Multi-Shot", {
				function() return RA.Fetch("nocleave", false) == false end,
				"!player.channeling",
				"!pet.buff(Beast Cleave)",
				"!target.raccinarea(8)",
			}, "pettarget", },

			{ "Kill Command", {
				"!player.channeling",
			}, "pettarget", },

			{ "!Kill Shot", {
				"!player.channeling",
				"raexecute(Kill Shot)",
			}, },

			-- Cobra Shot 1st cast for Steady Focus --
			{ "Cobra Shot", {
				"!player.channeling",
				"talent(4,1)",
				"player.buff(Steady Focus).duration < 3",
				"!player.buff(Bestial Wrath)",
			}, },

			{ "Explosive Trap", {
				function() return RA.Fetch("nocleave", false) == false end,
				"!target.raccinarea(10)",
				"!player.channeling",
				"player.buff(Trap Launcher)",
			}, "target.ground", },

			{ "Arcane Shot", {
				"!player.channeling",
				"player.focus >= 70",
			}, },

			{ "Cobra Shot", {
				"!player.channeling",
			}, },

		}, "target.raarea(10).enemies >= 2", },
	}, "player.time > 5", },
},


-- Out of Combat
{
	{ queueSpells, },
	{ miscellaneousOOC, },
	{ petManagementOOC, function() return RA.Fetch("petmanagement", true) == true end, },
	{ petSummon, },
},


-- Callback
function()
	RA.PEOverloads()
	RA.BaseStatsInit()
	RA.HunterPetSlots()

	-- Splash Logo
	RA.SplashInit()

	function RA.OptionsWindowHBMBuild()
		RA.optionsWindowHBM = ProbablyEngine.interface.buildGUI({
			key = 'ra_config_h_bm',
			title = 'RotAgent',
			subtitle = 'Beast Mastery',
			profiles = true,
			width = 275,
			height = 500,
			color = "4e7300",
			config = {
				{ type = 'rule', },
				{ type = 'header', text = 'Basics', },
				{ type = 'rule', },

				{ type = 'checkbox', key = 'autotarget', text = 'Auto Target Logic', default = true, },
				{ type = 'dropdown', key = 'autotargetalgorithm', text = 'Auto Target Algorithm', list = {
					{ key = 'highest', text = 'Highest HP' },
					{ key = 'lowest', text = 'Lowest HP' },
					{ key = 'nearest', text = 'Nearest' },
					{ key = 'cascade', text = 'Cascade' },
				}, default = 'highest', },
				{ type = 'checkbox', key = 'autocleartarget', text = 'Auto Clear Target if not Enemy Unit.', default = true, },
				{ type = 'checkbox', key = 'autotraplauncher', text = 'Auto Trap Launcher Logic', default = true, },
				{ type = 'checkbox', key = 'autoaspect', text = 'Auto Aspect of the Cheetah', default = false, },
				{ type = 'checkbox', key = 'bosslogic', text = 'Boss Logic', default = true, },
				{ type = 'checkbox', key = 'agipotion', text = 'Agility Potion Logic', default = false, },
				{ type = 'checkbox', key = 'defensives', text = 'Defensive Logic', default = true, },
				{ type = 'checkbox', key = 'misdirects', text = 'Misdirection Logic', default = true, },
				{ type = 'checkbox', key = 'pause', text = 'Pause on Keybind', default = true, },
				{ type = 'checkbox', key = 'petmanagement', text = 'Pet Management', default = true, },
				{ type = 'checkbox', key = 'nobarrage', text = 'No Barrage (prevent Barrage use)', default = false },
				{ type = 'checkbox', key = 'nocleave', text = 'No Cleave (prevent any AoE)', default = false },
				{ type = 'checkbox', key = 'noexecute', text = 'No raexecute() register', default = false, },
				{ type = 'checkbox', key = 'noprioritize', text = 'No raprioritize() register', default = false, },
				{ type = 'checkbox', key = 'summonpet', text = 'Summon Pet', default = true, },
				{ type = 'dropdown', key = 'summonslot', text = 'Which Pet to Summon', list = {
					{ key = 'slot1', text = tostring(RA.hunterPetSlot[1]) },
					{ key = 'slot2', text = tostring(RA.hunterPetSlot[2]) },
					{ key = 'slot3', text = tostring(RA.hunterPetSlot[3]) },
					{ key = 'slot4', text = tostring(RA.hunterPetSlot[4]) },
					{ key = 'slot5', text = tostring(RA.hunterPetSlot[5]) },
				}, default = 'slot1', },
				{ type = 'spinner', key = 'executevalue', text = 'Execute range percentage |cffaaaaaaHP < %|r', default = 20, },

				{ type = 'rule', },
				{ type = 'header', text = 'Advanced',},
				{ type = 'rule', },

				{ type = 'header', text = 'Unit Sniping',},
				{ type = 'checkbox', key = 'amocsnipe', text = 'Use AMoC on low health Units (to reset).', default = true },
				{ type = 'checkbox', key = 'killshotsnipe', text = 'Use Kill Shot on low health Units.', default = true },
				{ type = 'rule', },

				{ type = 'header', text = 'Aspect of the Cheetah',},
				{ type = 'checkbox', key = 'aspectincombat', text = 'Leave Aspect of the Pack on in combat?', default = true },
				{ type = 'spinner', key = 'aspectmovingfor', text = 'Aspect after Moving for |cffaaaaaaX seconds|r', min = 0, max = 10, step = 1, default = 2, desc = 'Aspect of the Cheetah will be cast if the player has been moving for X seconds. If X is 0 the cast will be instant. Aspect will cancel upon entering combat if Glyph of Aspect of the Cheetah is not active.' },
				{ type = 'rule', },

				{ type = 'header', text = 'Boss Logic' },
				{ type = 'checkbox', key = 'brackflamethrower', text = 'Turn off Flamethrower if activated', default = true },
				{ type = 'checkspin', key = 'fdinfestingsporestacks', text = 'Feign Death Infesting Spores |cffaaaaaaStacks >|r', default_check = true, default_spin = 6, },
				{ type = 'rule', },

				{ type = 'header', text = 'Camouflage' },
				{ type = 'checkbox', key = 'camouflage', text = 'Use Camouflage if Glyph of Camouflage enabled?', default = true },
				{ type = 'rule', },

				{ type = 'header', text = 'Defensive Logic' },
				{ type = 'spinner', key = 'exhilaration', text = 'Exhilaration at |cffaaaaaaHP < %|r', default = 60, },
				{ type = 'spinner', key = 'deterrence', text = 'Deterrence at |cffaaaaaaHP < %|r', default = 10 },
				{ type = 'spinner', key = 'healthpot', text = 'Health Potion/Stone at |cffaaaaaaHP < %|r', default = 40, },
				{ type = 'checkbox', key = 'masterscall', text = 'Master\'s Call slows?', default = false },
				{ type = 'rule' },

				{ type = 'header', text = 'Miscellaneous' },
				{ type = 'checkbox', key = 'concussiveshot', text = 'Concussive Shot moving targets', default = true, },
				{ type = 'checkbox', key = 'tranqshot', text = 'Tranquilize dispellable buffs', default = false, desc = 'Automatically attempt to remove dispellabe Magic or Enrage effects with Tranquilizing Shot.', },
				{ type = 'rule', },

				{ type = 'header', text = 'Misdirection' },
				{ type = 'spinner', key = 'mdfocusagro', text = 'Misdirect to Focus at |cffaaaaaaX% aggro|r', default = 50, },
				{ type = 'dropdown', key = 'mdkeybind', text = 'Misdirection Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'lalt', desc = 'Misdirect to Focus Target, if no Focus Misdirect to Mouseover Target', },
				{ type = 'rule' },

				{ type = 'header', text = 'Pause',},
				{ type = 'dropdown', key = 'pausekeybind', text = 'Pause Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'lshift' },
				{ type = 'rule', },

				{ type = 'header', text = 'Pet Management' },
				{ type = 'spinner', key = 'petmend', text = 'Mend Pet at |cffaaaaaaHP < %|r', default = 95, },
				{ type = 'spinner', key = 'petdash', text = 'Use Dash if |cffaaaaaaTarget > Distance|r', min = 1, max = 40, step = 1, default = 15, },
				{ type = 'rule' },

				{ type = 'header', text = 'Pool Focus',},
				{ type = 'dropdown', key = 'poolfocuskeybind', text = 'Focus pooling Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'rcontrol' },
				{ type = 'rule' },

				{ type = 'header', text = 'Debug', },
				{ type = 'checkspin', key = 'debug', text = 'Debug output, set time interval to the right.', min = 0, max = 5, step = .1, default = 0, },
				{ type = 'rule' },

				{ type = 'header', text = 'Extra' },
				{ type = 'checkbox', key = 'autolfg', text = 'Auto LFG Accept', default = false, },
				{ type = "checkbox", key = 'splash', text = "Splash Image", default = true, },
			}
		})
		RA.optionsWindowHBM.parent:Hide()
	end
	RA.OptionsWindowHBMBuild()

	ProbablyEngine.buttons.create(
		'config', 'Interface\\ICONS\\Inv_misc_gear_01',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('config')
				RA.OptionsWindowShow()
			else
				ProbablyEngine.buttons.setActive('config')
				RA.OptionsWindowShow()
			end
		end,
		'Configure', 'Configure the rotation options.'
	)
	ProbablyEngine.buttons.setInactive('config')

	ProbablyEngine.buttons.create(
		'nobarrage', 'Interface\\ICONS\\ability_hunter_rapidregeneration',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('nobarrage')
				self.checked = false
				RA.Write('nobarrage', false)
				RA.optionsWindowHBM = nil
				RA.OptionsWindowHBMBuild()
				RA.warningFrameBarrage:message("Barrage On!")
			else
				ProbablyEngine.buttons.setActive('nobarrage')
				self.checked = true
				RA.Write('nobarrage', true)
				RA.optionsWindowHBM = nil
				RA.OptionsWindowHBMBuild()
				RA.warningFrameBarrage:message("Barrage Off!")
			end
		end,
		'No Barrage', 'Prevents the use Barrage.'
	)
	if RA.Fetch("nobarrage", false) == false then
		ProbablyEngine.buttons.setInactive("nobarrage")
	end

	ProbablyEngine.buttons.create(
		'nocleave', 'Interface\\ICONS\\Spell_shadow_rainoffire',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('nocleave')
				self.checked = false
				RA.Write('nocleave', false)
				RA.optionsWindowHBM = nil
				RA.OptionsWindowHBMBuild()
				RA.warningFrameCleave:message("Cleave On!")
			else
				ProbablyEngine.buttons.setActive('nocleave')
				self.checked = true
				RA.Write('nocleave', true)
				RA.optionsWindowHBM = nil
				RA.OptionsWindowHBMBuild()
				RA.warningFrameCleave:message("Cleave Off!")
			end
		end,
		'No Cleave', 'Prevents the use of all multi-target abilities.'
	)
	if RA.Fetch("nocleave", false) == false then
		ProbablyEngine.buttons.setInactive("nocleave")
	end
end)