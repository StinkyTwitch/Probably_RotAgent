--[[------------------------------------------------------------------------------------------------

DeathKnightFrostDW.lua

RotAgent (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

------------------------------------------------------------------------------------------------]]--


----------------------------------------------------------------------------------------------------
--  LOCAL VARIABLS/TABLES                                                                         --
----------------------------------------------------------------------------------------------------
local defensives = RA.dkDefensives
local queueSpells = RA.dkQueueSpellsFrost

local icyveins = {


}

local summonstone = {

	-- Pillar of Frost -----------------------------------------------------------------------------
	{ "Pillar of Frost", { "modifier.cooldowns", }, },
	{ "Berserking", { "modifier.cooldowns", }, },
	{ "Blood Fury", { "modifier.cooldowns", }, },

	-- Plauge Leech --------------------------------------------------------------------------------
	{ "Plague Leech", {
		"!lastcast(Plague Leech)",
		"!player.buff(Killing Machine)",
		"target.debuff(Blood Plague)",
		"target.debuff(Frost Fever)",
		"player.runesunavailable(2)",
	}, },
	{ "Plague Leech", {
		"!lastcast(Plague Leech)",
		"player.buff(Killing Machine)",
		"target.debuff(Blood Plague).duration <= 2",
		"target.debuff(Frost Fever).duration <= 2",
		"player.runesunavailable(2)",
	}, },

	-- Soul Reaper ---------------------------------------------------------------------------------
	{ "Soul Reaper", { "target.health <= 35", "target.raarea(10).enemies <= 3", }, },

	-- Blood Tap -----------------------------------------------------------------------------------
	{ "Blood Tap", {
		"player.buff(Blood Charge).count >= 5",
		"player.spell(Soul Reaper).cooldown = 0",
		"target.health <= 35",
		"player.runesunavailable(6)",
		function() RA.Debug("Blood Tap 1", "") end,
	}, },

	-- Defile --------------------------------------------------------------------------------------
	{ "Defile", { "target.deathin >= 9", }, },

	-- Blood Tap -----------------------------------------------------------------------------------
	{ "Blood Tap", {
		"player.buff(Blood Charge).count >= 5",
		"player.spell(Defile).cooldown = 0",
		"player.runesunavailable(6)",
		function() RA.Debug("Blood Tap 2", "") end,
	}, },

	-- Breath of Sindragosa ------------------------------------------------------------------------
	{ "Breath of Sindragosa", { "player.runicpower >= 75", }, },

	-- Obliterate ----------------------------------------------------------------------------------
	{ "Obliterate", {
		"player.buff(Killing Machine)",
		"player.runicpower <= 25",
		"player.runesfrac(Unholy) >= 1.95"
	}, },
	{ "Obliterate", { "player.runicpower <= 25", }, },
	{ "Obliterate", { "player.runesfrac(Unholy) >= 1.95", }, },

	-- Frost Strike --------------------------------------------------------------------------------
	{ "Frost Strike", { "player.buff(Killing Machine)", "player.runicpower >= 88", }, },
	{ "Frost Strike", { "player.buff(Killing Machine)", }, },

	-- Blood Boil ----------------------------------------------------------------------------------
	--?

	-- Frost Strike --------------------------------------------------------------------------------
	{ "Frost Strike", { "player.runicpower >= 88", }, },

	-- Death and Decay -----------------------------------------------------------------------------
	{ "Death and Decay", { "target.raarea(10).enemies >= 3", }, "target.ground", },

	-- Howling Blast -------------------------------------------------------------------------------
	{ "Howling Blast", {
		"player.buff(Freezing Fog)",
		"player.runesfrac(Frost) >= 1.99",
		"player.runesfrac(Death) >= 1.99",
	}, },

	-- Blood Tap -----------------------------------------------------------------------------------
	{ "Blood Tap", {
		"player.buff(Blood Charge).count >= 10",
		"player.runesunavailable(1)",
		function() RA.Debug("Blood Tap 3", "") end,
	}, },

	-- Frost Strike --------------------------------------------------------------------------------
	{ "Frost Strike", { "player.runicpower >= 76", }, },

	-- Outbreak ------------------------------------------------------------------------------------
	{ "Outbreak", {
		"!target.debuff(Blood Plague)",
		"!target.debuff(Frost Fever)",
	}, },

	-- Plague Strike -------------------------------------------------------------------------------
	{ "Plague Strike", {
		"player.spell(Outbreak).cooldown > 1",
		"!target.debuff(Blood Plague)",
	}, },

	-- Howling Blast -------------------------------------------------------------------------------
	{ "Howling Blast", {
		"player.runes(frost).count >= 1",
	}, },
	{ "Howling Blast", {
		"player.runes(death).count >= 1",
	}, },

	-- Frost Strike --------------------------------------------------------------------------------
	{ "Frost Strike", { "player.runicpower >= 25", }, },

	-- Empower Rune Weapon -------------------------------------------------------------------------
	{ "Empower Rune Weapon", {
		"modifier.cooldowns",
		"player.runicpower <= 70",
		"player.runes(unholy).count = 0",
		"player.runes(frost).count = 0",
		"player.runes(blood).count = 0",
	}, },
}


----------------------------------------------------------------------------------------------------
--  ROTATION OPENERS                                                                              --
----------------------------------------------------------------------------------------------------


----------------------------------------------------------------------------------------------------
--  ROTATION                                                                                      --
----------------------------------------------------------------------------------------------------
ProbablyEngine.rotation.register_custom(251, "RA - Frost DW",
-- Combat
{
	-- Pause ---------------------------------------------------------------------------------------
	{ "pause", { "@RA.Pause()" }, },

	{ queueSpells, },

	{ defensives, function() return RA.Fetch("defensives", true) == true end, },

	{ summonstone, },
},

-- OoC
{
	{ queueSpells, },
},

-- Callback
function()
	RA.BaseStatsInit()

	-- Splash Logo
	RA.SplashInit()

	function RA.OptionsWindowDKFDWBuild()
		RA.optionsWindowDKFDW = ProbablyEngine.interface.buildGUI({
			key = 'ra_config_dk_fdw',
			title = 'RotAgent',
			subtitle = 'Frost DW',
			profiles = true,
			width = 275,
			height = 500,
			color = "4e7300",
			config = {
				{ type = 'rule', },
				{ type = 'header', text = 'Basics', },
				{ type = 'rule', },

				{ type = 'checkbox', key = 'autotarget', text = 'Auto Target Logic', default = true, },
				{ type = 'dropdown', key = 'autotargetalgorithm', text = 'Auto Target Algorithm', list = {
					{ key = 'highest', text = 'Highest HP' },
					{ key = 'lowest', text = 'Lowest HP' },
					{ key = 'nearest', text = 'Nearest' },
					{ key = 'cascade', text = 'Cascade' },
				}, default = 'highest', },
				{ type = 'checkbox', key = 'autocleartarget', text = 'Auto Clear Target if not Enemy Unit.', default = true, },
				{ type = 'checkbox', key = 'bosslogic', text = 'Boss Logic', default = true, },
				{ type = 'checkbox', key = 'defensives', text = 'Defensive Logic', default = true, },
				{ type = 'checkbox', key = 'pause', text = 'Pause on Keybind', default = true, },
				{ type = 'checkbox', key = 'nocleave', text = 'No Cleave (prevent any AoE)', default = false },
				{ type = 'checkbox', key = 'noexecute', text = 'No raexecute() register', default = false, },
				{ type = 'checkbox', key = 'noprioritize', text = 'No raprioritize() register', default = false, },
				{ type = 'spinner', key = 'executevalue', text = 'Execute range percentage |cffaaaaaaHP < %|r', default = 35, },

				{ type = 'rule', },
				{ type = 'header', text = 'Advanced',},
				{ type = 'rule', },

				{ type = 'header', text = 'Defensive Logic' },
				{ type = 'spinner', key = 'deathpact', text = 'Death Pact at |cffaaaaaaHP < %|r', default = 10 },
				{ type = 'spinner', key = 'deathstrike', text = 'Death Strike with Dark Succor at |cffaaaaaaHP < %|r', default = 90 },
				{ type = 'spinner', key = 'healthpot', text = 'Health Potion/Stone at |cffaaaaaaHP < %|r', default = 40, },
				{ type = 'spinner', key = 'ibf', text = 'Icebound Fortitude at |cffaaaaaaHP < %|r', default = 10 },
				{ type = 'rule' },

				{ type = 'header', text = 'Pause',},
				{ type = 'dropdown', key = 'pause_keybind', text = 'Pause Keybind', list = {
					{ key = 'lalt', text = 'lalt' },
					{ key = 'lcontrol', text = 'lcontrol' },
					{ key = 'lshift', text = 'lshift' },
					{ key = 'ralt', text = 'ralt' },
					{ key = 'rcontrol', text = 'rcontrol' },
					{ key = 'rshift', text = 'rshift' },
				}, default = 'lshift' },
				{ type = 'rule', },

				{ type = 'header', text = 'Debug', },
				{ type = 'checkspin', key = 'debug', text = 'Debug output, set time interval to the right.', min = 0, max = 5, step = .1, default = 0, },
				{ type = 'rule' },

				{ type = 'header', text = 'Extra' },
				{ type = 'checkbox', key = 'autolfg', text = 'Auto LFG Accept', default = false, },
				{ type = "checkbox", key = 'splash', text = "Splash Image", default = true, },
			}
		})
		RA.optionsWindowDKFDW.parent:Hide()
	end
	RA.OptionsWindowDKFDWBuild()

	ProbablyEngine.buttons.create(
		'config', 'Interface\\ICONS\\Inv_misc_gear_01',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('config')
				RA.OptionsWindowShow()
			else
				ProbablyEngine.buttons.setActive('config')
				RA.OptionsWindowShow()
			end
		end,
		'Configure', 'Change how the rotation behaves.'
	)
	ProbablyEngine.buttons.create(
		'nocleave', 'Interface\\ICONS\\Spell_shadow_rainoffire',
		function(self)
			if self.checked then
				ProbablyEngine.buttons.setInactive('nocleave')
				self.checked = false
				RA.Write('nocleave', false)
				RA.optionsWindowDKFDW = nil
				RA.OptionsWindowDKFDWBuild()
				RA.warningFrameCleave:message("Cleave On!")
			else
				ProbablyEngine.buttons.setActive('nocleave')
				self.checked = true
				RA.Write('nocleave', true)
				RA.optionsWindowDKFDW = nil
				RA.OptionsWindowDKFDWBuild()
				RA.warningFrameCleave:message("Cleave Off!")
			end
		end,
		'No Cleave', 'Prevents the use of all multi-target abilities.'
	)
	ProbablyEngine.buttons.setInactive('config')
	ProbablyEngine.buttons.setInactive('nocleave')
end)