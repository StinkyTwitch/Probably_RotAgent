--[[------------------------------------------------------------------------------------------------

HelperSymbols.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
--local LibDraw = LibStub("LibDraw-1.0")

RA.helperSymbols = false
RA.helperSymbolsFilter = nil
function RA.HelperSymbolsToggle(filter)
	RA.helperSymbols = not RA.helperSymbols

	if RA.helperSymbols then
		if filter then
			RA.helperSymbolsFilter = filter
		end
		print("Helper Symbols ON!")
	else
		print("Helper Symbols OFF!")
	end
end


RotDraw.Sync(function()


	------------------------------------------------------------------------------------------------
	-- UNFILTERED SYMBOLS
	------------------------------------------------------------------------------------------------
	if FireHack and RA.helperSymbols then

		if RA.drawParsedTarget ~= nil then
			RA.Debug("RA.drawParsedTarget", "entry!")
			RotDraw.SetColor(0, 255, 0, 255)
			RotDraw.SetWidth(1)

			local unitExists = UnitExists(RA.drawParsedTarget)
			local unitDead = UnitIsDead(RA.drawParsedTarget)

			if unitExists and not unitDead then
				local playerX, playerY, playerZ = ObjectPosition("player")
				local targetX, targetY, targetZ = ObjectPosition(RA.drawParsedTarget)
				local combatReach = RA.CombatReach(RA.drawParsedTarget)

				RotDraw.Circle(targetX, targetY, targetZ, combatReach-0.25)
				RotDraw.Line(playerX, playerY, playerZ, targetX, targetY, targetZ)
				C_Timer.After(1, function() RA.drawParsedTarget = nil end)
			end
		end

		if RA.drawBarrageTarget ~= nil then
			RA.Debug("RA.drawBarrageTarget", "entry!")

			RotDraw.SetColor(0, 128, 255, 255)
			RotDraw.SetWidth(1)

			local unitExists = UnitExists(RA.drawBarrageTarget)
			local unitDead = UnitIsDead(RA.drawBarrageTarget)

			if unitExists and not unitDead then
				local playerX, playerY, playerZ = ObjectPosition("player")
				local targetX, targetY, targetZ = ObjectPosition(RA.drawBarrageTarget)
				local combatReach = RA.CombatReach(RA.drawBarrageTarget)

				RotDraw.Circle(targetX, targetY, targetZ, combatReach)
				RotDraw.Line(playerX, playerY, playerZ, targetX, targetY, targetZ)
				C_Timer.After(3, function() RA.drawBarrageTarget = nil end)
			end
		end

		if RA.drawChimaeraTarget ~= nil then
			RA.Debug("RA.drawChimaeraTarget", "entry!")

			RotDraw.SetColor(255, 102, 255, 255)
			RotDraw.SetWidth(1)

			local unitExists = UnitExists(RA.drawChimaeraTarget)
			local unitDead = UnitIsDead(RA.drawChimaeraTarget)

			if unitExists and not unitDead then
				local playerX, playerY, playerZ = ObjectPosition("player")
				local targetX, targetY, targetZ = ObjectPosition(RA.drawChimaeraTarget)
				local combatReach = RA.CombatReach(RA.drawChimaeraTarget)

				RotDraw.Circle(targetX, targetY, targetZ, combatReach)
				RotDraw.Line(playerX, playerY, playerZ, targetX, targetY, targetZ)
				C_Timer.After(1.5, function() RA.drawChimaeraTarget = nil end)
			end
		end

		if RA.drawExplosiveTarget ~= nil then
			RA.Debug("RA.drawExplosiveTarget", "entry!")

			RotDraw.SetColor(255, 128, 0, 255)
			RotDraw.SetWidth(1)

			local unitExists = UnitExists(RA.drawExplosiveTarget)
			local unitDead = UnitIsDead(RA.drawExplosiveTarget)

			if unitExists and not unitDead then
				local playerX, playerY, playerZ = ObjectPosition("player")
				local targetX, targetY, targetZ = ObjectPosition(RA.drawExplosiveTarget)
				local combatReach = RA.CombatReach(RA.drawExplosiveTarget)

				RotDraw.Circle(targetX, targetY, targetZ, combatReach)
				RotDraw.Line(playerX, playerY, playerZ, targetX, targetY, targetZ)
				C_Timer.After(1.5, function() RA.drawExplosiveTarget = nil end)
			end
		end

		if RA.drawMultiShotTarget ~= nil then
			RA.Debug("RA.drawMultiShotTarget", "entry!")

			RotDraw.SetColor(153, 51, 255, 255)
			RotDraw.SetWidth(1)

			local unitExists = UnitExists(RA.drawMultiShotTarget)
			local unitDead = UnitIsDead(RA.drawMultiShotTarget)

			if unitExists and not unitDead then
				local playerX, playerY, playerZ = ObjectPosition("player")
				local targetX, targetY, targetZ = ObjectPosition(RA.drawMultiShotTarget)
				local combatReach = RA.CombatReach(RA.drawMultiShotTarget)

				RotDraw.Circle(targetX, targetY, targetZ, combatReach)
				RotDraw.Line(playerX, playerY, playerZ, targetX, targetY, targetZ)
				C_Timer.After(1.5, function() RA.drawMultiShotTarget = nil end)
			end
		end





	------------------------------------------------------------------------------------------------
	-- FILTERED SYMBOLS
	------------------------------------------------------------------------------------------------





		-- Target Symbols --------------------------------------------------------------------------
		if RA.helperSymbolsFilter == "target" then
			-- Pet Target Identifier --
			if UnitExists("pettarget") then
				local targetX, targetY, targetZ = ObjectPosition("pettarget")

				RotDraw.SetColor(252, 255, 150)
				RotDraw.Circle(targetX, targetY, targetZ, 0.9)
			end


			-- Target Identifier --
			if UnitExists("target") then
				local targetX, targetY, targetZ = ObjectPosition("target")

				RotDraw.SetColor(100, 246, 255)
				RotDraw.Circle(targetX, targetY, targetZ, 1.0)
			end
		end


		-- Custom Range Markers --------------------------------------------------------------------
		if RA.helperSymbolsFilter == "range10" then
			local targetX, targetY, targetZ = ObjectPosition("player")

			RotDraw.SetColor(100, 246, 255)
			RotDraw.Circle(targetX, targetY, targetZ, 10.0)
		end

		if RA.helperSymbolsFilter == "range8" then
			local targetX, targetY, targetZ = ObjectPosition("player")

			RotDraw.SetColor(100, 246, 255)
			RotDraw.Circle(targetX, targetY, targetZ, 8.0)
		end


		-- Garrison Mining -------------------------------------------------------------------------
		if GetSubZoneText() == "Frostwall Mine" and RA.helperSymbolsFilter == "mine" then
			local objectCount = GetObjectCount()

			for i=1, objectCount do
				local object = GetObjectWithIndex(i)
				local objectExists = ObjectExists(object)

				if ObjectName(object) == 'Rich True Iron Deposit' then
					local x, y, z = ObjectPosition(object)
					RotDraw.SetColor(0, 255, 0)
					RotDraw.Circle(x, y, z, 1.5)
				elseif ObjectName(object) == 'True Iron Deposit' then
					local x, y, z = ObjectPosition(object)
					RotDraw.SetColor(0, 255, 0)
					RotDraw.Circle(x, y, z, 1.5)

				elseif ObjectName(object) == 'Rich Blackrock Deposit' then
					local x, y, z = ObjectPosition(object)
					RotDraw.SetColor(0, 255, 0)
					RotDraw.Circle(x, y, z, 1.5)

				elseif ObjectName(object) == 'Blackrock Deposit' then
					local x, y, z = ObjectPosition(object)
					RotDraw.SetColor(0, 255, 0)
					RotDraw.Circle(x, y, z, 1.5)

				elseif ObjectName(object) == 'Mine Cart' then
					local x, y, z = ObjectPosition(object)
					local rotation = ObjectFacing(object)
					RotDraw.SetColor(0, 255, 0)
					RotDraw.Box(x, y, z, 2, 2, rotation)

				end
			end
		end


		-- HFC Archimonde --------------------------------------------------------------------------
		--[[
		if GetSubZoneText() == "The Black Gate" and RA.helperSymbolsFilter == "archimonde" then

			for i = 1, #RA.unitCacheAll do
				local npcID = select(6, ObjectGUID(RA.unitCacheAll[i].object))

				-- Doomfire Spirit
				if npcID == 92208 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				-- Felborne Overfiend
				elseif npcID == 93615 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				-- Hellfire Death
				elseif npcID == 92740 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				-- Infernal Doombringer
				elseif npcID == 94412 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				-- Living Shadow
				elseif npcID == 93297 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				-- Void Star
				elseif npcID == 95775 then
					local x, y, z = ObjectPosition(RA.unitCacheAll[i].object)
					RotDraw.SetColor(255, 0, 0)
					RotDraw.Circle(x, y, z, 1.5)

				end
			end

			-- Shadowfel Burst Range 8
			if UnitCastingInfo("Archimonde") == "Shadowfel Burst" then
				local x, y, z = ObjectPosition("player")
				local uaplayer = RA.UnitsAroundUnit("player", 8)

					if uaplayer > 1 then
						-- Draw Red circle if someone is within 8 yards of "player"
						RotDraw.SetColor(0, 255, 0)
						RotDraw.Circle(x, y, z, 8.0)
					else
						-- Draw Green circle if no one is within 8 yards of "player"
						RotDraw.SetColor(0, 255, 0)
						RotDraw.Circle(x, y, z, 8.0)
					end
			end
		end
		]]


	end
end)