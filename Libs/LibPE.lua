--[[------------------------------------------------------------------------------------------------

LibPE.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]


----------------------------------------------------------------------------------------------------
-- CUSTOM PE OVERLOADS
----------------------------------------------------------------------------------------------------


--[[------------------------------------------------------------------------------------------------
	Name: RA.rotOverloads
	Type:
	Arguments:
	Return:
	Description:
--]]
local overloadsFirstRun = false
function RA.PEOverloads()
	-- We only want to override this once.
	if not overloadsFirstRun then
		if FireHack then
			CastGround = nil
			LineOfSight = nil

			function CastGround(spell, target)
				if UnitExists(target) then
					local _,_,_,_,_, maxRange,_ = GetSpellInfo(spell)
					local reachPlayer = UnitCombatReach("player")
					local reachTarget = UnitCombatReach(target)
					local combatDistance = Distance(target, "player")
					local trueDistance = combatDistance + reachTarget + reachPlayer

					if combatDistance < maxRange and trueDistance > maxRange then
						local x, y, z = GetPositionBetweenObjects("player", target, maxRange-3)
						CastSpellByName(spell)
						ClickPosition(x, y, z, true)
						CancelPendingSpell()
						return
					else
						local x, y, z = ObjectPosition(target)
						CastSpellByName(spell)
						ClickPosition(x, y, z, true)
						CancelPendingSpell()
						return
					end
				end
				if not ProbablyEngine.timeout.check('groundCast') then
					ProbablyEngine.timeout.set('groundCast', 0.05, function()
						Cast(spell)
						if IsAoEPending() then
							SetCVar("deselectOnClick", "0")
							CameraOrSelectOrMoveStart(1)
							CameraOrSelectOrMoveStop(1)
							SetCVar("deselectOnClick", "1")
							SetCVar("deselectOnClick", stickyValue)
							CancelPendingSpell()
						end
					end)
				end
			end
			function LineOfSight(a, b)
				if UnitExists(a) and UnitExists(b) then
					-- Ignore Line of Sight on these units with very weird combat.
					local ignoreLOS = {
						[76585] = true,     -- Ragewing the Untamed (UBRS)
						[77063] = true,     -- Ragewing the Untamed (UBRS)
						[77182] = true,     -- Oregorger (BRF)
						[77891] = true,     -- Grasping Earth (BRF)
						[77893] = true,     -- Grasping Earth (BRF)
						[78981] = true,     -- Iron Gunnery Sergeant (BRF)
						[81318] = true,     -- Iron Gunnery Sergeant (BRF)
						[83745] = true,     -- Ragewing Whelp (UBRS)
						[86252] = true,     -- Ragewing the Untamed (UBRS)
					}
					local losFlags =  bit.bor(0x10, 0x100)
					local ax, ay, az = ObjectPosition(a)
					local bx, by, bz = ObjectPosition(b)

					-- Variables
					local aCheck = select(6,strsplit("-",UnitGUID(a)))
					local bCheck = select(6,strsplit("-",UnitGUID(b)))

					if ignoreLOS[tonumber(aCheck)] ~= nil then return true end
					if ignoreLOS[tonumber(bCheck)] ~= nil then return true end

					if ax == nil or ay == nil or az == nil or bx == nil or by == nil or bz == nil then
						return false
					end

					if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then
						return false
					end

					return true
				end
			end
		end

		-- Only load once
		if ProbablyEngine.protected.unlocked and ProbablyEngine.protected.method == "firehack" then
			overloadsFirstRun = true
			print("RotAgent Overloads Loaded!")
		end
	end
end









----------------------------------------------------------------------------------------------------
-- CUSTOM PE CONDITION REGISTERS
----------------------------------------------------------------------------------------------------


--[[------------------------------------------------------------------------------------------------
	Name: bursthaste
	Type: PE Register
	Arguments: target - Valid game unit
	Return: Boolean
	Description: Checks if the 'target' currently has any one of the Burst Haste buffs. If found
	return true, otherwise return false.
--]]
ProbablyEngine.condition.register("rabursthaste", function(target)
	local burstHasteBuffs = {
		2825,       -- Bloodlust (Horde Shaman)
		32182,      -- Heroism (Alliance Shaman)
		90355,      -- Ancient Hysteria (Core Hound, BM Hunter Pet)
		80353,      -- Time Warp (Mage)
		120257,     -- Drums of Fury (Leatherworking)
		146555,     -- Drums of Rage (Leatherworking)
		160452,     -- Netherwinds (Nether Ray, Hunter Pet)
		178207,     -- Drums of Fury (Leatherworking)
	}

	for i = 1, #burstHasteBuffs do
		if UnitBuff('player', GetSpellName(burstHasteBuffs[i])) then
			return true
		end
	end
	return false
end)


--[[------------------------------------------------------------------------------------------------
	Name: bursthastedebuff
	Type: PE Register
	Arguments: target
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("rabursthastedebuff", function(target)
	local burstHasteDebuffs = {
		57723,      -- Exhaustion (Heroism/Bloodlust)
		57724,      -- Sated (Ancient Hysteria)
		80354,      -- Temporal Displacement (Time Warp)
		160455,     -- Fatigued (Netherwinds)
	}

	for i = 1, #burstHasteDebuffs do
		if UnitDebuff('player', GetSpellName(burstHasteDebuffs[i])) then
			return true
		end
	end
	return false
end)


--[[------------------------------------------------------------------------------------------------
	Name: cc
	Type: PE Register
	Arguments: target - Valid game unit
	Return: Boolean
	Description: If the unit exists then check if it has a Special Aura (cc). If it does we return
	true, otherwise return false.
--]]
ProbablyEngine.condition.register("racc", function(target)
	if not UnitExists(target) then
		return false
	end

	if RA.SpecialCCDebuff(target) then
		return true
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: ccinarea
	Type: PE Register
	Arguments:
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raccinarea", function(target, radius)
	if not UnitExists(target) or not radius then
		return false
	end

	if FireHack then
		local targetGUID = UnitGUID(target)

		for i=1, #RA.unitCacheAll do
			local unitGUID = UnitGUID(RA.unitCacheAll[i].object)

			if unitGUID ~= targetGUID then
				local splashRadius = tonumber(radius)
				local distance = RA.Distance(target, RA.unitCacheAll[i].object, 2, "reach")
				local ccdUnit = RA.SpecialCCDebuff(RA.unitCacheAll[i].object)
				RA.Debug("ccinarea1", ""..splashRadius.." "..distance.." "..tostring(ccdUnit))
				if distance <= splashRadius and ccdUnit then
					return true
				end
			end
		end
		return false
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: distancetotarget
	Type: PE Register
	Arguments: target - Valid game unit
	Return: distance - Integer representing distance in yards between 'target' and  "target"
	Description:
--]]
ProbablyEngine.condition.register("radistancetotarget", function(unit)
	local targetExists = UnitExists("target")
	local unitExists = UnitExists(unit)
	if not targetExists or not unitExists then
		return 0
	end

	local isAttackable = UnitCanAttack(unit, "target")
	local isDead = UnitIsDead("target")

	if isAttackable and not isDead then
		if FireHack then
			return RA.Distance(unit, "target", 2, "reach")
		else
			return 0
		end
	else
		return 0
	end
end)


--[[------------------------------------------------------------------------------------------------
    Name: execute
    Type: PE Register
    Arguments:
    Return:
    Description:
--]]
ProbablyEngine.condition.register("raexecute", function(spell)
	RA.Debug("raexecute1", "register entry!")

	if RA.Fetch("noexecute", true) == true then
		RA.Debug("raexecute2", "raexecute is off!")
		return true
	elseif RA.Pause() then
		RA.Debug("raexecute3", "RA.Pause true!")
		return true
	elseif ProbablyEngine.module.player.casting == true then
		RA.Debug("raexecute4", "player casting!")
		return true
	end

	local spellCD, _, _ = GetSpellCooldown(spell)

	if RA.Eval("target.health") <= RA.Fetch("executevalue", 20) then
		RA.Debug("raexecute5", "target.health <= " .. RA.Fetch("executevalue", 20) .. ", return control to PE!")
		return true
	elseif spellCD == 0 then
		for i=1,#RA.unitCache do
			if RA.unitCache[i].health <= RA.Fetch("executevalue", 20) then
				if RA.LineOfSight("player", RA.unitCache[i].object) and RA.Facing("player", RA.unitCache[i].object) then
					RA.Debug("raexecute6", "" .. RA.unitCache[i].object .. " <= " .. RA.Fetch("executevalue", 20) .. " [return false]")
					Cast(spell, RA.unitCache[i].object)
					return false
				else
					RA.Debug("raexecute7", "Player not in LoS or not facing unit(" .. RA.unitCache[i].object .. ")!")
					return true
				end
			end
		end

		RA.Debug("raexecute8", "not execute targets found!")
		return true
	else
		RA.Debug("raexecute9", "" .. spell .. " not ready or no mana, return control to PE!")
		return true
	end

	RA.Debug("raexecute10", "you should not be here, return control to PE!")
	return true
end)


--[[------------------------------------------------------------------------------------------------
	Name: facing
	Type: PE Register
	Arguments:
	Return:
	Description:
--]]
ProbablyEngine.condition.register("rafacing", function(unit, target)
	local unitExists = UnitExists(unit)
	local targetExists = UnitExists(target)
	if not unitExists or not targetExists then
		return
	end
	if FireHack then
		if ObjectIsFacing(unit, target) then
			return true
		else
			return false
		end
	else
		if RA.listenerNotFacingTarget == true then
			return false
		else
			return true
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
    Name: lineofsight
    Type: PE Register
    Arguments:
    Return:
    Description:
--]]
ProbablyEngine.condition.register("ralineofsight", function(unit, target)
    local unitExists = UnitExists(unit)
    local targetExists = UnitExists(target)

    if not unitExists or not targetExists then
        return false
    end

    if FireHack then
        if RA.LineOfSight(unit, target) then
            return true
        else
            return false
        end
    end
end)


--[[------------------------------------------------------------------------------------------------
	Name: petdead
	Type: PE Register
	Arguments: target - Valid game unit
	Return: Boolean
	Description:
--]]
RA.listenerPetIsDead = false
RA.listenerReviveFailed = false
RA.listenerCallPetFailedPetDead = false
ProbablyEngine.condition.register("rapetdead", function(target)
	if RA.listenerPetIsDead and not RA.listenerReviveFailed then
		return true
	elseif RA.listenerCallPetFailedPetDead then
		return true
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: power.deficit
	Type: PE Register
	Arguments: target - Valid game unit
	Return: deficit - Interger representing power deficit as a percentage from 100%
	Description:
--]]
ProbablyEngine.condition.register("rapower.deficit", function(target)
	if not UnitExists(target) then
		return false
	end
	return UnitPowerMax(target) - UnitPower(target)
end)


--[[------------------------------------------------------------------------------------------------
	Name: power.regen
	Type: PE Register
	Arguments: target - Valid game unit
	Return: regen - Number indicating the 'targets' current Power regen rate per second
	Description:
--]]
ProbablyEngine.condition.register("rapower.regen", function(target)
	return select(2, GetPowerRegen(target))
end)


--[[------------------------------------------------------------------------------------------------
    Name: prioritize
    Type: PE Register
    Arguments:
    Return:
    Description:
--]]
RA.prioritizeLock = false
ProbablyEngine.condition.register("raprioritize", function(args)
	RA.Debug("raprioritize1", "register entry!")

	if RA.Fetch("noprioritize", true) == true then
		RA.Debug("raprioritize2", "raprioritize is off!")
		return true
	end

	if RA.Pause() then
		RA.Debug("raprioritize3", "RA.Pause() true!")
		return true
	end

	if ProbablyEngine.module.player.casting == true then
		RA.Debug("raprioritize4", "player casting!")
		return true
	end

	local spell, minHealth = strsplit(",", args, 2)
	spell = tostring(spell)
    minHealth = tonumber(minHealth)
	local spellUsable, spellNoMana = IsUsableSpell(spell)

	if RA.Eval("target.health >= 80") then
		RA.Debug("raprioritize5", "target > 80%!")
		return true
	else
		if spellUsable and not spellNoMana then
			for i=1,#RA.unitCache do
				if RA.unitCache[i].health >= minHealth then
					if RA.LineOfSight("player", RA.unitCache[i].object) and RA.Facing("player", RA.unitCache[i].object) then
						RA.Debug("raprioritize6", "" .. RA.unitCache[i].object .. " > 80%, cast Aimed Shot!")
						Cast(spell, RA.unitCache[i].object)
						return false
					else
						RA.Debug("raprioritize7", "Player not in LoS or not facing unit(" .. RA.unitCache[i].object .. ")!")
						return true
					end
				end
			end
		else
			RA.Debug("raprioritize8", "" .. spell .. " not ready or no mana!")
			return true
		end
	end

	RA.Debug("raprioritize9", "you should not be here!")
	return true
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.any
	Type: PE Register
	Arguments: target - Valid game unit
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.any", function(target)
	if UnitStat("player", 1) > RA.baseStats.strength then
		return true
	elseif UnitStat("player", 2) > RA.baseStats.agility then
		return true
	elseif UnitStat("player", 3) > RA.baseStats.stamina then
		return true
	elseif UnitStat("player", 4) > RA.baseStats.intellect then
		return true
	elseif UnitStat("player", 5) > RA.baseStats.spirit then
		return true
	elseif GetCritChance() > RA.baseStats.crit then
		return true
	elseif GetHaste() > RA.baseStats.haste then
		return true
	elseif GetMastery() > RA.baseStats.mastery then
		return true
	elseif GetMultistrike() > RA.baseStats.multistrike then
		return true
	elseif GetCombatRating(29) > RA.baseStats.versatility then
		return true
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.agility
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.agility", function(target, stat)
	local stat = string.lower(stat)
	if stat == "agility" then
		if UnitStat("player", 2) > RA.baseStats.agility then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.crit
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.crit", function(target, stat)
	if stat == "crit" then
		if GetCritChance() > RA.baseStats.crit then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.haste
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.haste", function(target, stat)
	if stat == "haste" then
		if GetHaste() > RA.baseStats.haste then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.intellect
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.intellect", function(target, stat)
	local stat = string.lower(stat)
	if stat == "intellect" then
		if UnitStat("player", 4) > RA.baseStats.intellect then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.mastery
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.mastery", function(target, stat)
	if stat == "mastery" then
		if GetMastery() > RA.baseStats.mastery then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.multistrike
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.multistrike", function(target, stat)
	if stat == "multistrike" then
		if GetMultistrike() > RA.baseStats.multistrike then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.spirit
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.spirit", function(target, stat)
	local stat = string.lower(stat)
	if stat == "spirit" then
		if UnitStat("player", 5) > RA.baseStats.spirit then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.stamina
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.stamina", function(target, stat)
	local stat = string.lower(stat)
	if stat == "stamina" then
		if UnitStat("player", 3) > RA.baseStats.stamina then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.strength
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.strength", function(target, stat)
	local stat = string.lower(stat)

	if stat == "strength" then
		if UnitStat("player", 1) > RA.baseStats.strength then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: proc.versatility
	Type: PE Register
	Arguments: target - Valid game unit
			   stat - String of primary or secondary stat type
	Return: Boolean
	Description:
--]]
ProbablyEngine.condition.register("raproc.versatility", function(target, stat)
	if stat == "versatility" then
		if GetCombatRating(29) > RA.baseStats.versatility then
			return true
		else
			return false
		end
	end

end)


--[[------------------------------------------------------------------------------------------------
	Name: raarea.enemies
	Type: PE Register
	Arguments: target, distance
	Return: total
	Description:
--]]
ProbablyEngine.condition.register("raarea.enemies", function(target, distance)
	if FireHack then
		distance = tonumber(distance)
		local total = RA.UnitsAroundUnit(target, distance)
		RA.Debug("raarea.enemies1", tostring(total))
		return total
	else
		if RA.Eval("modifier.multitarget") then
			return 2
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: ratalent
	Type: PE Register
	Arguments: row, col, spell
	Return: total
	Description:
--]]
ProbablyEngine.condition.register("ratalent", function(args)
	local row, col, id = strsplit(",", args, 3)
	local spellName = GetSpellInfo(id)
	local group = GetActiveSpecGroup()
	local talentId, talentName, icon, selected, active = GetTalentInfo(tonumber(row), tonumber(col), group)

	if talentName == spellName and selected and active then
		return true
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: runesfrac
	Type: PE Register
	Arguments: color, number
	Return:	boolean
	Description: Checks rune type and how far regeneration is on "depleted" and "ready" runes. Value
	             would check between 0 and 2. 0 being both runes of a "type" are fully depleted and
	             2 being both runes of a "type" are ready. 1.5 for example meaning 1 rune of a type
	             is ready and the other is 50% regenerated.
--]]
ProbablyEngine.condition.register("rarunesfrac", function(target, args)
	local color, number = strsplit(",", args, 2)

	rune = {}
	runePartial = {}

	bloodRunes = 0
	bloodPartial = 0
	unholyRunes = 0
	unholyPartial = 0
	frostRunes = 0
	frostPartial = 0
	deathRunes = 0
	deathPartial = 0

	for i=1,6 do
		runeStart = select(1,GetRuneCooldown(i))
		runeDuration = select(2,GetRuneCooldown(i))
		if runeStart > 0 then
			rune[i] = 0
			runePartial[i] = 0
			runePartial[i] = (rune[i] + (((((runeStart + runeDuration - GetTime()) - runeDuration ) * -1)) / runeDuration))
		else
			rune[i] = 1
		end

		if runePartial[i] == nil then
			runePartial[i] = 1
		end

		if runePartial[i] ~= nil and runePartial[i] < 0 then
			runePartial[i] = 0
		end

		--BLOOD
		if GetRuneType(i) == 1 then
			bloodRunes = rune[i] + bloodRunes
			bloodPartial = runePartial[i] + bloodPartial
		end

		--UNHOLY
		if GetRuneType(i) == 2 then
			unholyRunes = rune[i] + unholyRunes
			unholyPartial = runePartial[i] + unholyPartial
		end

		--FROST
		if GetRuneType(i) == 3 then
			frostRunes = rune[i] + frostRunes
			frostPartial = runePartial[i] + frostPartial
		end

		--DEATH
		if GetRuneType(i) == 4 then
			deathRunes = rune[i] + deathRunes
			deathPartial = runePartial[i] + deathPartial
		end
	end

	if color == "Blood" then
		if bloodPartial >= number then
			return true
		else
			return false
		end
	end

	if color == "Unholy" then
		if unholyPartial >= number then
			return true
		else
			return false
		end
	end

	if color == "Frost" then
		if frostPartial >= number then
			return true
		else
			return false
		end
	end

	if color == "Death" then
		if deathPartial >= number then
			return true
		else
			return false
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: runesunavailable
	Type: PE Register
	Arguments: number
	Return:	boolean
	Description: Check against a number versus the number of unavailable runes.
--]]
ProbablyEngine.condition.register("rarunesunavailable", function(target, number)
	number = tonumber(number)
	local unavailable = 0

	for i=1,6 do
		local start, duration, runeReady = GetRuneCooldown(i);
		if not runeReady then
			unavailable = unavailable + 1
		end
	end
	local typeUnavailable = type(unavailable)
	local typeNumber = type(number)
	--print(typeUnavailable, unavailable, typeNumber, number)
	if unavailable >= number then
		return true
	else
		return false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: spell.casttime
	Type: PE Register
	Arguments:
	Return:
	Description:
--]]
ProbablyEngine.condition.register("raspell.casttime", function(target, spell)
	local name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(spell)

	if name ~= nil then
		return castingTime / 1000.0
	else
		return 0
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: spell.cooldownremains
	Type: PE Register
	Arguments:
	Return:
	Description:
--]]
ProbablyEngine.condition.register("raspell.cooldownremains", function(target, spell)
	local spellStart, spellDuration, enabled = GetSpellCooldown(spell)
	local spellCDRemains = math.abs((spellStart + spellDuration) - GetTime())
	return spellCDRemains
end)


--[[------------------------------------------------------------------------------------------------
	Name: spell.regen
	Type: PE Register
	Arguments:
	Return:
	Description:
--]]
ProbablyEngine.condition.register("raspell.regen", function(target, spell)
	local name, rank, icon, castingTime, minRange, maxRange, spellID = GetSpellInfo(spell)
	local powerRegen = select(2, GetPowerRegen(target))

	if castingTime == 0 or castingTime == nil then
		castingTime = 1000
	else
		castingTime = castingTime / 1000.0
	end

	return castingTime * powerRegen
end)









----------------------------------------------------------------------------------------------------
-- CUSTOM PE INTERFACE FUNCTIONS
----------------------------------------------------------------------------------------------------


--[[------------------------------------------------------------------------------------------------
	Name: writeKey
	Type: PE Interface Function
	Arguments: keyA, keyB, value
	Return: Boolean
	Description:
--]]
function ProbablyEngine.interface.writeKey(keyA, keyB, value)
	local selectedProfile = ProbablyEngine.config.read(keyA .. '_profile', 'Default Profile')
	if selectedProfile then
		return ProbablyEngine.config.write(keyA .. selectedProfile .. '_' .. keyB, value)
	else
		return ProbablyEngine.config.write(keyA .. '_' .. keyB, value)
	end
end










----------------------------------------------------------------------------------------------------
-- CUSTOM PE LISTENERS
----------------------------------------------------------------------------------------------------


--[[------------------------------------------------------------------------------------------------
	Name: COMBAT_LOG_EVENT_UNFILTERED
	Type: PE Listener
	Arguments: None
	Return: None
	Description: If a combat log event for 'UNIT_DIED' for "players" 'pet' then we check that the
	"player" is not currently feigning dead. If not feigning then the "players" pet has died and
	we set the variable RA.listenerPetIsDead.
--]]
ProbablyEngine.listener.register("COMBAT_LOG_EVENT_UNFILTERED", function(...)
	local timeStamp, event, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags = ...
	local type, zero, server_id, instance_id, zone_uid, npc_id, spawn_uid = strsplit("-", destGUID)

	if event == "UNIT_DIED" and type == "Pet" and UnitIsDead("pet") then
		local spell = GetSpellInfo(5384)
		local feignDeath = UnitBuff("player", spell)

		if not feignDeath then
			RA.listenerPetIsDead = true
		end
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: UI_ERROR_MESSAGE
	Type: PE Listener
	Arguments: None
	Return: None
	Description:
--]]
RA.listenerNotFacingTarget = nil
ProbablyEngine.listener.register("UI_ERROR_MESSAGE", function(...)
	local args = ...

	if args == "Target needs to be in front of you." and UnitExists("target") then
		RA.listenerNotFacingTarget = true
	else
		RA.listenerNotFacingTarget = false
	end

	if args == "Your pet is dead. Use Revive Pet." then
		RA.listenerCallPetFailedPetDead = true
	else
		RA.listenerCallPetFailedPetDead = false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: UNIT_PET
	Type: PE Listener
	Arguments: None
	Return: None
	Description:
--]]
ProbablyEngine.listener.register("UNIT_PET", function(...)
	local unit = ...

	if unit == "player" and UnitExists("pet") then
		RA.listenerPetExists = true
	end
	if unit == "player" and not UnitExists("pet") then
		RA.listenerPetExists = false
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: UNIT_SPELLCAST_FAILED_QUIET
	Type: PE Listener
	Arguments: None
	Return: None
	Description:
--]]
ProbablyEngine.listener.register("UNIT_SPELLCAST_FAILED_QUIET", function(...)
	local unitID, spell, rank, lineID, spellID = ...

	if unitID == "player" and spellID == 982 then
		RA.listenerReviveFailed = true
	end
end)


--[[------------------------------------------------------------------------------------------------
	Name: UNIT_SPELLCAST_SUCCEEDED
	Type: PE Listener
	Arguments: None
	Return: None
	Description:
--]]
ProbablyEngine.listener.register("UNIT_SPELLCAST_SUCCEEDED", function(...)
	local unitID, spell, rank, lineID, spellID = ...

	if unitID == "player" and spellID == 982 then
		RA.listenerPetIsDead = false
		RA.listenerReviveFailed = false
	end
end)