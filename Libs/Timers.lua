--[[------------------------------------------------------------------------------------------------

Timers.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
--local RAName, RA = ...

-- Main
RA.mainTimer = C_Timer.NewTicker(0.25,
	(function()

		if
			ProbablyEngine.config.read('button_states', 'MasterToggle', false) and
			(ProbablyEngine.rotation.currentStringComp == "RA - Combat" or
			ProbablyEngine.rotation.currentStringComp == "RA - Frost DW" or
			ProbablyEngine.rotation.currentStringComp == "RA - Beast Mastery" or
			ProbablyEngine.rotation.currentStringComp == "RA - Marksmanship" or
			ProbablyEngine.rotation.currentStringComp == "RA - Survival")
		then
			RA.BaseStatsUpdate()
			RA.HelperWindowTarget("target")
			RA.HelperWindowTargetUpdate()
			RA.HelperWindowVariablesUpdate()
			RA.UnitCacheManager(40)

			-- Run ONLY if in Combat
			if ProbablyEngine.module.player.combat then
				if RA.Fetch('autotarget', true) then
					RA.AutoTargetClear()
					RA.AutoTargetEnemy()
				end
			end

			if RA.Fetch('autolfg', false) then
				ProbablyEngine.config.write('autolfg', true)
			else
				ProbablyEngine.config.write('autolfg', false)
			end
		end
	end),
nil)