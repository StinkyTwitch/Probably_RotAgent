--[[------------------------------------------------------------------------------------------------

ClassDK.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]


RA.dkDefensives = {
	{ "Death Strike", {
		"player.buff(Dark Succor)",
		"!player.raid",
	}, },
	{ "Death Strike", {
		function() return RA.Eval("player.health") < RA.Fetch("deathstrike", 90) end,
		"!player.raid",
		"player.runes(frost).count >= 1",
		"player.runes(unholy).count >= 1",
	}, },
	{ "#5512", {
		function() return RA.Eval("player.health") < RA.Fetch("healthpot", 40) end,
	}, },
	{ "#109223", {
		function() return RA.Eval("player.health") < RA.Fetch("healthpot", 40) end,
	}, },
	{ "Icebound Fortitude", {
		function() return RA.Eval("player.health") < RA.Fetch("ibf", 25) end,
	}, },
	{ "Death Pact", {
		function() return RA.Eval("player.health") < RA.Fetch("deathpact", 30) end,
	}, },
}


RA.dkQueueSpells = {
	-- GROUND SPELLS
	{ "Death and Decay", { "@RA.Queue(43265)", }, "mouseover.ground", },
	-- GENERAL
	{ "Anti-Magic Shell", "@RA.Queue(48707)", },
	{ "Army of the Dead", "@RA.Queue(42650)", },
	{ "Blood Boil", "@RA.Queue(50842)", },
	{ "Blood Presence", "@RA.Queue(48263)", },
	{ "Chains of Ice", "@RA.Queue(45524)", },
	{ "Control Undead", "@RA.Queue(111673)", },
	{ "Dark Simulacrum", "@RA.Queue(77606)", },
	{ "Death Coil", "@RA.Queue(47541)", },
	{ "Death Grip", "@RA.Queue(49576)", },
	{ "Death Strike", "@RA.Queue(49998)", },
	{ "Empower Rune Weapon", "@RA.Queue(47568)", },
	{ "Frost Presence", "@RA.Queue(48266)", },
	{ "Horn of Winter", "@RA.Queue(57330)", },
	{ "Icebound Fortitude", "@RA.Queue(48792)", },
	{ "Icy Touch", "@RA.Queue(45477)", },
	{ "Mind Freeze", "@RA.Queue(47528)", },
	{ "Outbreak", "@RA.Queue(77575)", },
	{ "Path of Frost", "@RA.Queue(3714)", },
	{ "Plague Strike", "@RA.Queue(45462)", },
	{ "Raise Ally", "@RA.Queue(61999)", },
	{ "Strangulate", "@RA.Queue(47476)", },
	{ "Unholy Presence", "@RA.Queue(48265)", },
	-- GLYPHS
	{ "Corpse Explosion", "@RA.Queue(127344)", },
	-- TALENTS
	{ "Anti-Magic Zone", "@RA.Queue(51052)", },
	{ "Asphyxiate", "@RA.Queue(108194)", },
	{ "Blood Tap", "@RA.Queue(45529)", },
	{ "Breath of Sindragosa", "@RA.Queue(152279)", },
	{ "Conversion", "@RA.Queue(119975)", },
	{ "Death Pact", "@RA.Queue(48743)", },
	{ "Death Siphon", "@RA.Queue(108196)", },
	{ "Death's Advance", "@RA.Queue(96268)", },
	{ "Defile", "@RA.Queue(152280)", },
	{ "Desecrated Ground", "@RA.Queue(108201)", },
	{ "Gorefiend's Grasp", "@RA.Queue(108199)", },
	{ "Lichborne", "@RA.Queue(49039)", },
	{ "Necrotic Plague", "@RA.Queue(152281)", },
	{ "Plague Leech", "@RA.Queue(123693)", },
	{ "Plaguebearer", "@RA.Queue(161497)", },
	{ "Remorseless Winter", "@RA.Queue(108200)", },
	{ "Unholy Blight", "@RA.Queue(115989)", },
	-- RACIALS
	{ "Arcane Torrent", "@RA.Queue(50613)", },
	{ "Berserking", "@RA.Queue(26297)", },
	{ "Blood Fury", "@RA.Queue(20572)", },
}


RA.dkQueueSpellsBlood = {
	{ RA.dkQueueSpells, },
	-- BLOOD
	{ "Bone Shield", "@RA.Queue(49222)", },
	{ "Dancing Rune Weapon", "@RA.Queue(49028)", },
	{ "Dark Command", "@RA.Queue(56222)", },
	{ "Rune Tap", "@RA.Queue(48982)", },
	{ "Soul Reaper", "@RA.Queue(114866)", },
	{ "Vampiric Blood", "@RA.Queue(55233)", },
}


RA.dkQueueSpellsFrost = {
	{ RA.dkQueueSpells, },
	-- FROST
	{ "Frost Strike", "@RA.Queue(49143)", },
	{ "Howling Blast", "@RA.Queue(49184)", },
	{ "Obliterate", "@RA.Queue(49020)", },
	{ "Pillar of Frost", "@RA.Queue(51271)", },
	{ "Soul Reaper", "@RA.Queue(130735)", },
}


RA.dkQueueSpellsUnholy = {
	{ RA.dkQueueSpells, },
	-- UNHOLY
	{ "Dark Transformation", "@RA.Queue(63560)", },
	{ "Festering Strike", "@RA.Queue(85948)", },
	{ "Raise Dead", "@RA.Queue(46584)", },
	{ "Scourge Strike", "@RA.Queue(55090)", },
	{ "Summon Gargoyle", "@RA.Queue(49206)", },
	{ "Soul Reaper", "@RA.Queue(130735)", },
}