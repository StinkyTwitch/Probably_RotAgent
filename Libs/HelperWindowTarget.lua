--[[------------------------------------------------------------------------------------------------

HelperWindowTarget.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
--local RAName, RA = ...

--[[------------------------------------------------------------------------------------------------
	Name: RA.HelperWindowTarget
	Type: Function
	Arguments:  target
	Return: None
	Description:
--]]
RA.helperWindowTarget = RA.helperWindowTarget or { }
function RA.HelperWindowTarget(target)

	if RA.helperWindowTargetShow then
		if not UnitExists(target) then
			for k in pairs(RA.helperWindowTarget) do
				RA.helperWindowTarget[k] = nil
			end
		else
			local targetGUID = UnitGUID(target)
			local targetName = UnitName(target)
			local targetDistance = RA.Distance("player", target, 2, "reach")
			local targetRealDistance = RA.Distance("player", target, 2)
			local targetCombatReach = RA.CombatReach(target)
			local targetHealthAct = UnitHealth(target)
			local targetHealthMax = UnitHealthMax(target)
			local targetHealthPct = math.floor((targetHealthAct / targetHealthMax) * 100)
			local targetAffectingCombat = UnitAffectingCombat(target)
			local targetReaction = UnitReaction("player", target)
			local targetSpecialAura = RA.SpecialCCDebuff(target)
			local targetSpecialTarget = RA.SpecialEnemyTarget(target)
			local targetTappedByPlayer = UnitIsTappedByPlayer(target)
			local targetTappedByAll = UnitIsTappedByAllThreatList(target)
			local targetAttackP2T = UnitCanAttack("player", target)
			local targetAttackT2P = UnitCanAttack(target, "player")
			local targetLoS = RA.LineOfSight("player", target)
			local targetDeathin = RA.TimeToDeath(target)

			RA.helperWindowTarget["guid"] = tostring(targetGUID)
			RA.helperWindowTarget["name"] = tostring(targetName)
			RA.helperWindowTarget["distance"] = targetDistance
			RA.helperWindowTarget["realdistance"] = targetRealDistance
			RA.helperWindowTarget["reach"] = targetCombatReach
			RA.helperWindowTarget["healthact"] = targetHealthAct
			RA.helperWindowTarget["healthmax"] = targetHealthMax
			RA.helperWindowTarget["healthpct"] = targetHealthPct
			RA.helperWindowTarget["combat"] = tostring(targetAffectingCombat)
			RA.helperWindowTarget["reaction"] = targetReaction
			RA.helperWindowTarget["specialaura"] = tostring(targetSpecialAura)
			RA.helperWindowTarget["specialtarget"] = tostring(targetSpecialTarget)
			RA.helperWindowTarget["tappedbyme"] = tostring(targetTappedByPlayer)
			RA.helperWindowTarget["tappedbyall"] = tostring(targetTappedByAll)
			RA.helperWindowTarget["attackp2t"] = tostring(targetAttackP2T)
			RA.helperWindowTarget["attackt2p"] = tostring(targetAttackT2P)
			RA.helperWindowTarget["los"] = tostring(targetLoS)
			RA.helperWindowTarget["deathin"] = targetDeathin
		end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: helperWindowTargetGUI
	Type: Diesal GUI config table
	Arguments:  None
	Return: None
	Description:
--]]
RA.helperWindowTargetGUI = ProbablyEngine.interface.buildGUI({
	key = "helperWindowTargetconfig",
	title = 'RotAgent',
	subtitle = 'Target Info',
	width = 175,
	height = 285,
	resize = true,
	color = "4e7300",
	config = {
		{ key = 'guid', type = "text", text = "random", size = 12, align = "left", offset = 14 },
		{ type = 'rule', },
		{ key = 'name', type = "text", text = "random", size = 12, align = "left", offset = 14 },
		{ key = 'distance', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'realdistance', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'reach', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'healthact', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'healthmax', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'healthpct', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'combat', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'reaction', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'specialaura', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'specialtarget', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'tappedbyme', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'tappedbyall', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'attackp2t', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'attackt2p', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'los', type = "text", text = "random", size = 12, align = "left", offset = 0 },
		{ key = 'deathin', type = "text", text = "random", size = 12, align = "left", offset = 0 },
	}
})
RA.helperWindowTargetGUI.parent:Hide()

--[[------------------------------------------------------------------------------------------------
	Name: RA.HelperWindowTargetShow
	Type: Function
	Arguments: None
	Return: None
	Description:
--]]
RA.helperWindowTargetShow = false
function RA.HelperWindowTargetShow()
	RA.helperWindowTargetShow = not RA.helperWindowTargetShow

	if RA.helperWindowTargetShow then
		RA.helperWindowTargetGUI.parent:Show()
	else
		RA.helperWindowTargetGUI.parent:Hide()
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: RA.HelperWindowTargetUpdate
	Type: Function
	Arguments:  None
	Return: None
	Description:
--]]
function RA.HelperWindowTargetUpdate()
	if RA.helperWindowTargetShow then
		local nameIndex = tostring(RA.helperWindowTarget["name"])
		local nameElement = ("\124cffFFFFFFname \124cff666666= \124cff4e7300"..nameIndex)
		local distanceIndex = tostring(RA.helperWindowTarget["distance"])
		local distanceElement = ("\124cffFFFFFFdistance \124cff666666= \124cff4e7300"..distanceIndex)
		local realDistanceIndex = tostring(RA.helperWindowTarget["realdistance"])
		local realDistanceElement = ("\124cffFFFFFFactual distance \124cff666666= \124cff4e7300"..realDistanceIndex)
		local reachIndex = tostring(RA.helperWindowTarget["reach"])
		local reachElement = ("\124cffFFFFFFreach \124cff666666= \124cff4e7300"..reachIndex)
		local healthActIndex = tostring(RA.helperWindowTarget["healthact"])
		local healthActElement = ("\124cffFFFFFFhealth actual \124cff666666= \124cff4e7300"..healthActIndex)
		local healthMaxIndex = tostring(RA.helperWindowTarget["healthmax"])
		local healthMaxElement = ("\124cffFFFFFFhealth max \124cff666666= \124cff4e7300"..healthMaxIndex)
		local healthPctIndex = tostring(RA.helperWindowTarget["healthpct"])
		local healthPctElement = ("\124cffFFFFFFhealth pct \124cff666666= \124cff4e7300"..healthPctIndex)
		local combatIndex = tostring(RA.helperWindowTarget["combat"])
		local combatElement = ("\124cffFFFFFFcombat \124cff666666= \124cff4e7300"..combatIndex)
		local reactionIndex = tostring(RA.helperWindowTarget["reaction"])
		local reactionElement = ("\124cffFFFFFFreaction \124cff666666= \124cff4e7300"..reactionIndex)
		local specialAuraIndex = tostring(RA.helperWindowTarget["specialaura"])
		local specialAuraElement = ("\124cffFFFFFFspecial aura \124cff666666= \124cff4e7300"..specialAuraIndex)
		local specialTargetIndex = tostring(RA.helperWindowTarget["specialtarget"])
		local specialTargetElement = ("\124cffFFFFFFspecial target \124cff666666= \124cff4e7300"..specialTargetIndex)
		local tappedByMeIndex = tostring(RA.helperWindowTarget["tappedbyme"])
		local tappedByMeElement = ("\124cffFFFFFFtapped by me \124cff666666= \124cff4e7300"..tappedByMeIndex)
		local tappedByAllIndex = tostring(RA.helperWindowTarget["tappedbyall"])
		local tappedByAllElement = ("\124cffFFFFFFtapped by all \124cff666666= \124cff4e7300"..tappedByAllIndex)
		local attackP2TIndex = tostring(RA.helperWindowTarget["attackp2t"])
		local attackP2TElement = ("\124cffFFFFFFyou can attack \124cff666666= \124cff4e7300"..attackP2TIndex)
		local attackT2PIndex = tostring(RA.helperWindowTarget["attackt2p"])
		local attackT2PElement = ("\124cffFFFFFFcan attack you \124cff666666= \124cff4e7300"..attackT2PIndex)
		local losIndex = tostring(RA.helperWindowTarget["los"])
		local losElement = ("\124cffFFFFFFline of sight \124cff666666= \124cff4e7300"..losIndex)
		local deathInIndex = tostring(RA.helperWindowTarget["deathin"])
		local deathInElement = ("\124cffFFFFFFdeath in \124cff666666= \124cff4e7300"..deathInIndex)

		RA.helperWindowTargetGUI.elements.guid:SetText(RA.helperWindowTarget["guid"])
		RA.helperWindowTargetGUI.elements.name:SetText(nameElement)
		RA.helperWindowTargetGUI.elements.distance:SetText(distanceElement)
		RA.helperWindowTargetGUI.elements.realdistance:SetText(realDistanceElement)
		RA.helperWindowTargetGUI.elements.reach:SetText(reachElement)
		RA.helperWindowTargetGUI.elements.healthact:SetText(healthActElement)
		RA.helperWindowTargetGUI.elements.healthmax:SetText(healthMaxElement)
		RA.helperWindowTargetGUI.elements.healthpct:SetText(healthPctElement)
		RA.helperWindowTargetGUI.elements.combat:SetText(combatElement)
		RA.helperWindowTargetGUI.elements.reaction:SetText(reactionElement)
		RA.helperWindowTargetGUI.elements.specialaura:SetText(specialAuraElement)
		RA.helperWindowTargetGUI.elements.specialtarget:SetText(specialTargetElement)
		RA.helperWindowTargetGUI.elements.tappedbyme:SetText(tappedByMeElement)
		RA.helperWindowTargetGUI.elements.tappedbyall:SetText(tappedByAllElement)
		RA.helperWindowTargetGUI.elements.attackp2t:SetText(attackP2TElement)
		RA.helperWindowTargetGUI.elements.attackt2p:SetText(attackT2PElement)
		RA.helperWindowTargetGUI.elements.los:SetText(losElement)
		RA.helperWindowTargetGUI.elements.deathin:SetText(deathInElement)
	end
end