--[[------------------------------------------------------------------------------------------------

LibCommands.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
SLASH_ROTAGENTCMD1 = "/ra"
function SlashCmdList.ROTAGENTCMD(msg, editbox)
	local command, moretext = msg:match("^(%S*)%s*(.-)$")
	command = string.lower(command)

	if msg == "" then
		print("RA "..RA.version)

	elseif command == "cast" then
		local spellName,_,_,_,_,_, spellID = GetSpellInfo(moretext)

		if spellName == moretext then
			RA.QueueSpell = spellID
		else
			print("Unknown 'cast' command " .. moretext)
		end


	elseif command == "debug" then
		if moretext ~= "" then
			RA.debugThisThing = moretext
			print("Now Debugging: " .. tostring(RA.debugThisThing))
		else
			RA.debugThisThing = nil
			print("Now Debugging: None")
		end


	elseif command == "fish" then
		Macro("/raf")


	elseif command == "info" then
		RA.HelperWindowCacheShow()
		RA.HelperWindowTargetShow()
		RA.HelperWindowVariablesShow()


	elseif command == "stats" then
		RA.BaseStatsPrint()


	elseif command == "symbols" then
		RA.HelperSymbolsToggle(moretext)


	elseif command == "tables" then
		RA.BuildTreeCacheAll()
		RA.BuildTreeCache()


	else
		RA.QueueSpell = nil
	end


	if RA.QueueSpell ~= nil then
		RA.QueueTime = GetTime()
	end
end