--[[------------------------------------------------------------------------------------------------

HelperWindowCache.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]


--[[------------------------------------------------------------------------------------------------
CACHE WINDOW FUNCTIONS
------------------------------------------------------------------------------------------------]]--
local L = ProbablyEngine.locale.get
local DiesalTools = LibStub("DiesalTools-1.0")
local DiesalStyle = LibStub("DiesalStyle-1.0")
local DiesalGUI = LibStub("DiesalGUI-1.0")
local DiesalMenu = LibStub("DiesalMenu-1.0")
local SharedMedia = LibStub("LibSharedMedia-3.0")

ProbablyEngine.helperWindowCacheGUI = { }
RA.helperWindowCacheGUI = { }

RA.helperWindowCacheGUI = DiesalGUI:Create('Window')
ProbablyEngine.helperWindowCacheGUI.window = RA.helperWindowCacheGUI

ProbablyEngine.interface.makeSavable(RA.helperWindowCacheGUI, 'RA.helperWindowCacheGUI')

RA.helperWindowCacheGUI:SetWidth(400)
RA.helperWindowCacheGUI:SetHeight(175)
RA.helperWindowCacheGUI:Hide()

RA.helperWindowCacheGUI:SetTitle("|cff4e7300RotAgent|r |cff7F7F7FCache Units:|r")

RA.helperWindowCacheScroll = DiesalGUI:Create('ScrollFrame')
RA.helperWindowCacheGUI:AddChild(RA.helperWindowCacheScroll)
RA.helperWindowCacheScroll:SetParent(RA.helperWindowCacheGUI.content)
RA.helperWindowCacheScroll:SetAllPoints(RA.helperWindowCacheGUI.content)
RA.helperWindowCacheScroll.parent = RA.helperWindowCacheGUI

RA.helperWindowCacheGUI:ApplySettings()
ProbablyEngine.interface.makeSavable(RA.helperWindowCacheGUI, 'RotAgent_CacheUnits')

local statusBars = { }
local statusBarsUsed = { }

local function getStatusBar()
    local statusBar = tremove(statusBars)
    if not statusBar then
        statusBar = DiesalGUI:Create('StatusBar')
        RA.helperWindowCacheScroll:AddChild(statusBar)
        statusBar:SetParent(RA.helperWindowCacheScroll.content)
        statusBar.frame:SetStatusBarColor(DiesalTools:GetColor("4e7300"))
    end
    statusBar:Show()
    table.insert(statusBarsUsed, statusBar)
    return statusBar
end

local function recycleStatusBars()
    for i = #statusBarsUsed, 1, -1 do
        statusBarsUsed[i]:Hide()
        tinsert(statusBars, tremove(statusBarsUsed))
    end
end

ProbablyEngine.timer.register("HelperWindowCacheUpdate", function()

    recycleStatusBars()

    local currentRow = 0

    RA.helperWindowCacheGUI:SetTitle("|cff4e7300RotAgent|r |cff7F7F7FCache Units:|r(" .. #RA.unitCache .. ") - |cff7F7F7FCacheAll Units:|r(" .. #RA.unitCacheAll .. ") - |cff7F7F7FUnitsAroundUnit:|r (" .. RA.uauCacheSize .. ")")

    if #RA.unitCache > 0 then
        for i = 1, #RA.unitCache do
            local statusBar = getStatusBar()
            local name = UnitName(RA.unitCache[i].object)
            local objectID = tostring(RA.unitCache[i].object)
            local health = UnitHealth(RA.unitCache[i].object)
            local maxHealth = UnitHealthMax(RA.unitCache[i].object)

            statusBar.frame:SetPoint("TOPRIGHT", RA.helperWindowCacheScroll.frame, "TOPRIGHT", -1, -1 + (currentRow * -15) + -currentRow )
            statusBar.frame:SetPoint("TOPLEFT", RA.helperWindowCacheScroll.frame, "TOPLEFT", 2, -1 + (currentRow * -15) + -currentRow )
            statusBar.frame.Left:SetText(" " .. string.sub(name, 1, 30) .. " - " .. objectID)

            if health and maxHealth then
                local percent = math.floor(((health / maxHealth) * 100))
                local remaining = math.floor(health / 1000)

                statusBar:SetValue(percent)
                if ProbablyEngine.module.combatTracker.ttd[RA.unitCache[i].object] then
                    seconds = ProbablyEngine.module.combatTracker.ttd[RA.unitCache[i].object]
                    deaht_in = string.format("%.2d:%.2d", seconds/60, seconds%60)
                else
                    deaht_in = "..."
                end
                statusBar.frame.Right:SetText(percent .. '%' .. ' ( ' .. deaht_in .. ' ) ')
            end

            statusBar.frame:SetScript("OnMouseDown", function(self) TargetUnit(RA.unitCache[i].object) end)
            currentRow = currentRow + 1
        end
    end
end, 100)

RA.helperWindowCacheShow = false
function RA.HelperWindowCacheShow()
    RA.helperWindowCacheShow = not RA.helperWindowCacheShow

    if RA.helperWindowCacheShow then
        RA.helperWindowCacheGUI:Show()
    else
        RA.helperWindowCacheGUI:Hide()
    end
end

RA.helperWindowCacheGUI.frame:SetPoint("TOPLEFT", RA.helperWindowTargetGUI.parent.frame, "BOTTOMLEFT", 0, -3)
RA.helperWindowCacheGUI.frame:SetPoint("TOPRIGHT", RA.helperWindowVariablesGUI.parent.frame, "BOTTOMRIGHT", 0, -3)