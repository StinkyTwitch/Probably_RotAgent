--[[------------------------------------------------------------------------------------------------

ClassHunter.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]


RA.hunterDefensives = {
	{ "#5512", {
		function() return RA.Eval("player.health") < RA.Fetch("healthpot", 40) end,
	}, },
	{ "#109223", {
		function() return RA.Eval("player.health") < RA.Fetch("healthpot", 40) end,
	}, },
	{ "Exhilaration", {
		function() return RA.Eval("player.health") < RA.Fetch("exhilaration", 50) end,
	}, },
	{ "Deterrence", {
		function() return RA.Eval("player.health") < RA.Fetch("deterrence", 10) end,
	}, },
	{ {
		{ "Master's Call", { "player.state.disorient", }, },
		{ "Master's Call", { "player.state.stun",  }, },
		{ "Master's Call", { "player.state.root",  }, },
		{ "Master's Call", { "!player.debuff(Dazed)", "player.state.snare", }, },
	}, {
		"pet.exists",
		function() return RA.Fetch("masterscall", false) == true end,
	}, },
}


RA.hunterMiscellaneousCombat = {
	{ "Aspect of the Cheetah", {
		function() return RA.Fetch("aspectincombat", true) == true end,
		"player.glyph(Glyph of Aspect of the Cheetah)",
		"!player.buff(Aspect of the Cheetah)",
	}, },
	{ "Concussive Shot", {
		function() return RA.Fetch("concussiveshot", true) == true end,
		"target.moving",
		"!target.immune.snare",
	}, },
	{ "Tranquilizing Shot", {
		function() return RA.Fetch("tranqshot", false) == true end,
		"target.dispellable(Tranquilizing Shot)",
	}, },
}


RA.hunterMiscellaneousOOC = {
	{ "Aspect of the Cheetah", {
		function() return RA.Fetch("autoaspect", false) == true end,
		function() return RA.Eval("player.movingfor > "..RA.Fetch('aspectmovingfor', 2)) end,
		"!player.buff(Aspect of the Cheetah)",
		"!player.buff(Aspect of the Pack)",
	}, },
	{ "Trap Launcher", "!player.buff(Trap Launcher)", },
	{ "Camouflage", {
		function() return RA.Fetch("camouflage", true) == true end,
		"!player.buff(Camouflage)",
		"player.glyph(Glyph of Camouflage)",
	}, },
}


RA.hunterMisdirection = {
	{ {
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'lalt' end,
			"modifier.lalt",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'lcontrol' end,
			"modifier.lcontrol",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'lshift' end,
			"modifier.lshift",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'ralt' end,
			"modifier.ralt",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'rcontrol' end,
			"modifier.rcontrol",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Fetch('mdkeybind', 'lalt') == 'rshift' end,
			"modifier.rshift",
		}, "focus", },
		{ "Misdirection", {
			function() return RA.Eval("target.threat") > RA.Fetch('mdfocusagro', 50) end,
		}, "focus", },
	}, {
		"!player.glyph(Glyph of Misdirection)",
		"!player.casting",
		"!player.channeling",
		"focus.exists",
		"!focus.enemy",
		"!focus.dead",
		"!focus.buff(Misdirection)",
	}, },

	{ {
		{ "Misdirection", {
			"!focus.exists",
		}, "pet", },
		{ "Misdirection", {
			"focus.exists",
			"focus.enemy",
		}, "pet", },
	}, {
		"player.glyph(Glyph of Misdirection)",
		"!player.casting",
		"!player.channeling",
		"pet.exists",
		"!pet.dead",
		"!pet.buff(Misdirection)",
		"player.combat",
	}, },
}


RA.hunterPetAttack = {
	{ {
		{ "/petattack focus", {
			"focus.exists",
			"focus.enemy",
			"!focus.racc",
			"timeout(petAttack, 1)",
		}, },
		{ "/petattack", {
			"!pettarget.exists",
			"target.exists",
			"!target.racc",
			"!focus.exists",
		}, },
		{ "/petattack", {
			"!pettarget.exists",
			"target.exists",
			"!target.racc",
			"focus.exists",
			"focus.friend",
		}, },
		{ "/cast Dash", {
			function() return RA.Eval("pet.radistancetotarget") > RA.Fetch('petdash', 15) end,
			"timeout(petDash, 1)",
		}, },
	}, "pet.exists", },


}

RA.hunterPetManagementCombat = {
	{ "Heart of the Phoenix", {
		"player.alive",
		"pet.dead",
		"!player.debuff(Weakened Heart)",
		"player.spell(Heart of the Phoenix).cooldown = 0",
	}, },
	{ "Revive Pet", {
		"player.alive",
		"pet.rapetdead",
	}, },
	{ "Mend Pet", {
		"player.alive",
		"pet.exists",
		"!pet.dead",
		"!pet.buff(Mend Pet)",
		function() return RA.Eval("pet.health") <= RA.Fetch("petmend", 95) end,
	}, },
}


RA.hunterPetManagementOOC = {
	{ "Dismiss Pet", {
		"ratalent(7,3,155228)",
		"pet.exists",
	}, },
	{ "Revive Pet", {
		"pet.rapetdead",
		"player.alive",
	}, },
	{ "Mend Pet", {
		"pet.exists",
		"!pet.dead",
		"!pet.buff(Mend Pet)",
		"player.alive",
		function() return RA.Eval("pet.health") <= RA.Fetch("petmend", 95) end,
	}, },
}


RA.hunterPetSlot = RA.hunterPetSlot or { }
function RA.HunterPetSlots()
	for i=1, 5 do
		local icon, name, level, family, talent = GetStablePetInfo(i)
		RA.hunterPetSlot[i] = name
	end
end


RA.hunterPetSummon = {
	{ {
		{ "883", { function() return RA.Fetch('summonslot', true) == 'slot1' end, }, },
		{ "83242", { function() return RA.Fetch('summonslot', true) == 'slot2' end, }, },
		{ "83243", { function() return RA.Fetch('summonslot', true) == 'slot3' end, }, },
		{ "83244", { function() return RA.Fetch('summonslot', true) == 'slot4' end, }, },
		{ "83245", { function() return RA.Fetch('summonslot', true) == 'slot5' end, }, },
	}, {
		function() return RA.Fetch('summonpet', true) == true end,
		"!ratalent(7,3,155228)",
		"!pet.exists",
		"!pet.rapetdead",
	}, },
}


RA.hunterPoolFocusCobraShot = {
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lalt' end,
		"modifier.lalt",
		"!talent(7,2)",
	}, },
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lcontrol' end,
		"modifier.lcontrol",
		"!talent(7,2)",
	}, },
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lshift' end,
		"modifier.lshift",
		"!talent(7,2)",
	}, },
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'ralt' end,
		"modifier.ralt",
		"!talent(7,2)",
	}, },
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rcontrol' end,
		"modifier.rcontrol",
		"!talent(7,2)",
	}, },
	{ "Cobra Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rshift' end,
		"modifier.rshift",
		"!talent(7,2)",
	}, },
}


RA.hunterPoolFocusFocusingShot = {
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lalt' end,
		"modifier.lalt",
		"talent(7,2)",
	}, },
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lcontrol' end,
		"modifier.lcontrol",
		"talent(7,2)",
	}, },
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lshift' end,
		"modifier.lshift",
		"talent(7,2)",
	}, },
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'ralt' end,
		"modifier.ralt",
		"talent(7,2)",
	}, },
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rcontrol' end,
		"modifier.rcontrol",
		"talent(7,2)",
	}, },
	{ "Focusing Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rshift' end,
		"modifier.rshift",
		"talent(7,2)",
	}, },
}


RA.hunterPoolFocusSteadyShot = {
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lalt' end,
		"modifier.lalt",
		"!talent(7,2)",
	}, },
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lcontrol' end,
		"modifier.lcontrol",
		"!talent(7,2)",
	}, },
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'lshift' end,
		"modifier.lshift",
		"!talent(7,2)",
	}, },
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'ralt' end,
		"modifier.ralt",
		"!talent(7,2)",
	}, },
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rcontrol' end,
		"modifier.rcontrol",
		"!talent(7,2)",
	}, },
	{ "Steady Shot", {
		function() return RA.Fetch('poolfocuskeybind', 'rcontrol') == 'rshift' end,
		"modifier.rshift",
		"!talent(7,2)",
	}, },
}


RA.hunterQueueSpells = {
	-- HUNTER TRAPS/GROUND SPELLS
	{ "Explosive Trap", { "@RA.Queue(82939)", "player.buff(Trap Launcher)", }, "mouseover.ground", },
	{ "Freezing Trap", { "@RA.Queue(60192)", "player.buff(Trap Launcher)", }, "mouseover.ground", },
	{ "Ice Trap", { "@RA.Queue(82941)", "player.buff(Trap Launcher)", }, "mouseover.ground", },
	{ "Binding Shot", "@RA.Queue(109248)", "mouseover.ground", },
	{ "Flare", "@RA.Queue(1543)", "mouseover.ground", },
	-- HUNTER GENERAL
	{ "A Murder of Crows", "@RA.Queue(131894)", },
	{ "Arcane Shot", "@RA.Queue(3044)", },
	{ "Barrage", { "@RA.Queue(120360)", }, },
	{ "Camouflage", "@RA.Queue(51753)", },
	{ "Concussive Shot", "@RA.Queue(5116)", },
	{ "Counter Shot", "@RA.Queue(147362)", },
	{ "Deterrence", "@RA.Queue(148467)", },
	{ "Distracting Shot", "@RA.Queue(20736)", },
	{ "!Feign Death", "@RA.Queue(5384)", },
	{ "Flare", "@RA.Queue(1543)", },
	{ "Focusing Shot", "@RA.Queue(152245)", },
	{ "Glaive Toss", { "@RA.Queue(117050)", }, },
	{ "Intimidation", "@RA.Queue(19577)", },
	{ "Master's Call", "@RA.Queue(53271)", },
	{ "Multi-Shot", { "@RA.Queue(2643)", }, },
	{ "Powershot", { "@RA.Queue(109259)", }, },
	{ "Stampede", "@RA.Queue(121818)", },
	{ "Tranquilizing Shot", "@RA.Queue(19801)", },
	{ "Wyvern Sting", "@RA.Queue(19386)", },
}


RA.hunterQueueSpellsBM = {
	{ RA.hunterQueueSpells, },
	-- HUNTER BEAST MASTERY
	{ "Bestial Wrath", "@RA.Queue(19574)", },
	{ "Cobra Shot", "@RA.Queue(77767)", },
	{ "Focus Fire", "@RA.Queue(82692)", },
	{ "Kill Command", "@RA.Queue(34026)", },
	{ "Kill Shot", "@RA.Queue(157708)", },
}


RA.hunterQueueSpellsMM = {
	{ RA.hunterQueueSpells, },
	-- HUNTER MARKSMANSHIOP
	{ "Aimed Shot", "@RA.Queue(19434)", },
    { "Chimaera Shot", "@RA.Queue(53209)", },
    { "Kill Shot", "@RA.Queue(157708)", },
    { "Rapid Fire", "@RA.Queue(3045)", },
}


RA.hunterQueueSpellsSV = {
	{ RA.hunterQueueSpells, },
	-- HUNTER SURVIVAL
	{ "Black Arrow", "@RA.Queue(3674)", },
	{ "Explosive Shot", "@RA.Queue(53301)", },
}