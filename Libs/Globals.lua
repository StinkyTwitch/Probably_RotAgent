--[[------------------------------------------------------------------------------------------------

Globals.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
_G["RA"] = _G["RA"] or { }
ProbablyEngine.library.register('RA', RA)










----------------------------------------------------------------------------------------------------
-- GLOBAL TABLES AND VARIABLES
----------------------------------------------------------------------------------------------------
RA.version = GetAddOnMetadata("Probably_RotAgent", "Version")
RA.executeUnit = RA.executeUnit or nil
RA.executeUnitPercentage = RA.executeUnitPercentage or nil
RA.lowestUnit = RA.lowestUnit or nil
RA.lowestUnitHealth = RA.lowestUnitHealth or nil
RA.highestUnit = RA.highestUnit or nil
RA.highestUnitHealth = RA.highestUnitHealth or nil
RA.nearestUnit = RA.nearestUnit or nil
RA.nearestUnitDistance = RA.nearestUnitDistance or nil

RA.specialBlackListTargets = {
	[ 60197] = "60197",  -- Scarlet Monastery Dummy
	[ 64446] = "64446",  -- Scarlet Monastery Dummy
	[ 76829] = "76829",  -- Slag Elemental (BrF - Blast Furnace)
	[ 78463] = "78463",  -- Slag Elemental (BrF - Blast Furnace)
	[ 93391] = "93391",  -- Captured Prisoner (HFC)
	[ 93392] = "93392",  -- Captured Prisoner (HFC)
	[ 93828] = "93828",  -- Training Dummy (HFC)
	[ 93926] = "93926",  -- Gul'dan (HFC, Mannaroth)
}

RA.specialCCDebuffs = {
	-- CROWD CONTROL
	[   118] = "118",    -- Polymorph: Sheep
	[  2094] = "2094",   -- Blind
	[  1776] = "1776",   -- Gouge
	[  3355] = "3355",   -- Freezing Trap
	[  6770] = "6770",   -- Sap
	[  9484] = "9484",   -- Shackle Undead
	[ 19386] = "19386",  -- Wyvern Sting
	[ 20066] = "20066",  -- Repentance
	[ 28271] = "28271",  -- Polymorph: Turtle
	[ 28272] = "28272",  -- Polymorph: Pig
	[ 51514] = "51514",  -- Hex
	[ 61305] = "61305",  -- Polymorph: Black Cat
	[ 61721] = "61721",  -- Polymorph: Rabbit
	[115078] = "115078", -- Paralysis
	[115268] = "115268", -- Mesmerize
	[126819] = "126819", -- Polymorph: Porcupine
	[161353] = "161353", -- Polymorph: Polar Bear Cub
	[161354] = "161354", -- Polymorph: Monkey
	[161355] = "161355", -- Polymorph: Penguin
	[161372] = "161372", -- Polymorph: Peacock
}

RA.specialEnemyTargets = {
	[ 31144] = "31144",  -- Training Dummy - Lvl 80 (Orgrimmar, Darnassus, Ruins of Gileas, ...)
	[ 31146] = "31146",  -- Raider's Training Dummy - Lvl ?? (Ogrimmar, Stormwind, Darnassus, ...)
	[ 32541] = "32541",  -- Initiate's Training Dummy - Lvl 55 (Scarlet Enclave)
	[ 32542] = "32542",  -- Disciple's Training Dummy - Lvl 65 (Eastern Plaguelands)
	[ 32543] = "32543",  -- Veteran's Training Dummy - Lvl 75 (Eastern Plaguelands)
	[ 32545] = "32545",  -- Initiate's Training Dummy - Lvl 55 (Eastern Plaguelands)
	[ 32546] = "32546",  -- Ebon Knight's Training Dummy - Lvl 80 (Eastern Plaguelands)
	[ 32666] = "32666",  -- Training Dummy - Lvl 60 (Orgrimmar, Ironforge, Darnassus, ...)
	[ 32667] = "32667",  -- Training Dummy - Lvl 70 (Darnassus, Silvermoon, Orgrimar, ...)

	[ 46647] = "46647",  -- Training Dummy - Lvl 85 (Orgrimmar, Stormwind)

	[ 52577] = "52577",  -- Left Foot (Lord Rhyolith - Firelands)
	[ 53087] = "53087",  -- Right Foot (Lord Rhyolith - Firelands)

	[ 60197] = "60197",  -- Scarlet Monastery Dummy
	[ 64446] = "64446",  -- Scarlet Monastery Dummy
	[ 67127] = "67127",  -- Training Dummy - Lvl 90 (Vale of Eternal Blossoms)

	[ 70245] = "70245",  -- Training Dummy - Lvl ?? (Throne of Thunder)
	[ 71075] = "71075",  -- Small Illusionary Banshee (Proving Grounds)
	[ 75966] = "75966",  -- Defiled Spirit (Shadowmoon Burial Grounds)
	[ 76220] = "76220",  -- Blazing Trickster (Auchindoun Normal)
	[ 76222] = "76222",  -- Rallying Banner (UBRS Black Iron Grunt)
	[ 76267] = "76267",  -- Solar Zealot (Skyreach)
	[ 76518] = "76518",  -- Ritual of Bones (Shadowmoon Burial Grounds)
	[ 76585] = "76585",  -- Ragewing (UBRS)
	[ 76598] = "76598",  -- Ritual of Bones ()
	[ 77252] = "77252",  -- Ore Crate (BRF Oregorger)
	[ 77665] = "77665",  -- Iron Soldier
	[ 77893] = "77893",  -- Grasping Earth (BRF Kromog)
	[ 78583] = "78583",  -- Dominator Turret (Iron Maiden)
	[ 78584] = "78584",  -- Dominator Turret (Iron Maiden)
	[ 79414] = "79414",  -- Training Dummy - Lvl 95 (Talador)
	[ 79504] = "79504",  -- Ore Crate (BRF Oregorger)
	[ 79511] = "79511",  -- Blazing Trickster (Auchindoun Heroic)

	[ 81638] = "81638",  -- Aqueous Globule (The Everbloom)
	[ 86644] = "86644",  -- Ore Crate (BRF Oregorger)
	[ 86752] = "86752",  -- Stone Pillars (BRF Mythic Kromog)
	[ 87317] = "87317",  -- Dungeoneer's Training Dummy - Lvl 102 (Lunarfall - Damage)
	[ 87318] = "87318",  -- Dungeoneer's Training Dummy - Lvl 102 (Lunarfall - Damage)
	[ 87320] = "87320",  -- Raider's Training Dummy - Lvl ?? (Stormshield - Damage)
	[ 87321] = "87321",  -- Training Dummy - Lvl 100 (Stormshield, Warspear - Healing)
	[ 87322] = "87322",  -- Dungeoneer's Training Dummy - Lvl 102 (Stormshield, Warspear - Tank)
	[ 87329] = "87329",  -- Raider's Training Dummy - Lvl ?? (Stormshield - Tank)
	[ 87761] = "87761",  -- Dungeoneer's Training Dummy - Lvl 102 (Frostwall - Damage)
	[ 87762] = "87762",  -- Raider's Training Dummy - Lvl ?? (Warspear - Damage)
	[ 88288] = "88288",  -- Dungeoneer's Training Dummy - Lvl 102 (Frostwall - Tank)
	[ 88314] = "88314",  -- Dungeoneer's Training Dummy - Lvl 102 (Lunarfall - Tank)
	[ 88835] = "88835",  -- Training Dummy - Lvl 100 (Warspear - Healing)
	[ 88836] = "88836",  -- Dungeoneer's Training Dummy - Lvl 102 (Warspear - Tank)
	[ 88837] = "88837",  -- Raider's Training Dummy - Lvl ?? (Warspear - Tank)
	[ 88906] = "88906",  -- Combat Dummy - Lvl 100 (Nagrand)
	[ 88967] = "88967",  -- Training Dummy - Lvl 100 (Lunarfall, Frostwall)
	[ 89078] = "89078",  -- Training Dummy - Lvl 100 (Lunarfall, Frostwall)

	[ 90387] = "90387",  -- Shadowy Construct (HFC)
	[ 90407] = "90407",  -- Hellblaze Imp (HFC, Kilrog)
	[ 90410] = "90410",  -- Felfire Crusher (HFC)
	[ 90411] = "90411",  -- Hellblaze Fiend (HFC, Kilrog)
	[ 90414] = "90414",  -- Hellblaze Mistress (HFC, Kilrog)
	[ 90432] = "90432",  -- Felfire Flamebelcher (HFC)
	[ 90485] = "90485",  -- Felfire Artillery (HFC)
	[ 90508] = "90508",  -- Gorebound Construct (HFC)
	[ 90568] = "90568",  -- Gorebound Essence (HFC)
	[ 91368] = "91368",  -- Crushing Hand (HFC)
	[ 91540] = "91540",  -- Illusionary Outcast (HFC)
	[ 92208] = "92208",  -- Doomfire Spirit (HFC - Archimonde)
	[ 93435] = "93435",  -- Felfire Transporter (HFC)
	[ 93717] = "93717",  -- Volatile Firebomb (HFC)
	[ 93851] = "93851",  -- Felfire Crusher (HFC)
	[ 93840] = "94840",  -- Felfire Artillery (HFC)
	[ 93838] = "93838",  -- Grasping Hand (HFC)
	[ 93839] = "93839",  -- Dragging Hand (HFC)
	[ 93851] = "93851",  -- Felfire Crusher (HFC)
	[ 94455] = "94455",  -- Blademaster Jubei'thos (HFC)
	[ 94865] = "94865",  -- Grasping Hand (HFC)
	[ 94873] = "94873",  -- Felfire Flamebelcher (HFC)
	[ 94955] = "94955",  -- Reinforced Firebomb (Iron Reaver, HFC)
	[ 94996] = "94996",  -- Fragment of the Crone (HFC)
	[ 95586] = "95586",  -- Felfire Demolisher (HFC)
	[ 95656] = "95656",  -- Carrion Swarm (Trash, HFC)
}

RA.specialHealingTargets = {
	-- TRAINING DUMMIES
	[ 88289] = "88289",  -- Training Dummy <Healing> HORDE GARRISON
	[ 88316] = "88316",  -- Training Dummy <Healing> ALLIANCE GARRISON
	-- WOD DUNGEONS/RAIDS
	[ 78868] = "78868",  -- Rejuvinating Mushroom (HM Brackenspore)
	[ 78884] = "78884",  -- Living Mushroom (HM Brackenspore)
}

RA.specialImmuneBuffs = {
	-- MOP DUNGEONS/RAIDS/ELITES
	[106062] = "106062", -- Water Bubble (Wise Mari)
	[110945] = "110945", -- Charging Soul (Gu Cloudstrike)
	[116994] = "116994", -- Unstable Energy (Elegon)
	[122540] = "122540", -- Amber Carapace (Amber Monstrosity - Heat of Fear)
	[123250] = "123250", -- Protect (Lei Shi)
	[143574] = "143574", -- Swelling Corruption (Immerseus)
	[143593] = "143593", -- Defensive Stance (General Nazgrim)
	-- WOD DUNGEONS/RAIDS/ELITES
	[155176] = "155176", -- Damage Shield (Primal Elementalists - BRF)
	[155185] = "155185", -- Cotainment (Primal Elementalists - BRF)
	[155233] = "155233", -- Dormant (Blast Furnace)
	[155265] = "155265", -- Cotainment (Primal Elementalists - BRF)
	[155266] = "155266", -- Cotainment (Primal Elementalists - BRF)
	[155267] = "155267", -- Cotainment (Primal Elementalists - BRF)
	[157289] = "157289", -- Arcane Protection (Imperator Mar'Gok)
	[174057] = "174057", -- Arcane Protection (Imperator Mar'Gok)
	[182055] = "182055", -- Full Charge (Iron Reaver - HFC)
	[184053] = "184053", -- Fel Barrier (Sargerei Dominator - HFC)
}










----------------------------------------------------------------------------------------------------
-- FUNCTIONS
----------------------------------------------------------------------------------------------------


--[[------------------------------------------------------------------------------------------------
	Name: AutoTargetClear
	Type: Function
	Arguments:  None
	Return: Guid or Object
	Description:
--]]
function RA.AutoTargetClear()
	if UnitExists("target") then
		if RA.Fetch("autocleartarget", true) == true then
			if UnitReaction("player", "target") > 4 then
				ClearTarget()
				return
			-- Do not Auto Target if Hunter is Feigned Death
			elseif RA.Eval("player.buff(Feign Death)") then
				ClearTarget()
				return
			end
		end
	else
		return
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: AutoTargetEnemy
	Type: Function
	Arguments:  None
	Return: Guid or Object
	Description:
--]]
function RA.AutoTargetEnemy()
	-- If Target exists and its NOT dead or a ghost return.
	if UnitExists("target") and not UnitIsDeadOrGhost("target") then
		return
	end

	-- Do not Auto Target if Hunter is Feigned Death
	if RA.Eval("player.buff(Feign Death)") then
		return
	end

	local algorithm = RA.Fetch("autotargetalgorithm", "highest")

	-- FireHack Unlocker
	if FireHack then
		if algorithm == "lowest" then
			if RA.lowestUnit ~= nil then
					TargetUnit(RA.lowestUnit)
					return
			else
				return
			end
		elseif algorithm == "highest" then
			if RA.highestUnit ~= nil then
					TargetUnit(RA.highestUnit)
					return
			else
				return
			end
		elseif algorithm == "nearest" then
			if RA.nearestUnit ~= nil then
					TargetUnit(RA.nearestUnit)
					return
			else
				return
			end
		elseif algorithm == "cascade" then
			for i=1, #RA.unitCache do
				if GetRaidTargetIndex(RA.unitCache[i].object) == 8 then
						TargetUnit(RA.unitCache[i].object)
						return
				end
			end
			if UnitExists("focus") and (UnitExists("focustarget") and UnitReaction("player", "focustarget") <= 4) then
					TargetUnit("focustarget")
					return
			else
				if RA.highestUnit ~= nil then
						TargetUnit(RA.highestUnit)
						return
				else
					return
				end
			end
		else
			return
		end
	-- Generic Unlocker
	else
		-- Target Focus Target if Focus' Target is an Enemy
		if UnitExists("focus") and reactionFocus > 4 then
			if UnitExists("focustarget") and reactionFocusTarget <= 4 then
				TargetUnit("focustarget")
				return
			end
		-- Target Nearest Enemy if valid Target
		else
			TargetNearestEnemy()
			if UnitExists("target") then
				local reaction = UnitReaction("player", "target")
				local specialEnemy = RA.SpecialEnemyTarget("target")
				local ccDebuf = RA.SpecialCCDebuff("target")
				local immuneBuff = RA.SpecialImmuneBuff("target")
				local tappedByPlayer = UnitIsTappedByPlayer("target")
				local tappedByAll = UnitIsTappedByAllThreatList("target")

				if reaction <= 4
					and not ccDebuf
					and not immuneBuff
					and (tappedByPlayer or tappedByAll or specialEnemy)
				then
					return
				else
					ClearTarget()
					return
				end
			end
		end
	end
	return
end


--[[------------------------------------------------------------------------------------------------
	Name: BaseStatsInit
	Type: Function
	Arguments:  None
	Return: None
	Description:
--]]
RA.baseStats = { }
function RA.BaseStatsInit()
	RA.baseStats.strength = UnitStat("player", 1)
	RA.baseStats.agility = UnitStat("player", 2)
	RA.baseStats.stamina = UnitStat("player", 3)
	RA.baseStats.intellect = UnitStat("player", 4)
	RA.baseStats.spirit = UnitStat("player", 5)
	RA.baseStats.crit = GetCritChance()
	RA.baseStats.haste = GetHaste()
	RA.baseStats.mastery = GetMastery()
	RA.baseStats.multistrike = GetMultistrike()
	RA.baseStats.versatility = GetCombatRating(29)
	print("RA BaseStatsInit")
end


--[[------------------------------------------------------------------------------------------------
	Name: BaseStatsPrint
	Type: Function
	Arguments:  None
	Return: None
	Description: Prints to chat frame the stat numbers for the base stats being tracked.
--]]
function RA.BaseStatsPrint()
	print("Strength: "..RA.baseStats.strength)
	print("Agility: "..RA.baseStats.agility)
	print("Stamina: "..RA.baseStats.stamina)
	print("Intellect: "..RA.baseStats.intellect)
	print("Spirit: "..RA.baseStats.spirit)
	print("Crit: "..RA.baseStats.crit)
	print("Haste: "..RA.baseStats.haste)
	print("Mastery: "..RA.baseStats.mastery)
	print("Multistrike: "..RA.baseStats.multistrike)
	print("Versatility: "..RA.baseStats.versatility)
end


--[[------------------------------------------------------------------------------------------------
	Name: BaseStatsUpdate
	Type: Function
	Arguments:  None
	Return: None
	Description:
--]]
function RA.BaseStatsUpdate()
	if not UnitAffectingCombat("player") then
		if RA.baseStats.strength ~= UnitStat("player", 1) then RA.baseStats.strength = UnitStat("player", 1) end
		if RA.baseStats.agility ~= UnitStat("player", 2) then RA.baseStats.agility = UnitStat("player", 2) end
		if RA.baseStats.stamina ~= UnitStat("player", 3) then RA.baseStats.stamina = UnitStat("player", 3) end
		if RA.baseStats.intellect ~= UnitStat("player", 4) then RA.baseStats.intellect = UnitStat("player", 4) end
		if RA.baseStats.spirit ~= UnitStat("player", 5) then RA.baseStats.spirit = UnitStat("player", 5) end
		if RA.baseStats.crit ~= GetCritChance() then RA.baseStats.crit = GetCritChance()end
		if RA.baseStats.haste ~= GetHaste() then RA.baseStats.haste = GetHaste() end
		if RA.baseStats.mastery ~= GetMastery() then RA.baseStats.mastery = GetMastery() end
		if RA.baseStats.multistrike ~= GetMultistrike() then RA.baseStats.multistrike = GetMultistrike() end
		if RA.baseStats.versatility ~= GetCombatRating(29) then RA.baseStats.versatility = GetCombatRating(29) end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name:
	Type:
	Arguments:
	Return:
	Description:
--]]
function RA.BuildTreeCache()
	local DiesalGUI = LibStub("DiesalGUI-1.0")
	local explore = DiesalGUI:Create('TableExplorer')
	explore:SetTable("Unit Cache", RA.unitCache)
	explore:BuildTree()
end


--[[------------------------------------------------------------------------------------------------
	Name:
	Type:
	Arguments:
	Return:
	Description:
--]]
function RA.BuildTreeCacheAll()
	local DiesalGUI = LibStub("DiesalGUI-1.0")
	local explore = DiesalGUI:Create('TableExplorer')
	explore:SetTable("Unit Cache All", RA.unitCacheAll)
	explore:BuildTree()
end


--[[------------------------------------------------------------------------------------------------
	Name: RA.CarefulAim
	Type: Function
	Arguments: None
	Return: Boolean
	Description:
--]]
function RA.CarefulAim()
	if RA.Eval("target.health >= 80") then
		RA.Debug("RA.CarefulAim()1", "target.health >= 80%!")
		return true

	elseif RA.Eval("player.buff(Rapid Fire)") then
		RA.Debug("RA.CarefulAim()2", "buff Rapid Fire found!")
        return true

    elseif RA.Fetch("noprioritize", true) == true then
        for i=1,#RA.unitCache do
            if RA.unitCache[i].health >= 80 then
            	RA.Debug("RA.CarefulAim()3", "" .. RA.unitCache[i].object .. " >= 80%")
                return true
            end
        end
    else
    	RA.Debug("RA.CarefulAim()4", "no Units meet requirements!")
    	return false
    end

    RA.Debug("RA.CarefulAim()5", "you should not be here ever!")
    return false
end


--[[------------------------------------------------------------------------------------------------
	Name: Cluster
	Type: Function
	Arguments:
	Return:
	Description:
--]]
function RA.Cluster(radius, numUnits, spell)
	RA.Debug("RA.Cluster()1", "register entry!")

	radius = tonumber(radius)
	numUnits = tonumber(numUnits)
	spell = tostring(spell)

	if RA.Fetch("nocluster", true) == true then
		RA.Debug("RA.Cluster()2", "cluster is off!")
		return true
	end

	local spellName = GetSpellInfo(spell)
	local spellUsable, spellNoMana = IsUsableSpell(spellName)

	if spellUsable then
		local start, duration, enable = GetSpellCooldown(spellName)
	end

	local casting = UnitCastingInfo("player")
	local channeling = UnitChannelInfo("player")

	if spellNoMana then
		RA.Debug("RA.Cluster()4", "Not enough power!")
		return true
	elseif duration ~= 0 then
		RA.Debug("RA.Cluster()5", "Spell not ready!")
		return true
	elseif casting ~= nil then
		RA.Debug("RA.Cluster()6", "Player casting!")
		return true
	elseif channeling ~= nil then
		RA.Debug("RA.Cluster()7", "Player channeling!")
		return true
	end

	local pivotTarget = nil
	local largestCluster = 0
	local unitsInCluster = 0

	for i = 1, #RA.unitCache do
		local unitsInCluster = 0

		for j = 1, #RA.unitCache do
			local clusterDistance = RA.Distance(RA.unitCache[i].object, RA.unitCache[j].object, 2, "reach")

			if clusterDistance <= radius then
				unitsInCluster = unitsInCluster + 1
			end
		end
		if unitsInCluster >= largestCluster and unitsInCluster >= numUnits then
			largestCluster = unitsInCluster
			pivotTarget = i
		end
	end

	if pivotTarget ~= nil and not UnitIsDead(RA.unitCache[pivotTarget].object) then
		RA.Debug("RA.Cluster()8", "Pivot Target found!")

		if spellName == "Explosive Trap" then
			--if not ProbablyEngine.condition["moving"](RA.unitCache[pivotTarget].object) then
			if not RA.Eval("target.moving") then
				RA.Debug("RA.Cluster()9", "Explosive Trap!")
				RA.drawExplosiveTarget = RA.unitCache[pivotTarget].object
				CastGround("Explosive Trap", RA.unitCache[pivotTarget].object)
				return true
			end

		elseif spellName == "Multi-Shot" then
			if not UnitIsUnit("pettarget", RA.unitCache[pivotTarget].object) then
				RA.Debug("RA.Cluster()10", "Multi-Shot!")
				RA.drawMultiShotTarget = RA.unitCache[pivotTarget].object
				Cast("Multi-Shot", RA.unitCache[pivotTarget].object)
				PetAttack(RA.unitCache[pivotTarget].object)
				return true

			else
				RA.Debug("RA.Cluster()11", "Multi-Shot!")
				RA.drawMultiShotTarget = RA.unitCache[pivotTarget].object
				Cast("Multi-Shot", RA.unitCache[pivotTarget].object)
				return true

			end

		elseif spellName == "Barrage" then
			RA.Debug("RA.Cluster()12", "Barrage!")
			RA.drawBarrageTarget = RA.unitCache[pivotTarget].object
			Cast("Barrage", RA.unitCache[pivotTarget].object)
			return true

		elseif spellName == "Chimaera Shot" then
			RA.Debug("RA.Cluster()13", "Chimaera Shot!")
			RA.drawChimaeraTarget = RA.unitCache[pivotTarget].object
			Cast("Chimaera Shot", RA.unitCache[pivotTarget].object)
			return true

		else
			RA.Debug("RA.Cluster()14", "ParsedTarget!")
			RA.drawParsedTarget = RA.unitCache[pivotTarget].object
			ProbablyEngine.dsl.parsedTarget = RA.unitCache[pivotTarget].object
			return true
		end
	end

	RA.Debug("RA.Cluster()15", "you should not be here!")
	return true
end


--[[------------------------------------------------------------------------------------------------
	Name: ClusterSize
	Type: Function
	Arguments:
	Return:
	Description:
--]]
function RA.ClusterSize(radius, numUnits)
	RA.Debug("RA.ClusterSize()1", "entry!")

	if #RA.unitCache >= 1 then
		local radius = tonumber(radius)
		local largestCluster = 0
		local unitsInCluster = 0

		for i=#RA.unitCache, 1, -1 do
			local unitsInCluster = 0

			for j=#RA.unitCache, 1, -1 do
				local clusterDistance = Distance(RA.unitCache[i].pointer, RA.unitCache[j].pointer)
				if clusterDistance <= radius then
					unitsInCluster = unitsInCluster + 1
				end
			end

			if unitsInCluster >= largestCluster then
				largestCluster = unitsInCluster
			end
		end

		if largestCluster == 1 and numUnits == 1 then
			return true
		elseif largestCluster == 2 and numUnits == 2 then
			return true
		elseif largestCluster >= 3 and numUnits >= 3 then
			return true
		end

	else
		return false

	end
end


--[[------------------------------------------------------------------------------------------------
	Name: CombatReach
	Type: Function
	Arguments:  unit - Valid in game unit
	Return: number representing the units combat reach in yards
	Description:
--]]
function RA.CombatReach(unit)
	RA.Debug("RA.CombatReach()1", "function entry!")
	if not UnitExists(unit) then
		RA.Debug("RA.CombatReach()2", "Unit does not exist!")
		return 0
	end

	if FireHack then
		unit = UnitGUID(unit)
		object = GetObjectWithGUID(unit)
		local distance = UnitCombatReach(object)

		RA.Debug("RA.CombatReach()3", "return combat reach (" .. distance .. ")")
		return distance
	else
		RA.Debug("RA.CombatReach()4", "function entry!")
		return 0
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: Debug
	Type: Function
	Arguments:  logLevel - Arbitrary level assigned to this print statement.
				string - String to print if debugging is enabled.
				name - The name associated with this specific call to Debug.
	Return: Nothing
	Description: Debug will print out a supplied string if debugging is enabled AND the supplied
	level is equal to or higher than the configured logging level. Print output is throttle by
	the variable printThrottle. Default is 1 second. This keeps the chat frame from being spammed.
--]]
RA.debugThisThing = nil
RA.debugTrack = RA.debugTrack or { }
function RA.Debug(name, message)
	local debugToggle = RA.Fetch("debug_check", false)

	if not debugToggle or RA.debugThisThing == nil then
		return
	end

    name = tostring(name)
    message = tostring(message)
    local search = tostring(RA.debugThisThing)
    local printThrottle = 1
    local debugThrottle = RA.Fetch("debug_spin", 0)

    if string.find(name, search) then
        if RA.debugTrack[name] then
            if (GetTime() - RA.debugTrack[name].start) >= debugThrottle then
                RA.debugTrack[name].start = GetTime()
                if debugToggle then
					print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
                end
            end
        else
            RA.debugTrack[name] = { }
            RA.debugTrack[name].start = GetTime()
            if debugToggle then
                print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
            end
        end
    elseif RA.debugThisThing == "All" or RA.debugThisThing == "all" then
        if RA.debugTrack[name] then
            if (GetTime() - RA.debugTrack[name].start) >= debugThrottle then
                RA.debugTrack[name].start = GetTime()
                if debugToggle then
                    print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
                end
            end
        else
            RA.debugTrack[name] = { }
            RA.debugTrack[name].start = GetTime()
            if debugToggle then
                print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
            end
        end
    end
end


--[[------------------------------------------------------------------------------------------------
	Name: Distance
	Type: Function
	Arguments:  a - Point 1
				b - Point 2
				precision - Number of decimal places to track
				reach - Use combat reach in the factoring of distance
	Return: Distance
	Description:
--]]
function RA.Distance(a, b, precision, reach)
	RA.Debug("RA.Distance()1", "function entry!")

	if not UnitExists(a) or not UnitExists(b) or not UnitIsVisible(a) or not UnitIsVisible(b) then
		RA.Debug("RA.Distance()2", "unit (" .. tostring(a) .. ") or unit (" .. tostring(b) .. ") does not exist")
		return 0
	end

	a = GetObjectWithGUID(UnitGUID(a))
	b = GetObjectWithGUID(UnitGUID(b))

	if FireHack then
		local ax, ay, az = ObjectPosition(a)
		local bx, by, bz = ObjectPosition(b)
		RA.Debug("RA.Distance()3", "" .. ax .. "," .. ay .. "," .. az .. "," .. bx .. "," .. by .. "," .. bz .. "")

		if precision then
			if reach then
				local value = math.sqrt(((bx-ax)^2) + ((by-ay)^2) + ((bz-az)^2)) - ((RA.CombatReach(a)) + (RA.CombatReach(b)))
				local valuePrecision = math.floor( (value * 10^precision) + 0.5) / (10^precision)

				RA.Debug("RA.Distance()4", valuePrecision)
				return valuePrecision
			else
				local value = math.sqrt(((bx-ax)^2) + ((by-ay)^2) + ((bz-az)^2))
				local valuePrecision = math.floor( (value * 10^precision) + 0.5) / (10^precision)

				RA.Debug("RA.Distance()5", valuePrecision)
				return valuePrecision
			end
		else
			if reach then
				local value = math.sqrt(((bx-ax)^2) + ((by-ay)^2) + ((bz-az)^2)) - ((RA.CombatReach(a)) + (RA.CombatReach(b)))

				RA.Debug("RA.Distance()6", value)
				return value
			else
				local value = math.sqrt(((bx-ax)^2) + ((by-ay)^2) + ((bz-az)^2))

				RA.Debug("RA.Distance()7", value)
				return value
			end
		end
	else
		RA.Debug("RA.Distance()8", "FireHack not loaded, return 0")
		return 0
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: Eval
--]]
function RA.Eval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end


--[[------------------------------------------------------------------------------------------------
	Name: Eval
--]]
function RA.Facing(a, b)
	RA.Debug("RA.Facing()1", "function entry!")

	if not UnitExists(a) or not UnitExists(b) then
		RA.Debug("RA.Facing()2", "unit ("..a..") or unit ("..b..") does not exist!")
	else
		if ObjectIsFacing(a, b) then
			RA.Debug("RA.Facing()3", "unit("..a..") is facing unit("..b..")")
			return true
		else
			RA.Debug("RA.Facing()4", "unit("..a..") is NOT facing unit("..b..")")
			return false
		end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: Fetch
--]]
function RA.Fetch(key, default)
	if ProbablyEngine.rotation.currentStringComp == "RA - Combat" then
		return ProbablyEngine.interface.fetchKey('ra_config_r_combat', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Frost DW" then
		return ProbablyEngine.interface.fetchKey('ra_config_dk_fdw', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Beast Mastery" then
		return ProbablyEngine.interface.fetchKey('ra_config_h_bm', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Marksmanship" then
		return ProbablyEngine.interface.fetchKey('ra_config_h_mm', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Survival" then
		return ProbablyEngine.interface.fetchKey('ra_config_h_sv', key, default)
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: LineOfSight
	Type: Function
	Arguments: None
	Return: None
	Description:
--]]
function RA.LineOfSight(a, b)
	RA.Debug("RA.LineOfSight()1", "function entry!")

	if not UnitExists(a) or not UnitExists(b) then
		RA.Debug("RA.LineOfSight()2", "unit ("..a..") or unit ("..b..") does not exist!")
	else
		-- Ignore Line of Sight on these units with very weird combat.
		local ignoreLOS = {
			[76585] = true,     -- Ragewing the Untamed (UBRS)
			[77063] = true,     -- Ragewing the Untamed (UBRS)
			[77182] = true,     -- Oregorger (BRF)
			[77891] = true,     -- Grasping Earth (BRF)
			[77893] = true,     -- Grasping Earth (BRF)
			[78981] = true,     -- Iron Gunnery Sergeant (BRF)
			[81318] = true,     -- Iron Gunnery Sergeant (BRF)
			[83745] = true,     -- Ragewing Whelp (UBRS)
			[86252] = true,     -- Ragewing the Untamed (UBRS)
		}

		local aCheck = select(6,strsplit("-",UnitGUID(a)))
		local bCheck = select(6,strsplit("-",UnitGUID(b)))

		if ignoreLOS[tonumber(aCheck)] ~= nil or ignoreLOS[tonumber(bCheck)] ~= nil then
			RA.Debug("RA.LineOfSight()3", "ignoreLOS unit found!")
			return true
		end

		if FireHack then
			local losFlags =  bit.bor(0x10, 0x100)
			local ax, ay, az = ObjectPosition(a)
			local bx, by, bz = ObjectPosition(b)

			if ax == nil or ay == nil or az == nil or bx == nil or by == nil or bz == nil then
				RA.Debug("RA.LineOfSight()4", "one or more coordinates is nil!")
				return false
			end
			if TraceLine(ax, ay, az+2.25, bx, by, bz+2.25, losFlags) then
				RA.Debug("RA.LineOfSight()5", "units are NOT IN Line of Sight!")
				return false
			end
		end

		RA.Debug("RA.LineOfSight()6", "units are IN Line of Sight!")
		return true
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: NotificationUpdate
	Arguments: self - Reference to frame
			   elapsed - Time passed
	Return: Nothing
	Description: Each onUpdate checks if the frame is older than 2 seconds. If it is, then check if
	the Frames alpha is 0, fully invisibile. If its invisible then Hide the frame. Else we slowly
	start to fade the frame out 0.05 each run through onUpdate.
--]]
function RA.NotificationUpdate(self, elapsed)
	if RA.notificationFrame.time < GetTime() - 2.0 then
		if RA.notificationFrame:GetAlpha() == 0 then
			RA.notificationFrame:Hide()
		else
			RA.notificationFrame:SetAlpha(RA.notificationFrame:GetAlpha() - .05)
		end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: NotificationInit
	Type: Function
	Arguments: None
	Return: Nothing
	Description:
--]]
function RA.NotificationInit()
	RA.notificationFrame = CreateFrame("Frame",nil,UIParent)
	RA.notificationFrame:SetSize(300,50)
	RA.notificationFrame:Hide()
	RA.notificationFrame:SetScript("onUpdate", RA.NotificationUpdate)
	RA.notificationFrame:SetPoint("CENTER", 0, -200)
	RA.notificationFrame.text = RA.notificationFrame:CreateFontString(nil,"OVERLAY","MovieSubtitleFont")
	RA.notificationFrame.text:SetTextColor(0.67, 0.83, 0.45)
	RA.notificationFrame.text:SetTextHeight(24)
	RA.notificationFrame.text:SetAllPoints()
	RA.notificationFrame.texture = RA.notificationFrame:CreateTexture()
	RA.notificationFrame.texture:SetAllPoints()
	RA.notificationFrame.texture:SetTexture(0,0,0,0)
	RA.notificationFrame.time = 0
end
RA.NotificationInit()


--[[------------------------------------------------------------------------------------------------
	Name: notificationFrame:message
	Type: Function
	Arguments: message - String passed to function to display
	Return: Nothing
	Description: Set the text for the frame with 'message'. Then set the alpha for the frame to 1,
	fully visible. Next set the timer for the start of the message to now, GetTime. Lastly show the
	frame.
--]]
function RA.notificationFrame:message(message)
	self.text:SetText(message)
	self:SetAlpha(1)
	self.time = GetTime()
	self:Show()
end


--[[------------------------------------------------------------------------------------------------
	Name: OptionsWindowShow
	Type: Function
	Arguments: None
	Return: None
	Description:
--]]
RA.optionsWindowShow = false
function RA.OptionsWindowShow()
	RA.optionsWindowShow = not RA.optionsWindowShow

	if RA.optionsWindowShow then
		if ProbablyEngine.rotation.currentStringComp == "RA - Combat" then
			RA.optionsWindowRCOMBAT.parent:Show()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Frost DW" then
			RA.optionsWindowDKFDW.parent:Show()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Beast Mastery" then
			RA.optionsWindowHBM.parent:Show()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Marksmanship" then
			RA.optionsWindowHMM.parent:Show()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Survival" then
			RA.optionsWindowHSV.parent:Show()
		end
	else
		if ProbablyEngine.rotation.currentStringComp == "RA - Combat" then
			RA.optionsWindowRCOMBAT.parent:Hide()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Frost DW" then
			RA.optionsWindowDKFDW.parent:Hide()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Beast Mastery" then
			RA.optionsWindowHBM.parent:Hide()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Marksmanship" then
			RA.optionsWindowHMM.parent:Hide()
		elseif ProbablyEngine.rotation.currentStringComp == "RA - Survival" then
			RA.optionsWindowHSV.parent:Hide()
		end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: Pause
	Type: Function
	Arguments: None
	Return: None
	Description:
--]]
function RA.Pause()
	RA.Debug("RA.Pause()1", "function entry!")

    if FireHack then
    	local hack = IsHackEnabled("AlwaysFacing")
        if RA.Eval("!player.rafacing(target)") and not hack then
        	RA.Debug("RA.Pause()2", "player not facing target!")
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("!player.ralineofsight(target)") then
        	RA.Debug("RA.Pause()3", "player not in los of target!")
            SpellStopCasting()
            StopAttack()
            return true
        end
    end

    if RA.Eval("target.racc") then
    	RA.Debug("RA.Pause()4", "target is crowd controlled!")
        PetFollow()
        SpellStopCasting()
        StopAttack()
        return true
    elseif RA.Eval("player.buff(Feign Death)") then
		RA.Debug("RA.Pause()5", "player is feigned death!")
		PetFollow()
		SpellStopCasting()
		StopAttack()
		ClearTarget()
		return
    elseif RA.Fetch("pausekeybind") ~= "none" then
        if RA.Eval("modifier.lalt") and RA.Fetch("pausekeybind", "lshift") == "lalt" then
        	RA.Debug("RA.Pause()6", "lalt pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("modifier.lcontrol") and RA.Fetch("pausekeybind", "lshift") == "lcontrol" then
        	RA.Debug("RA.Pause()7", "lcontrol pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("modifier.lshift") and RA.Fetch("pausekeybind", "lshift") == "lshift" then
        	RA.Debug("RA.Pause()8", "lshift pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("modifier.ralt") and RA.Fetch("pausekeybind", "lshift") == "ralt" then
        	RA.Debug("RA.Pause()9", "ralt pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("modifier.rcontrol") and RA.Fetch("pausekeybind", "lshift") == "rcontrol" then
        	RA.Debug("RA.Pause()10", "rcontrol pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        elseif RA.Eval("modifier.rshift") and RA.Fetch("pausekeybind", "lshift") == "rshift" then
        	RA.Debug("RA.Pause()11", "rshift pressed for pause!")
            PetFollow()
            SpellStopCasting()
            StopAttack()
            return true
        end
    else
    	RA.Debug("RA.Pause()12", "pausekeybind config key set to none!")
        return false
    end
end


--- Get the position between two objects that is a specified distance from one of the objects.
-- @param Distance The distance from the first object that the position is.
-- @param First The first object.
-- @param Second The second object. The player if not passed.
-- @return The X, Y, and Z coordinates of the position between the two objects that is the distance from the first object.
function RA.PositionBetweenObjects (distance, a, b)
	RA.Debug("RA.PositionBetweenObjects()1", "function entry!")

	local ax, ay, az = ObjectPosition(a)
	local bx, by, bz = ObjectPosition(b or "Player")
	RA.Debug("RA.PositionBetweenObjects()2", "" .. fx .. "," .. fy .. "," .. fz .. "," .. sx .. "," .. sy .. "," .. sz .. "")

	local facing = math.atan2(by - ay, bx - ax) % (math.pi * 2)
	local pitch = math.atan((bz - az) / math.sqrt(((ax - bx) ^ 2) + ((ay - by) ^ 2))) % (math.pi * 2)
	RA.Debug("RA.PositionBetweenObjects()3", "Facing (" .. facing .. ") Pitch (" .. pitch .. ")")

	local rx = math.cos(facing) * distance + ax
	local ry = math.sin(facing) * distance + ay
	local rz = math.sin(pitch) * distance + az

	RA.Debug("RA.PositionBetweenObjects()4", "" .. rx .. "," .. ry .. "," .. rz)
	return rx, ry, rz
end


--[[------------------------------------------------------------------------------------------------
	Name: Queue
	Type: Function
	Arguments: spellID
	Return: Boolean
	Description: If the current time is greater than 4 seconds then reset the queue timer and
	queued spell variables and return false. Otherwise if the queued spell called by a macro is the
	same as the spell we are checking for in the rotation then, if the player's last casted spell
	is the spell we are trying to queue reset the queue timer and queued spell variables. Otherwise
	send a notification message to the screen and return true to the rotation line that called this
	function. Otherwise return false.
--]]
RA.QueueSpell = RA.QueueSpell or nil
RA.QueueTime = RA.QueueTime or 0
function RA.Queue(spellID)
	RA.Debug("RA.Queue()1", "function entry!")

	if (GetTime() - RA.QueueTime) > 4 then
		RA.Debug("RA.Queue()2", "RA.QueueTime(" .. RA.QueueTime .. ") > 4 seconds!")
		RA.QueueSpell = nil
		RA.QueueTime = 0
		return false
	else
		if RA.QueueSpell then
			if RA.QueueSpell == spellID then
				RA.Debug("RA.Queue()3", "" .. RA.QueueSpell .. " == " .. spellID)

				if ProbablyEngine.parser.lastCast == GetSpellName(spellID) then
					RA.Debug("RA.Queue()4", "" .. ProbablyEngine.parser.lastCast .. " == " .. GetSpellName(spellID))

					RA.QueueSpell = nil
					RA.QueueTime = 0
				end

				RA.notificationFrame:message(GetSpellName(spellID).." Queued")
				return true
			else
				return false
			end
		end
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: SpecialBlacklistTarget
	Type: Function
	Arguments: unit - Valid game unit
	Return: Boolean
	Description: If the UnitID matches a Special Enemy Unit in the table then return true.
	Otherwise the unit is not a Special Enemy.
--]]
function RA.SpecialBlackListTarget(unit)
	if not UnitExists(unit) then
		return false
	end
	RA.Debug("RA.SpecialBlackListTarget()2", unit)

	local _,_,_,_,_,unitID = strsplit("-", UnitGUID(unit))
	if RA.specialBlackListTargets[tonumber(unitID)] ~= nil then
		RA.Debug("RA.SpecialBlackListTarget()3", "found "..unitID)
		return true
	else
		return false
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: SpecialCCDebuff
	Type: Function
	Arguments: unit - Valid game unit
	Return: Boolean
	Description: Run through the specialCCDebuffs table checking to see if the supplied 'unit'
	has a matching debuff. If a match is found return true, otherwise return false.
--]]
function RA.SpecialCCDebuff(unit)
	if not UnitExists(unit) then
		return false
	end
	RA.Debug("RA.SpecialCCDebuff()2", unit)

	for k,v in pairs(RA.specialCCDebuffs) do
		debuff = GetSpellInfo(k)
		if UnitDebuff(unit, debuff) then
			RA.Debug("RA.SpecialCCDebuff()3", "found "..debuff.." on "..unit)
			return true
		end
	end
	return false
end


--[[------------------------------------------------------------------------------------------------
	Name: SpecialEnemyTarget
	Type: Function
	Arguments: unit - Valid game unit
	Return: Boolean
	Description: If the UnitID matches a Special Enemy Unit in the table then return true.
	Otherwise the unit is not a Special Enemy.
--]]
function RA.SpecialEnemyTarget(unit)
	if not UnitExists(unit) then
		return false
	end
	RA.Debug("RA.SpecialEnemyTarget()2", unit)

	local _,_,_,_,_,unitID = strsplit("-", UnitGUID(unit))
	if RA.specialEnemyTargets[tonumber(unitID)] ~= nil then
		RA.Debug("RA.SpecialEnemyTarget()3", "found "..unitID)
		return true
	else
		return false
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: SpecialImmuneBuff
	Type: Function
	Arguments: unit - Valid game unit
	Return: Boolean
	Description: Run through the specialImmuneBuffs table checking to see if the supplied 'unit'
	has a matching buff. If a match is found return true, otherwise return false.
--]]
function RA.SpecialImmuneBuff(unit)
	if not UnitExists(unit) then
		return false
	end
	RA.Debug("RA.SpecialImmuneBuff()2", unit)

	for k,v in pairs(RA.specialImmuneBuffs) do
		buff = GetSpellInfo(k)
		if UnitBuff(unit, buff) then
			RA.Debug("RA.SpecialImmuneBuff()3", "found "..buff)
			return true
		end
	end
	return false
end


--[[------------------------------------------------------------------------------------------------
    Name: SplashFrameHide
    Type: Function
    Arguments: None
    Return: Nothing
    Description:
--]]
function RA.SplashFrameHide()
    C_Timer.NewTimer(1, function()
        if RA.splashFrame.hide then
            UIFrameFadeOut(RA.splashFrame, 1, 1, 0)
            C_Timer.NewTimer(1, function()
                RA.splashFrame:Hide()
                RA.splashFrame.donate:Hide()
            end)
        else
            RA.SplashFrameHide()
        end
    end)
end

--[[------------------------------------------------------------------------------------------------
    Name: SplashFrame
    Type: Function
    Arguments: None
    Return: Nothing
    Description:
--]]
function RA.SplashFrame()
    RA.splashFrame:SetAlpha(1)
    RA.splashFrame.donate:SetAlpha(1)
    RA.splashFrame:Show()
    RA.splashFrame.donate:Show()
    RA.splashFrame.shown = true
    C_Timer.NewTimer(4, function()
        RA.SplashFrameHide()
    end)
end


--[[------------------------------------------------------------------------------------------------
    Name: SplashInit
    Type: Function
    Arguments: None
    Return: Nothing
    Description:
--]]
function RA.SplashInit()
    if RA.Fetch("splash", true) == true and not RA.splashFrame then

        RA.splashFrame = CreateFrame("Frame", nil, UIParent)
        RA.splashFrame.shown = false
        RA.splashFrame.hide = true
        RA.splashFrame:SetPoint("CENTER", UIParent)
        RA.splashFrame:SetWidth(512)
        RA.splashFrame:SetHeight(512)
        RA.splashFrame:SetBackdrop({ bgFile = "Interface\\AddOns\\Probably_RotAgent\\Libs\\Media\\splash.blp" })
        RA.splashFrame:Hide()

        RA.splashFrame.donate = CreateFrame("Frame", nil, RA.splashFrame)
        RA.splashFrame.donate:SetWidth(128)
        RA.splashFrame.donate:SetHeight(64)
        RA.splashFrame.donate:SetBackdrop({ bgFile = "Interface\\AddOns\\Probably_RotAgent\\Libs\\Media\\donate.blp" })
        RA.splashFrame.donate:SetPoint("BOTTOMLEFT", RA.splashFrame, "BOTTOMLEFT", 0, 70)
        RA.splashFrame.donate:SetScript("OnMouseDown", function()
            if FireHack then OpenURL("https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=PBKAPJ6VCJ2VS&lc=US&item_name=RotAgent%20Donations&currency_code=USD&bn=PP%2dDonationsBF%3abtn_donate_LG%2egif%3aNonHosted") end
        end)
        RA.splashFrame.donate:SetScript("OnEnter", function()
            RA.splashFrame.hide = false
            SetCursor("BUY_CURSOR")
        end)
        RA.splashFrame.donate:SetScript("OnLeave", function()
            RA.splashFrame.hide = true
            ResetCursor()
        end)
        RA.splashFrame.donate:Hide()

        ProbablyEngine.listener.register("PLAYER_ALIVE", function(...)
            if not RA.splashFrame.shown then
                RA.SplashFrame()
            end
        end)

        ProbablyEngine.listener.register("PLAYER_ENTERING_WORLD", function(...)
            if not RA.splashFrame.shown then
                RA.SplashFrame()
            end
        end)

    end
end


--[[------------------------------------------------------------------------------------------------
	Name: TargetIsImmune
	Type: Function
	Arguments: unit - Valid game unit
	Return: Boolean
	Description: Checks if the supplied unit has a Special Aura buff/debuff. Checks if the "player"
	can not attack the unit. Checks if the unit is not in combat and not a Special Enemy. Otherwise
	it will return false, the unit is not immune.
--]]
function RA.TargetIsImmune(unit)
	if not UnitExists(unit) then
		return false
	end
	RA.Debug("RA.TargetIsImmune()2", unit)

	if RA.SpecialAurasCheck(unit) then
		return true
	elseif not UnitCanAttack("player", unit) then
		return true
	elseif not UnitAffectingCombat(unit) and not RA.SpecialEnemyTarget(unit) then
		return true
	else
		return false
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: TimeToDeath
	Type: Function
	Arguments: unit - Valid game unit
	Return: seconds - Seconds til unit is dead
	Description:
--]]
RA.timeToDeathTrack = RA.timeToDeathTrack or { }
function RA.TimeToDeath(target)
	RA.Debug("RA.TimeToDeath()2", unit)

	local guid = UnitGUID(target)
	if RA.timeToDeathTrack[target] and RA.timeToDeathTrack[target].guid == guid then
		local floor = math.floor
		local startTime = RA.timeToDeathTrack[target].time
		local curHP = UnitHealth(target)
		local maxHP = RA.timeToDeathTrack[target].startHealth
		local diffHP = maxHP - curHP
		local duration = GetTime() - startTime
		local hpps = diffHP / duration
		local timeDeath = curHP / hpps
		local roundedDeath =  floor((timeDeath * 10^2) + 0.5) / (10^2)
		if timeDeath == math.huge then
			return 999999
		elseif timeDeath < 0 then
			return 0
		else
			return roundedDeath
		end
	elseif RA.timeToDeathTrack[target] then
		table.empty(RA.timeToDeathTrack[target])
	else
		RA.timeToDeathTrack[target] = { }
	end
	RA.timeToDeathTrack[target].guid = guid
	RA.timeToDeathTrack[target].time = GetTime()
	RA.timeToDeathTrack[target].startHealth = UnitHealth(target)
	return 999999
end


--[[------------------------------------------------------------------------------------------------
	Name: UnitsAroundUnit
	Type: Function
	Arguments: unit - Valid game unit
			   distance -
			   ignoreCombat -
	Return: total -
	Description:
--]]
RA.uauCacheSize = RA.uauCacheSize or 0
local uauCacheTime = uauCacheTime or { }
local uauCacheCount = uauCacheCount or { }
local uauCacheDuration = uauCacheDuration or 0.1
function RA.UnitsAroundUnit(unit, distance)
	local uauCacheTime_C = uauCacheTime[unit..distance]
	if uauCacheTime_C and ((uauCacheTime_C + uauCacheDuration) > GetTime()) then
		return uauCacheCount[unit..distance]
	end
	if UnitExists(unit) then
		RA.Debug("RA.UnitsAroundUnit()2", unit)

		local total = 0
		for i=1, #RA.unitCache do
			local unitDistance = RA.Distance(RA.unitCache[i].object, unit)
			if unitDistance <= distance then
				total = total + 1
			end
		end
		uauCacheCount[unit..distance..tostring(ignoreCombat)] = total
		uauCacheTime[unit..distance..tostring(ignoreCombat)] = GetTime()
		RA.uauCacheSize = total
		return total
	else
		RA.uauCacheSize = 0
		return 0
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: WarningFrameBarrageInit
	Type: Function
	Arguments: None
	Return: Nothing
	Description:
--]]
function RA.WarningFrameBarrageInit()
	RA.warningFrameBarrage = CreateFrame("Frame",nil,UIParent)
	RA.warningFrameBarrage:SetSize(300,50)
	RA.warningFrameBarrage:Hide()
	RA.warningFrameBarrage:SetScript("onUpdate", RA.NotificationUpdate)
	RA.warningFrameBarrage:SetPoint("CENTER", 0, 300)
	RA.warningFrameBarrage.text = RA.warningFrameBarrage:CreateFontString(nil,"OVERLAY","MovieSubtitleFont")
	RA.warningFrameBarrage.text:SetTextColor(0.80,0.29,0.72)
	RA.warningFrameBarrage.text:SetTextHeight(32)
	RA.warningFrameBarrage.text:SetAllPoints()
	RA.warningFrameBarrage.texture = RA.warningFrameBarrage:CreateTexture()
	RA.warningFrameBarrage.texture:SetAllPoints()
	RA.warningFrameBarrage.texture:SetTexture(0,0,0,.25)
	RA.warningFrameBarrage.time = 0
end
RA.WarningFrameBarrageInit()


--[[------------------------------------------------------------------------------------------------
	Name: warningFrameBarrage:message
	Type: Function
	Arguments: message - String passed to function to display
	Return: Nothing
	Description: Set the text for the frame with 'message'. Then set the alpha for the frame to 1,
	fully visible. Next set the timer for the start of the message to now, GetTime. Lastly show the
	frame.
--]]
function RA.warningFrameBarrage:message(message)
	if RA.Fetch('nobarrage', true) then
		self.text:SetText(message)
		self:SetAlpha(1)
		self:Show()
	else
		self:SetAlpha(0)
		self:Hide()
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: WarningFrameCleaveInit
	Type: Function
	Arguments: None
	Return: Nothing
	Description:
--]]
function RA.WarningFrameCleaveInit()
	RA.warningFrameCleave = CreateFrame("Frame",nil,UIParent)
	RA.warningFrameCleave:SetSize(300,50)
	RA.warningFrameCleave:Hide()
	RA.warningFrameCleave:SetScript("onUpdate", RA.NotificationUpdate)
	RA.warningFrameCleave:SetPoint("CENTER", 0, 250)
	RA.warningFrameCleave.text = RA.warningFrameCleave:CreateFontString(nil,"OVERLAY","MovieSubtitleFont")
	RA.warningFrameCleave.text:SetTextColor(0.80,0.29,0.72)
	RA.warningFrameCleave.text:SetTextHeight(32)
	RA.warningFrameCleave.text:SetAllPoints()
	RA.warningFrameCleave.texture = RA.warningFrameCleave:CreateTexture()
	RA.warningFrameCleave.texture:SetAllPoints()
	RA.warningFrameCleave.texture:SetTexture(0,0,0,.25)
	RA.warningFrameCleave.time = 0
end
RA.WarningFrameCleaveInit()


--[[------------------------------------------------------------------------------------------------
	Name: warningFrameCleave:message
	Type: Function
	Arguments: message - String passed to function to display
	Return: Nothing
	Description: Set the text for the frame with 'message'. Then set the alpha for the frame to 1,
	fully visible. Next set the timer for the start of the message to now, GetTime. Lastly show the
	frame.
--]]
function RA.warningFrameCleave:message(message)
	if RA.Fetch('nocleave', true) then
		self.text:SetText(message)
		self:SetAlpha(1)
		self:Show()
	else
		self:SetAlpha(0)
		self:Hide()
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: WarningFrameNukeInit
	Type: Function
	Arguments: None
	Return: Nothing
	Description:
--]]
function RA.WarningFrameNukeInit()
	RA.warningFrameNuke = CreateFrame("Frame",nil,UIParent)
	RA.warningFrameNuke:SetSize(300,50)
	RA.warningFrameNuke:Hide()
	RA.warningFrameNuke:SetScript("onUpdate", RA.NotificationUpdate)
	RA.warningFrameNuke:SetPoint("CENTER", 0, 200)
	RA.warningFrameNuke.text = RA.warningFrameNuke:CreateFontString(nil,"OVERLAY","MovieSubtitleFont")
	RA.warningFrameNuke.text:SetTextColor(0.80,0.29,0.72)
	RA.warningFrameNuke.text:SetTextHeight(32)
	RA.warningFrameNuke.text:SetAllPoints()
	RA.warningFrameNuke.texture = RA.warningFrameNuke:CreateTexture()
	RA.warningFrameNuke.texture:SetAllPoints()
	RA.warningFrameNuke.texture:SetTexture(0,0,0,.25)
	RA.warningFrameNuke.time = 0
end
RA.WarningFrameNukeInit()


--[[------------------------------------------------------------------------------------------------
	Name: warningFrameNuke:message
	Type: Function
	Arguments: message - String passed to function to display
	Return: Nothing
	Description: Set the text for the frame with 'message'. Then set the alpha for the frame to 1,
	fully visible. Next set the timer for the start of the message to now, GetTime. Lastly show the
	frame.
--]]
function RA.warningFrameNuke:message(message)
	if RA.Fetch('prioritynuke', true) then
		self.text:SetText(message)
		self:SetAlpha(1)
		self:Show()
	else
		self:SetAlpha(0)
		self:Hide()
	end
end


--[[------------------------------------------------------------------------------------------------
	Name: Write
--]]
function RA.Write(key, value)
	if ProbablyEngine.rotation.currentStringComp == "RA - Combat" then
		return ProbablyEngine.interface.writeKey('ra_config_r_combat', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Frost DW" then
		return ProbablyEngine.interface.writeKey('ra_config_dk_fdw', key, default)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Beast Mastery" then
		return ProbablyEngine.interface.writeKey('ra_config_h_bm', key, value)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Marksmanship" then
		return ProbablyEngine.interface.writeKey('ra_config_h_mm', key, value)
	elseif ProbablyEngine.rotation.currentStringComp == "RA - Survival" then
		return ProbablyEngine.interface.writeKey('ra_config_h_sv', key, value)
	end
end