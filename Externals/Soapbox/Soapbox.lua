--[[------------------------------------------------------------------------------------------------

Soapbox.lua

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]

--Hide the annoying ElvUI issues.
--BasicScriptErrors:Hide();

--If running the free version, trick it into turning on for level 100's.
--[[
if Soapbox ~= nil then
	Soapbox.SetSetting("Running",Running);
end
]]