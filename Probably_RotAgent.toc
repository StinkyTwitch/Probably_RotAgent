## Interface: 60200
## Title: Probably_RotAgent
## Notes: RotAgent Rotation Helper for ProbablyEngine
## Author: StinkyTwitch
## Version: 0.3.7
## Dependencies: Probably

Externals\LibSharedMedia\LibSharedMedia.lua
Externals\RotDraw\LibStub.lua
Externals\RotDraw\RotDraw.lua
Externals\Soapbox\Soapbox.lua

Libs\Globals.lua
Libs\LibCommands.lua

Libs\ClassDK.lua
Libs\ClassHunter.lua
Libs\ClassRogue.lua

Libs\LibPE.lua
Libs\LibUnitCacheManager.lua

Libs\HelperSymbols.lua
Libs\HelperWindowTarget.lua
Libs\HelperWindowVariables.lua
Libs\HelperWindowStatus.lua
Libs\HelperWindowCache.lua

Rotations\DeathKnightFrostDW.lua
Rotations\HunterBeastMastery.lua
Rotations\HunterMarksmanship.lua
Rotations\RogueCombat.lua

Plugins\RotFishing.lua
Plugins\RotFishing2.lua

Libs\Timers.lua