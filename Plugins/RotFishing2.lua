--[[------------------------------------------------------------------------------------------------

RotFishing.lua v0.2.1

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
_G["RAF"] = _G["RAF"] or { }


--[[------------------------------------------------------------------------------------------------
PE FUNCTIONS
------------------------------------------------------------------------------------------------]]--
function RAF.Eval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end

function RAF.Fetch(key, default)
	return ProbablyEngine.interface.fetchKey('rafconfig', key, default)
end


--[[------------------------------------------------------------------------------------------------
TABLES
------------------------------------------------------------------------------------------------]]--
RAF.baitBuff = {
	[158038] = { name = "Abyssal Gulper Eel Bait"  },
	[158039] = { name = "Blackwater Whiptail Bait" },
	[158035] = { name = "Blind Lake Sturgeon Bait" },
	[158034] = { name = "Fat Sleeper Bait"         },
	[158036] = { name = "Fire Ammonite Bait"       },
	[158031] = { name = "Jawless Skulker Bait"     },
	[158037] = { name = "Sea Scorpion Bait"        },
	[188904] = { name = "Felmouth Frenzy Bait"     },
}

RAF.bait = {
	[110293] = { name = "Abyssal Gulper Eel Bait"  },
	[110294] = { name = "Blackwater Whiptail Bait" },
	[110290] = { name = "Blind Lake Sturgeon Bait" },
	[110289] = { name = "Fat Sleeper Bait"         },
	[110291] = { name = "Fire Ammonite Bait"       },
	[110274] = { name = "Jawless Skulker Bait"     },
	[110292] = { name = "Sea Scorpion Bait"        },
	[128229] = { name = "Felmouth Frenzy Bait"     },
}

RAF.hats = {
	[ 19972] = { name = "Lucky Fishing Hat",    bonus = 5,   },
	[ 88710] = { name = "Nat's Hat",            bonus = 5,   },
	[ 93732] = { name = "Darkmoon Fishing Cap", bonus = 5,   },
	[117405] = { name = "Nat's Drinking Hat",   bonus = 10,  },
	[118380] = { name = "Hightfish Cap",        bonus = 100, },
	[118393] = { name = "Tentacled Hat",        bonus = 100, },
}

RAF.lures = {
	[  6529] = { name = "Shiny Bauble",               bonus = 25,  enchantID = 263  },
	[  6530] = { name = "Nightcrawlers",              bonus = 50,  enchantID = 264  },
	[  6532] = { name = "Bright Baubles",             bonus = 75,  enchantID = 265  },
	[  6533] = { name = "Aquadynamic Fish Attractor", bonus = 100, enchantID = 266  },
	[  6811] = { name = "Aquadynamic Fish Lens",      bonus = 50,  enchantID = 264  },
	[  7307] = { name = "Flesh Eating Worm",          bonus = 75,  enchantID = 265  },
	[ 33820] = { name = "Weather-Beaten Fishing Hat", bonus = 75,  enchantID = 265  },
	[ 34861] = { name = "Sharpened Fish Hook",        bonus = 100, enchantID = 266  },
	[ 46006] = { name = "Glow Worm",                  bonus = 100, enchantID = 3868 },
	[ 62673] = { name = "Feathered Lure",             bonus = 100, enchantID = 266  },
	[ 67404] = { name = "Glass Fishing Bobber",       bonus = 15,  enchantID = 4264 },
	[ 68049] = { name = "Heat-Treated Spinning Lure", bonus = 150, enchantID = 4225 },
	[ 88710] = { name = "Nat's Hat",                  bonus = 150, enchantID = 4919 },
	[116825] = { name = "Savage Fishing Pole",        bonus = 200, enchantID = 5386 },
	[116826] = { name = "Draenic Fishing Pole",       bonus = 200, enchantID = 5386 },
	[117405] = { name = "Nat's Drinking Hat",         bonus = 150, enchantID = 4919 },
	[118391] = { name = "Worm Supreme",               bonus = 200, enchantID = 5386 },
	[124674] = { name = "Day-Old Darkmoon Doughnut",  bonus = 200, enchantID = 5386 },
}

RAF.poles = {
	[  6256] = { name = "Fishing Pole",                       bonus = 0   },
	[  6365] = { name = "Strong Fishing Pole",                bonus = 5   },
	[  6366] = { name = "Darkwood Fishing Pole",              bonus = 15  },
	[  6367] = { name = "Big Iron Fishing Pole",              bonus = 20  },
	[ 12225] = { name = "Blump Family Fishing Pole",          bonus = 3   },
	[ 19022] = { name = "Nat Pagle's Extreme Angler FC-5000", bonus = 20  },
	[ 19970] = { name = "Arcanite Fishing Pole",              bonus = 40  },
	[ 25978] = { name = "Seth's Graphite Fishing Pole",       bonus = 20  },
	[ 44050] = { name = "Mastercraft Kalu'ak Fishing Pole",   bonus = 30  },
	[ 45858] = { name = "Nat's Lucky Fishing Pole",           bonus = 25  },
	[ 45991] = { name = "Bone Fishing Pole",                  bonus = 30  },
	[ 45992] = { name = "Jeweled Fishing Pole",               bonus = 30  },
	[ 46337] = { name = "Staats' Fishing Pole",               bonus = 3   },
	[ 84660] = { name = "Pandaren Fishing Pole",              bonus = 10  },
	[ 84661] = { name = "Dragon Fishing Pole",                bonus = 30  },
	[116825] = { name = "Savage Fishing Pole",                bonus = 30  },
	[116826] = { name = "Draenic Fishing Pole",               bonus = 30  },
	[118381] = { name = "Ephemeral Fishing Pole",             bonus = 100 },
	[120163] = { name = "Thruk's Fishing Rod",                bonus = 3   },
}

RAF.fishSchools = {
	[229073] = { name = "Blackwater Whiptail School" },
	[229069] = { name = "Blind Lake Sturgeon School" },
	[229068] = { name = "Fat Sleeper School"         },
	[243324] = { name = "Felmouth Frenzy School"     },
	[243325] = { name = "Felmouth Frenzy School"     },
	[243354] = { name = "Felmouth Frenzy School"     },
	[229070] = { name = "Fire Ammonite School"       },
	[229067] = { name = "Jawless Skulker School"     },
	[236756] = { name = "Oily Abyssal Gulper School" },
	[237295] = { name = "Oily Sea Scorpion School"   },
	[229071] = { name = "Sea Scorpion School"        },
}


--[[------------------------------------------------------------------------------------------------
RANDOM NUMBER FUNCTION
	* http://opentibia.net/topic/109869-code-for-generating-random-numbers-in-normal-distribution
------------------------------------------------------------------------------------------------]]--
function RAF.InternalRandom()
	local x1, x2, w, y1, y2

	repeat
		x1 = 2 * math.random() - 1
		x2 = 2 * math.random() - 1
		w = x1 * x1 + x2 * x2
	until (w < 1)

	w = math.sqrt((-2 * math.log(w)) / w)
	y1 = x1 * w
	y2 = x2 * w
	return y1, y2
end

function RAF.Random(min, max, variance, strictMin, strictMax)
	local average = (min + max) / 2

	if variance == nil then
		variance = 2.4
	end

	local escala = (max - average) / variance
	local x = escala * RAF.InternalRandom() + average

	if strictMin ~= nil then
		x = math.max(x, strictMin)
	end
	if strictMax ~= nil then
		x = math.min(x, strictMax)
	end

	return(x)
end


--[[------------------------------------------------------------------------------------------------
OBJECT FUNCTIONS/VARIABLES
------------------------------------------------------------------------------------------------]]--
RAF.bobberDisplayID = 668
RAF.objectBobbingOffset = 0x1E0
RAF.objectCreatorOffset = 0x30
RAF.objectOffset = 0x40
RAF.unitDisplayIDOffset = 0x190

function RAF.GetObjectDisplayID(object)
	return ObjectDescriptor(object, RAF.objectOffset, Types.UInt)
end

function RAF.GetObjectGUID(object)
	return tonumber(ObjectDescriptor(object, 0, Types.ULong))
end

function RAF.GetUnitDisplayID(object)
	return ObjectDescriptor(object, RAF.unitDisplayIDOffset, Types.UInt)
end

function RAF.IsObjectBobbing(object)
	return (ObjectField(object, RAF.objectBobbingOffset, Types.Byte) == 1)
end

function RAF.IsObjectCreatedBy(owner, object)
	return tonumber(ObjectDescriptor(object, RAF.objectCreatorOffset, Types.ULong)) == RAF.GetObjectGUID(owner)
end


--[[------------------------------------------------------------------------------------------------
GENERAL FUNCTIONS
------------------------------------------------------------------------------------------------]]--
RAF.debugThisThing = nil
RAF.debugTrack = RAF.debugTrack or { }
function RAF.Debug(name, message)
	local debugToggle = RAF.Fetch("debug_check", false)

	if not debugToggle or RAF.debugThisThing == nil then
		return
	end

	name = tostring(name)
	message = tostring(message)
	local search = tostring(RAF.debugThisThing)
	local printThrottle = 1
	local debugThrottle = RAF.Fetch("debug_spin", 0)

	if string.find(name, search) then
		if RAF.debugTrack[name] then
			if (GetTime() - RAF.debugTrack[name].start) >= debugThrottle then
				RAF.debugTrack[name].start = GetTime()
				if debugToggle then
					print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
				end
			end
		else
			RAF.debugTrack[name] = { }
			RAF.debugTrack[name].start = GetTime()
			if debugToggle then
				print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
			end
		end
	elseif RAF.debugThisThing == "All" or RAF.debugThisThing == "all" then
		if RAF.debugTrack[name] then
			if (GetTime() - RAF.debugTrack[name].start) >= debugThrottle then
				RAF.debugTrack[name].start = GetTime()
				if debugToggle then
					print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
				end
			end
		else
			RAF.debugTrack[name] = { }
			RAF.debugTrack[name].start = GetTime()
			if debugToggle then
				print("|cff556C38" .. GetTime() .. "|r |cffA4A4A4".. name .. "|r - " .. message)
			end
		end
	end
end

function RAF.PickupItem(itemID)
	local currentItemID = 0

	-- Look through the player inventory.
	for i = INVSLOT_FIRST_EQUIPPED, INVSLOT_LAST_EQUIPPED do
		currentItemID = GetInventoryItemID("player", i)

		if currentItemID == itemID then
			PickupInventoryItem(i)
			return true
		end
	end

	-- Look through the players bags.
	for i = 1, NUM_BAG_SLOTS do
		for j = 1, GetContainerNumSlots(i) do
			currentItemID = GetContainerItemID(i, j)

			if currentItemID == itemID then
				PickupContainerItem(i, j)
				return true
			end
		end
	end
end

function RAF.SpellToName(spellID)
	return tostring(select(1,GetSpellInfo(spellID)))
end




























































----------------------------------------------------------------------------------------------------
-- BLADEBONE HOOK FUNCTIONS
----------------------------------------------------------------------------------------------------
function RAF.BladeboneHookApplied()
	RAF.Debug("RAF.BladeboneHookApplied()1", "entry!")
	if UnitBuff("player", GetSpellInfo(182226)) then
		RAF.Debug("RAF.BladeboneHookApplied()2", "buff found!")
		return true
	else
		RAF.Debug("RAF.BladeboneHookApplied()4", "buff not found!")
		return false

	end
end

RAF.bladeboneHookLock = false
function RAF.UseBladeboneHook()
	RAF.Debug("RAF.UseBladeboneHook()1", "entry!")

	if RAF.BladeboneHookApplied() then
		RAF.Debug("RAF.UseBladeboneHook()2", "Bladebone Hook applied!")
		RAF.bladeboneHookLock = false
		RAF.goFishState = "run"
		return
	elseif not RAF.bladeboneHookLock then
		RAF.Debug("RAF.UseBladeboneHook()3", "proceeding to apply Bladebone Hook!")
		RAF.bladeboneHookLock = true

		for bag = 0,4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local bagItemID = GetContainerItemID(bag, slot)

				if bagItemID == 122742 then
					RAF.Debug("RAF.UseBladeboneHook()4", "found Bladebone Hook!")
					applyingBladeboneHookLock = true
					UseContainerItem(bag, slot)
				end
			end
		end

		RAF.Debug("RAF.UseBladeboneHook()5", "no Bladebone Hook found!")
		RAF.goFishState = "run"
		return
	else
		return
	end
end
















































--[[------------------------------------------------------------------------------------------------
BAIT FUNCTIONS
------------------------------------------------------------------------------------------------]]--
function RAF.BaitAlreadyApplied()
	for k,v in pairs(RAF.bait) do
		local buff = GetSpellInfo(k)
		if UnitBuff("player", buff) then
			return true
		end
	end
end

function RAF.BaitFound(baitID)
	baitID = tonumber(baitID)
	local baitFound = false

	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)
			local itemIDType = type(itemID)

			if itemID == baitID then
				RAF.Debug("RAF.BaitFound()2", "bait found " .. baitID .. "!")
				return true
			end
		end
	end
	return false
end

RAF.currentBait = nil
function RAF.CurrentBait()
	RAF.currentBait = { }

	-- Check Bags
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID then
				if RAF.bait[tonumber(itemID)] ~= nil then
					RAF.currentBait[#RAF.currentBait+1] =
						{
							name = RAF.bait[tonumber(itemID)].name,
							bonus = RAF.bait[tonumber(itemID)].bonus,
							enchantID = RAF.bait[tonumber(itemID)].enchantID,
							id = itemID,
						}
				end
			end
		end
	end
end

function RAF.UseBait()

end


















































--[[------------------------------------------------------------------------------------------------
BOBBER FUNCTIONS
------------------------------------------------------------------------------------------------]]--
function RAF.BobberObject()
	local bobber = nil
	local objectCount = GetObjectCount()

	for i = 1, objectCount do
		local object = GetObjectWithIndex(i)

		if RAF.GetObjectDisplayID(object) == RAF.bobberDisplayID then
			if RAF.IsObjectCreatedBy("player", object) then
				bobber = object
				break
			end
		end
	end

	return bobber
end

function RAF.BobberBobbing()
	local bobberBobbing = ObjectField(RAF.BobberObject(), RAF.objectBobbingOffset, Types.Short)
	if bobberBobbing == 1 then
		return true
	else
		return false
	end
end


















































--[[------------------------------------------------------------------------------------------------
FISHING HAT FUNCTIONS
------------------------------------------------------------------------------------------------]]--
RAF.currentHats = nil
function RAF.CurrentHats()
	RAF.currentHats = { }

	-- Check Bags
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local bagItemID = GetContainerItemID(bag, slot)

			if RAF.hats[tonumber(bagItemID)] ~= nil then
				local hatFound = false

				for i, v in ipairs(RAF.currentHats) do
					if v.id == bagItemID then
						hatFound = true
					end
				end

				if not hatFound then
					RAF.currentHats[#RAF.currentHats+1] =
						{
							name = RAF.hats[tonumber(bagItemID)].name,
							bonus = RAF.hats[tonumber(bagItemID)].bonus,
							id = bagItemID,
						}
				end
			end

		end
	end

	-- Check Equipment
	local headItemID = GetInventoryItemID("player", 1)

	if RAF.hats[tonumber(headItemID)] ~= nil then
		local hatFound = false

		for i, v in ipairs(RAF.currentHats) do
			if v.id == headItemID then
				hatFound = true
			end
		end

		if not hatFound then
			RAF.currentHats[#RAF.currentHats+1] =
				{
					name = RAF.hats[tonumber(headItemID)].name,
					bonus = RAF.hats[tonumber(headItemID)].bonus,
					id = headItemID,
				}
		end
	end
end

function RAF.BestHat()
	local bestHat = 0
	local bestBonus = 0

	for k,v in pairs(RAF.currentHats) do
		if v.bonus > bestBonus then
			bestBonus = v.bonus
			bestHat = v.id
		end
	end

	return bestHat
end

RAF.equipHatLock = false
function RAF.HatAlreadyEquipped()
	local currentHeadID = GetInventoryItemID("player", 1)

	if RAF.Fetch("hatdropdown", "hatnone") == "hatnone" then
		RAF.equipHatLock = false
		return true

	elseif RAF.Fetch("hatdropdown", "hatnone") == "hatbest" then
		if currentHeadID == RAF.BestHat() then
			RAF.equipHatLock = false
			return true
		end
	else
		if currentHeadID == RAF.Fetch("hatdropdown", "hatnone") then
			RAF.equipHatLock = false
			return true
		end
	end

	return false
end

function RAF.EquipHat()
	RAF.Debug("RAF.EquipHat()1", "entry!")

	if RAF.HatAlreadyEquipped() then
		RAF.Debug("RAF.EquipHat()2", "hat already equipped!")
		RAF.goFishState = "run"
	end

	RAF.CurrentHats()

	if not RAF.equipHatLock then
		if RAF.Fetch("hatdropdown", "hatnone") == "hatbest" then
			RAF.Debug("RAF.EquipHat()3", "equipping best hat!")

			local bestHat = RAF.BestHat()

			RAF.equipHatLock = true
			RAF.PickupItem(bestHat)
			AutoEquipCursorItem()
			return

		else
			RAF.Debug("RAF.EquipHat()4", "equipping specific hat!")
			local hat = RAF.Fetch("hatdropdown", "hatnone")
			RAF.equipHatLock = true
			RAF.PickupItem(hat)
			AutoEquipCursorItem()
			return

		end
	end
end

















































--[[------------------------------------------------------------------------------------------------
FISHING LURE FUNCTIONS
------------------------------------------------------------------------------------------------]]--
RAF.currentLures = nil
function RAF.CurrentLures()
	RAF.currentLures = { }

	local lureFound = false
	local ephemeralPoleFound = false

	-- Check for Ephemeral Pole
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID == 118381 then
				ephemeralPoleFound = true
			end
		end
	end

	-- Check Bags
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)
			local _, duration, _ = GetContainerItemCooldown(bag, slot)

			if itemID then
				if RAF.lures[tonumber(itemID)] ~= nil and duration == 0 then
					for i, v in ipairs(RAF.currentLures) do
						if v.id == itemID then
							lureFound = true
						end
					end

					if not lureFound then
						if ephemeralPoleFound and itemID ~= 116825 then
							RAF.currentLures[#RAF.currentLures+1] =
								{
									name = RAF.lures[tonumber(itemID)].name,
									bonus = RAF.lures[tonumber(itemID)].bonus,
									enchantID = RAF.lures[tonumber(itemID)].enchantID,
									id = itemID,
								}
						else
							RAF.currentLures[#RAF.currentLures+1] =
								{
									name = RAF.lures[tonumber(itemID)].name,
									bonus = RAF.lures[tonumber(itemID)].bonus,
									enchantID = RAF.lures[tonumber(itemID)].enchantID,
									id = itemID,
								}
						end
					end
				end
			end
		end
	end

	-- Check Equipment
	for i = INVSLOT_FIRST_EQUIPPED, INVSLOT_LAST_EQUIPPED do
		local equipmentID = GetInventoryItemID("player", i)
		local _, duration, _ = GetInventoryItemCooldown("player", i)

		if RAF.lures[tonumber(equipmentID)] ~= nil and duration == 0 then
			for i, v in ipairs(RAF.currentLures) do
				if v.id == equipmentID then
					lureFound = true
				end
			end

			if not lureFound then
				RAF.currentLures[#RAF.currentLures+1] =
					{
						name = RAF.lures[tonumber(equipmentID)].name,
						bonus = RAF.lures[tonumber(equipmentID)].bonus,
						enchantID = RAF.lures[tonumber(equipmentID)].enchantID,
						id = equipmentID,
					}
			end
		end
	end
end

function RAF.ApplyHatLure()
	RAF.Debug("RAF.ApplyHatLure()1", "entry!")

	if RAF.LureAlreadyApplied() then
		RAF.ReplaceHelmet()
		RAF.goFishState = "run"
		RAF.Debug("RAF.ApplyHatLure()2", "lure already applied! State changed to 'run'!")
		return
	end

	-- Get Best Lure --
	if RAF.Fetch("luredropdown", "lurenone") == "lurebest" then
		lureID = RAF.BestLure()
		RAF.Debug("RAF.ApplyHatLure()3", "best lure selected (" .. lureID .. ")")

	-- Get Specific Lure --
	else
		lureID = RAF.Fetch("luredropdown", "lurenone")
		RAF.Debug("RAF.ApplyHatLure()4", "specific lure selected (" .. lureID .. ")!")

	end

	if GetInventoryItemID("player", INVSLOT_HEAD) ~= lureID then
		for bag = 0,4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local bagItemID = GetContainerItemID(bag, slot)

				if bagItemID == lureID then
					RAF.Debug("RAF.ApplyHatLure()5", "lure found (" .. bagItemID ..") equipping!")
					PickupContainerItem(bag, slot)
					AutoEquipCursorItem()
				end
			end
		end
	end

	if GetInventoryItemID("player", INVSLOT_HEAD) == lureID and not RAF.applyLureLock then
		if RAF.PoleAlreadyEquipped() then
			local poleID = GetInventoryItemID("player", INVSLOT_MAINHAND)
			RAF.applyLureLock = true
			UseInventoryItem(INVSLOT_HEAD)
			SpellTargetItem(poleID)
			RAF.Debug("RAF.ApplyHatLure()6", "applying hat lure to fishing pole!")
			return

		end
	end
end

function RAF.ApplyPoleLure()
	RAF.Debug("RAF.ApplyPoleLure()1", "entry!")

	if RAF.LureAlreadyApplied() then
		RAF.goFishState = "run"
		RAF.Debug("RAF.ApplyPoleLure()2", "lure already applied! State changed to 'run'!")
		return
	end

	-- Get Best Lure --
	if RAF.Fetch("luredropdown", "lurenone") == "lurebest" then
		lureID = RAF.BestLure()
		RAF.Debug("RAF.ApplyPoleLure()3", "best lure selected (" .. lureID .. ")")

	-- Get Specific Lure --
	else
		lureID = RAF.Fetch("luredropdown", "lurenone")
		RAF.Debug("RAF.ApplyPoleLure()4", "specific lure selected (" .. lureID .. ")!")

	end

	if GetInventoryItemID("player", 16) ~= lureID then
		for bag = 0,4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local bagItemID = GetContainerItemID(bag, slot)

				if bagItemID == lureID then
					RAF.Debug("RAF.ApplyPoleLure()5", "lure found (" .. bagItemID ..") equipping!")
					PickupContainerItem(bag, slot)
					AutoEquipCursorItem()
				end
			end
		end
	end

	if GetInventoryItemID("player", INVSLOT_MAINHAND) == lureID and not RAF.applyLureLock then
		if RAF.PoleAlreadyEquipped() then
			local poleID = GetInventoryItemID("player", INVSLOT_MAINHAND)
			RAF.applyLureLock = true
			UseInventoryItem(INVSLOT_MAINHAND)
			RAF.Debug("RAF.ApplyPoleLure()6", "applying pole lure!")
			return

		end
	end
end

RAF.applySpecificLureLock = false
function RAF.ApplySpecificLure()
	RAF.Debug("RAF.ApplySpecificure()1", "entry!")

	if RAF.LureAlreadyApplied() then
		RAF.goFishState = "run"
		RAF.applySpecificLureLock = false
		RAF.Debug("RAF.ApplySpecificure()2", "lure already applied! State changed to 'run'!")
		return
	end

	-- Get Best Non-Pole/Non-Hat Lure --
	if RAF.Fetch("luredropdown", "lurenone") == "lurebest" then
		lureID = RAF.BestLure()
		RAF.Debug("RAF.ApplySpecificure()3", "best lure selected (" .. lureID .. ")")

		for bag = 0,4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local bagItemID = GetContainerItemID(bag, slot)

				if bagItemID == lureID and not RAF.applySpecificLureLock then
					RAF.applySpecificLureLock = true
					RAF.Debug("RAF.ApplySpecificure()4", "lure found (" .. bagItemID ..") applying!")
					UseContainerItem(bag, slot)
				end
			end
		end

	-- Get Specific Lure --
	else
		lureID = RAF.Fetch("luredropdown", "lurenone")
		RAF.Debug("RAF.ApplySpecificure()5", "specific lure selected (" .. lureID .. ")!")

		for bag = 0,4 do
			for slot = 1, GetContainerNumSlots(bag) do
				local bagItemID = GetContainerItemID(bag, slot)

				if bagItemID == lureID and not RAF.applySpecificLureLock then
					RAF.applySpecificLureLock = true
					RAF.Debug("RAF.ApplySpecificure()6", "lure found (" .. bagItemID ..") applying!")
					UseContainerItem(bag, slot)
				end
			end
		end
	end
end

function RAF.BestLure()
	RAF.CurrentLures()
	local bestLure = 0
	local bestBonus = 0

	for i, v in ipairs(RAF.currentLures) do
		if v.bonus > bestBonus then
			bestBonus = v.bonus
			bestLure = v.id
		end
	end

	return bestLure
end

RAF.applyLureLock = false
function RAF.LureAlreadyApplied()
	RAF.Debug("RAF.LureAlreadyApplied()1", "entry!")
	RAF.CurrentLures()

	local mainHandEnchantID = RAF.GetWeaponEnchant()
	RAF.Debug("RAF.LureAlreadyApplied()2", "Enchant ID: " .. tostring(mainHandEnchantID))

	for k, v in pairs(RAF.lures) do
		if mainHandEnchantID == v.enchantID then
			RAF.Debug("RAF.LureAlreadyApplied()3", "Lure already exists: " .. tostring(mainHandEnchantID))
			RAF.applyLureLock = false
			return true
		end
	end

	if RAF.Fetch("luredropdown", "lurenone") == "lurenone" then
		RAF.Debug("RAF.LureAlreadyApplied()4", "Lure 'None' option selected!")
		RAF.applyLureLock = false
		return true

	elseif RAF.Fetch("luredropdown", "lurenone") == "lurebest" then
		RAF.Debug("RAF.LureAlreadyApplied()5a", "Lure 'Best' option!")
		local bestLure = RAF.BestLure()
		RAF.Debug("RAF.LureAlreadyApplied()5b", "bestLure(" .. bestLure .. ")!")
		local bestLureEnchantID = RAF.lures[bestLure].enchantID

		RAF.Debug("RAF.LureAlreadyApplied()6", "Enchant ID(" .. tostring(mainHandEnchantID) .. ") == (" .. tostring(bestLureEnchantID) .. ")")
		if mainHandEnchantID == bestLureEnchantID then
			RAF.applyLureLock = false
			RAF.Debug("RAF.LureAlreadyApplied()6b", "true")
			return true
		end
	else
		local lure = RAF.Fetch("luredropdown", "lurenone")
		local lureEnchantID = RAF.lures[lure].enchantID

		RAF.Debug("RAF.LureAlreadyApplied()7", "Enchant ID(" .. tostring(mainHandEnchantID) .. ") == (" .. tostring(lureEnchantID) .. ")")
		if mainHandEnchantID == lureEnchantID then
			RAF.applyLureLock = false
			return true
		end
	end

	RAF.Debug("RAF.LureAlreadyApplied()8", "return false!")
	return false
end

function RAF.UseLure()
	RAF.Debug("RAF.UseLure()1", "entry!")

	if RAF.LureAlreadyApplied() then
		RAF.Debug("RAF.UseLure()2", "lure already applied!")
		RAF.applyLureLock = false
		RAF.goFishState = "run"
	end

	RAF.CurrentLures()


	if RAF.Fetch("luredropdown", 'lurenone') == "lurebest" then
		RAF.Debug("RAF.UseLure()2", "best!")
		local bestLure = RAF.BestLure()

		-- Best Pole Lure --
		if RAF.poles[bestLure] ~= nil then
			RAF.goFishState = "applypolelure"
			RAF.Debug("RAF.UseLure()3", "state changed to 'applypolelure'!")
			return

		-- Best Hat Lure --
		elseif RAF.hats[bestLure] ~= nil then
			RAF.goFishState = "applyhatlure"
			RAF.Debug("RAF.UseLure()4", "state changed to 'applyhatlure'!")
			return

		-- Best Non-Gear Lure --
		else
			RAF.goFishState = "applyspecificlure"
			RAF.Debug("RAF.UseLure()5", "state changed to 'applyspecificlure'!")
			return
		end

	else
		local lure = RAF.Fetch("luredropdown", "lurenone")

		-- Specific Pole Lure --
		if RAF.poles[lure] ~= nil then
			RAF.goFishState = "applypolelure"
			RAF.Debug("RAF.UseLure()6", "equipping specific pole lure!")
			return

		-- Specific Hat Lure --
		elseif RAF.hats[lure] ~= nil then
			RAF.goFishState = "applyhatlure"
			RAF.Debug("RAF.UseLure()7", "equipping specific hat lure!")
			return

		-- Specific Non-Gear Lure --
		else
			RAF.goFishState = "applyspecificlure"
			RAF.Debug("RAF.UseLure()8", "applying specific lure!")
			return

		end
	end

end


















































--[[------------------------------------------------------------------------------------------------
FISHING POLE FUNCTIONS
------------------------------------------------------------------------------------------------]]--
RAF.currentPoles = { }
function RAF.CurrentPoles()
	RAF.currentPoles = { }

	local poleFound = false

	-- Check Bags
	for bag = 0, 4 do
		for slot = 1, GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID then
				if RAF.poles[tonumber(itemID)] ~= nil  then
					for i, v in ipairs(RAF.currentPoles) do
						if v.id == itemID then
							poleFound = true
						end
					end

					if not poleFound then
						RAF.currentPoles[#RAF.currentPoles+1] =
							{
								name = RAF.poles[tonumber(itemID)].name,
								bonus = RAF.poles[tonumber(itemID)].bonus,
								id = itemID,
							}
					end
				end
			end
		end
	end

	-- Check MainHand (16)
	local equipmentID = GetInventoryItemID("player", 16)

	if RAF.poles[tonumber(equipmentID)] ~= nil  then
		for k,v in pairs(RAF.currentPoles) do
			if v.id == equipmentID then
				poleFound = true
			end
		end

		if not poleFound then
			RAF.currentPoles[#RAF.currentPoles+1] =
				{
					name = RAF.poles[tonumber(equipmentID)].name,
					bonus = RAF.poles[tonumber(equipmentID)].bonus,
					id = equipmentID,
				}
		end
	end
end

function RAF.BestPole()
	local bestPole = 0
	local bestBonus = 0

	for k,v in pairs(RAF.currentPoles) do
		if v.bonus > bestBonus then
			bestBonus = v.bonus
			bestPole = v.id
		end
	end

	return bestPole
end

RAF.equipPoleLock = false
function RAF.PoleAlreadyEquipped()
	local currentFishingPoleID = GetInventoryItemID("player", INVSLOT_MAINHAND)

	if RAF.Fetch("poledropdown", "polenone") == "polenone" then
		RAF.equipPoleLock = false
		return true

	elseif RAF.Fetch("poledropdown", "polenone") == "polebest" then
		local bestPole = RAF.BestPole()

		if currentFishingPoleID == bestPole then
			RAF.equipPoleLock = false
			return true
		end
	else
		if currentFishingPoleID == RAF.Fetch("poledropdown", "polenone") then
			RAF.equipPoleLock = false
			return true
		end
	end

	return false
end

function RAF.EquipPole()
	RAF.Debug("RAF.EquipPole()1", "entry!")

	if RAF.PoleAlreadyEquipped() then
		RAF.Debug("RAF.EquipPole()2", "pole equipped")
		RAF.equipPoleLock = false
		RAF.goFishState = "run"
	end

	RAF.CurrentPoles()

	if not RAF.equipPoleLock then
		if RAF.Fetch("poledropdown", "polenone") == "polenone" then
			return

		elseif RAF.Fetch("poledropdown", "polenone") == "polebest" then
			local bestPole = RAF.BestPole()
			RAF.equipPoleLock = true
			RAF.PickupItem(bestPole)
			AutoEquipCursorItem()
			return

		else
			local pole = RAF.Fetch("poledropdown", "polesnone")
			RAF.equipPoleLock = true
			RAF.PickupItem(pole)
			AutoEquipCursorItem()
			return

		end
	end
end


















































--[[------------------------------------------------------------------------------------------------
FISH POOL FUNCTIONS
------------------------------------------------------------------------------------------------]]--
function RAF.FacePool(poolObject)
	local facing = ObjectFacing("player")
	local x, y, z = ObjectPosition(poolObject)
	FaceDirection(GetAnglesBetweenObjects("player", poolObject), true)
end

function RAF.FishPool()
	local poolObject = RAF.GetPoolObject()
	local poolDistance = GetDistanceBetweenObjects("player", poolObject)

	if poolDistance <= 25 then
		RAF.FacePool(poolObject)
		local bobberObject = RAF.BobberObject()

		if bobberObject == nil and not RAF.fishingLock then
			local randomNumber = math.abs(RAF.Random(25, 150, 1.2, 50, 1000) / 100)

			RAF.fishingLock = true

			C_Timer.After(randomNumber,
				function()
					if not RAF.fishingTimer._cancelled then
						CastSpellByName(RAF.SpellToName(131474))

						if RAF.Fetch("printstatus", false) == true then
							print("Fishing pool, initial cast in "..randomNumber.." seconds.")
						end
					end
				end,
			nil)

			C_Timer.After(randomNumber+2, function() RAF.fishingLock = false end, nil)

		elseif bobberObject then
			local bobberDistance = GetDistanceBetweenObjects(bobberObject, poolObject)

			if bobberDistance >= 3 and not RAF.fishingLock then
				local randomNumber = math.abs(RAF.Random(20, 50, 1.2, 50, 1000) / 100)
				RAF.fishingLock = true

				C_Timer.After(randomNumber,
					function()
						if not RAF.fishingTimer._cancelled then
							CastSpellByName(RAF.SpellToName(131474))

							if RAF.Fetch("printstatus", false) == true then
								print("Fishing pool, recast in "..randomNumber.." seconds.")
							end
						end
					end,
				nil)

				C_Timer.After(randomNumber+1, function() RAF.fishingLock = false end, nil)

			elseif bobberDistance < 3 then
				RAF.goFishState = "run"

			end
		end
	else
		RAF.goFishState = "run"

	end
end

function RAF.GetPoolObject()
	local pools = { }
	local objectCount = GetObjectCount()

	for i = 1, objectCount do
		local object = GetObjectWithIndex(i)
		local poolID = select(6, strsplit("-", ObjectGUID(object)))

		if poolID == "229072" -- Abyssal Gulper School
			or poolID == "229073" -- Blackwater Whiptail School
			or poolID == "229069" -- Blind Lake Sturgeon School
			or poolID == "229068" -- Fat Sleeper School
			or poolID == "243324" -- Felmouth Frenzy School
			or poolID == "243325" -- Felmouth Frenzy School
			or poolID == "243354" -- Felmouth Frenzy School
			or poolID == "229070" -- Fire Ammonite School
			or poolID == "229067" -- Jawless Skulker School
			or poolID == "236756" -- Oily Abyssal Gulper School
			or poolID == "237295" -- Oily Sea Scorpion School
			or poolID == "229071" -- Sea Scorpion School
		then
			local distance = GetDistanceBetweenObjects("player", object)

			pools[#pools+1] = {
				guid = ObjectGUID(object),
				distance = distance
			}

		end
	end

	local poolCount = table.getn(pools)
	if poolCount > 0 then
		local nearest = nil
		for i = 1, #pools do
			if not nearest then
				nearest = pools[i]
			end

			if pools[i].distance <= nearest.distance then
				nearest = pools[i]
			end
		end

		return GetObjectWithGUID(nearest.guid)
	else
		return
	end
end

















































--[[------------------------------------------------------------------------------------------------
WEAPON FUNCTIONS
------------------------------------------------------------------------------------------------]]--
function RAF.GetHelmet()
	RAF.helmet = GetInventoryItemLink("player", 1)
end

function RAF.GetWeapons()
	RAF.mainHandWeapon = GetInventoryItemLink("player", 16)
	RAF.offHandWeapon = GetInventoryItemLink("player", 17)
end

function RAF.GetWeaponEnchant()
	local _, _, _, mainHandEnchantID, _, _, _, _ = GetWeaponEnchantInfo()
	return mainHandEnchantID
end

function RAF.ReplaceHelmet()
	if RAF.helmet ~= nil then
		EquipItemByName(RAF.helmet)
	end
end

function RAF.ReplaceWeapons()
	if RAF.mainHandWeapon ~= nil then
		EquipItemByName(RAF.mainHandWeapon)
	end
	if RAF.offHandWeapon ~= nil then
		EquipItemByName(RAF.offHandWeapon)
	end
end


















































--[[------------------------------------------------------------------------------------------------
FISHING
------------------------------------------------------------------------------------------------]]--
RAF.fishingLock = false
RAF.lootingLock = false
function RAF.GoFish()


	------------------------------------------------------------------
	-- POLE --
	------------------------------------------------------------------
	if RAF.goFishState == "pole" then
		RAF.EquipPole()


	------------------------------------------------------------------
	-- LURE --
	------------------------------------------------------------------
	elseif RAF.goFishState == "lure" then
		RAF.UseLure()


	------------------------------------------------------------------
	-- APPLYING HAT LURE --
	------------------------------------------------------------------
	elseif RAF.goFishState == "applyhatlure" then
		RAF.ApplyHatLure()


	------------------------------------------------------------------
	-- APPLYING POLE LURE --
	------------------------------------------------------------------
	elseif RAF.goFishState == "applypolelure" then
		RAF.ApplyPoleLure()


	------------------------------------------------------------------
	-- APPLYING SPECIFIC LURE --
	------------------------------------------------------------------
	elseif RAF.goFishState == "applyspecificlure" then
		RAF.ApplySpecificLure()


	------------------------------------------------------------------
	-- HAT --
	------------------------------------------------------------------
	elseif RAF.goFishState == "hat" then
		RAF.EquipHat()


	------------------------------------------------------------------
	-- BAIT --
	------------------------------------------------------------------
	--elseif RAF.goFishState == "bait" then


	------------------------------------------------------------------
	-- BLADEBONE HOOK --
	------------------------------------------------------------------
	elseif RAF.goFishState == "bladebone" then
		RAF.UseBladeboneHook()


	------------------------------------------------------------------
	-- POOL --
	------------------------------------------------------------------
	elseif RAF.goFishState == "pool" then
		RAF.FishPool()


	------------------------------------------------------------------
	-- PAUSE --
	------------------------------------------------------------------
	elseif RAF.goFishState == "pause" then
		RAF.Debug("RAF.GoFish()1", "pause")
		RAF.fishingWindow.elements.togglebutton:SetText("PAUSED, PRESS TO START!")
		return


	------------------------------------------------------------------
	-- STOP
	------------------------------------------------------------------
	elseif RAF.goFishState == "stop" then
		RAF.Debug("RAF.GoFish()2", "stop")
		return


	------------------------------------------------------------------
	-- RUN
	------------------------------------------------------------------
	else

		--------------------------------------------------------------------------- Update Window --
		RAF.FishingWindowUpdate()


		---------------------------------------------------------------------------------- Combat --
		if ProbablyEngine.module.player.combat and RAF.goFishState == "run" then
			RAF.Debug("RAF.GoFish()4", "state changed to pause")
			RAF.goFishState = "pause"
			RAF.ReplaceWeapons()
			return
		end


		--------------------------------------------------------------------------------- Bobbing --
		local bobberObject = RAF.BobberObject()
		if bobberObject and RAF.goFishState == "run" then
			if RAF.BobberBobbing() == true then
				if not RAF.lootingLock then
					RAF.lootingLock = true
					local randomNumber = math.abs(RAF.Random(50, 300) / 100)

					C_Timer.After(randomNumber,
						function()
							ObjectInteract(bobberObject);
							if RAF.Fetch("printstatus", false) == true then
								print("Looted in "..randomNumber.." seconds.")
							end
						end,
					nil)

					C_Timer.After(randomNumber+1, function() RAF.lootingLock = false end, nil)
				end

				RAF.Debug("RAF.GoFish()5", "RAF.BobberBobbing() is true.")
				return
			end
		end


		---------------------------------------------------------------------- Casting/Channeling --
		if ProbablyEngine.module.player.casting == true and RAF.goFishState == "run" then
			RAF.Debug("RAF.GoFish()6", "player casting/channeling.")
			return
		end

		------------------------------------------------------------------------------------ Pole --
		if RAF.Fetch("poledropdown", "polenone") ~= "polenone"
			and not RAF.PoleAlreadyEquipped()
			and RAF.goFishState == "run"
		then
			RAF.Debug("run(polecheck)", "Pole not equiped. State changed to 'pole'")
			RAF.goFishState = "pole"

		end

		------------------------------------------------------------------------------------ Lure --
		if RAF.Fetch("luredropdown", "lurenone") ~= "lurenone"
			and RAF.Fetch("poledropdown", "polenone") ~= "polenone"
			and not RAF.LureAlreadyApplied()
		--if not RAF.LureAlreadyApplied()
			and RAF.goFishState == "run"
		then
			RAF.Debug("run(lurecheck)1", "Lure not applied. State changed to 'lure'")
			RAF.goFishState = "lure"
		else
			RAF.Debug("run(lurecheck)2", "shits false!")
		end

		------------------------------------------------------------------------------------- Hat --
		if RAF.Fetch("hatdropdown", "hatnone") ~= "hatnone"
			and not RAF.HatAlreadyEquipped()
			and RAF.goFishState == "run"
		then
			RAF.Debug("run(hatcheck)", "Hat not equiped. State changed to 'hat'")
			RAF.goFishState = "hat"

		end


		------------------------------------------------------------------------------------ Bait --
		--[[
		if not RAF.BaitAlreadyApplied() and RAF.goFishState == "run" then
			RAF.goFishState = "bait"

		end
		]]

		-------------------------------------------------------------------------- Bladebone Hook --
		if RAF.Fetch("bladebone", false) == true
			and not RAF.BladeboneHookApplied()
			and RAF.goFishState == "run"
		then
			RAF.Debug("run(bladebonecheck)", "Bladebone Hook not applied. State changed to 'bladebone'")
			RAF.goFishState = "bladebone"

		end


		------------------------------------------------------------------------------------ Pool --
		if RAF.Fetch("pools", false) == true
			and RAF.GetPoolObject() ~= nil
			and RAF.goFishState == "run"
			and not RAF.fishingLock
		then
			RAF.Debug("run(poolcheck)", "Pool check found at least one pool nearby. State changed to 'pool'")
			RAF.goFishState = "pool"

		end


		------------------------------------------------------------------------------------ Fish --
		if not RAF.fishingLock and RAF.goFishState == "run" then
			local randomNumber = math.abs(RAF.Random(50, 300, 1.2, 50, 1000) / 100)

			RAF.fishingLock = true

			C_Timer.After(randomNumber,
				function()
					if not RAF.fishingTimer._cancelled then
						CastSpellByName(RAF.SpellToName(131474))

						if RAF.Fetch("printstatus", false) == true then
							print("Fishing delayed for "..randomNumber.." seconds.")
						end
					end
				end,
			nil)

			C_Timer.After(randomNumber+2, function() RAF.fishingLock = false end, nil)
		end
	end
end


















































--[[------------------------------------------------------------------------------------------------
CONFIGURATON WINDOW
------------------------------------------------------------------------------------------------]]--


----------------------------------------------------------------------------------- Bait Dropdown --
function RAF.BuildBaitDropdown()
	-- Build the primary table
	local elementTable = {
		type = "dropdown",
		key = "baitdropdown",
		text = "Use Bait",
		default = "baitnone"
	}

	-- Build the element list table
	local baitDropdownTable = { }

	table.insert(baitDropdownTable, { key = "baitnone", text = "None" })
	table.insert(baitDropdownTable, { key = "baitrandom", text = "Random" })
	table.insert(baitDropdownTable, { key = "baitzone", text = "Zone Specific" })

	for k, v in pairs(RAF.bait) do
		table.insert(baitDropdownTable, { key = k, text = v.name })
	end
	-- Put the table into list = {}
	elementTable.list = baitDropdownTable

	return elementTable
end


------------------------------------------------------------------------------------ Hat Dropdown --
function RAF.BuildHatDropdown()
	-- Build current table
	RAF.CurrentHats()

	-- Build the primary table
	local elementTable = {
		type = "dropdown",
		key = "hatdropdown",
		text = "Use Hat",
		default = "hatnone"
	}

	-- Build the element list table
	local hatDropdownTable = { }

	table.insert(hatDropdownTable, { key = "hatnone", text = "None" })
	table.insert(hatDropdownTable, { key = "hatbest", text = "Best Hat" })

	for i, v in ipairs(RAF.currentHats) do
		table.insert(hatDropdownTable, { key = v.id, text = v.name })
	end
	-- Put the table into list = {}
	elementTable.list = hatDropdownTable

	return elementTable
end


----------------------------------------------------------------------------------- Lure Dropdown --
function RAF.BuildLureDropdown()
	-- Build current table
	RAF.CurrentLures()

	-- Build the primary table
	local elementTable = {
		type = "dropdown",
		key = "luredropdown",
		text = "Use Lure",
		default = "lurenone"
	}

	-- Build the element list table
	local lureDropdownTable = { }

	table.insert(lureDropdownTable, { key = "lurenone", text = "None" })
	table.insert(lureDropdownTable, { key = "lurebest", text = "Best Lure" })

	for i, v in ipairs(RAF.currentLures) do
		table.insert(lureDropdownTable, { key = v.id, text = v.name })
	end
	-- Put the table into list = {}
	elementTable.list = lureDropdownTable

	return elementTable
end


----------------------------------------------------------------------------------- Pole Dropdown --
function RAF.BuildPoleDropdown()
	-- Build current table
	RAF.CurrentPoles()

	-- Build the primary table
	local elementTable = {
		type = "dropdown",
		key = "poledropdown",
		text = "Use Pole",
		default = "polenone"
	}

	-- Build the element list table
	local poleDropdownTable = { }

	table.insert(poleDropdownTable, { key = "polenone", text = "None" })
	table.insert(poleDropdownTable, { key = "polebest", text = "Best Pole" })

	for i, v in ipairs(RAF.currentPoles) do
		table.insert(poleDropdownTable, { key = v.id, text = v.name })
	end
	-- Put the table into list = {}
	elementTable.list = poleDropdownTable

	return elementTable
end


------------------------------------------------------------------------------ Main Config Window --
RAF.goFishState = "stop"
RAF.fishingWindow = { }
function RAF.FishingWindowBuild()
	RAF.fishingWindow = ProbablyEngine.interface.buildGUI({
		key = "rafconfig",
		title = 'RotFishing',
		subtitle = 'Fishing Plugin',
		width = 175,
		height = 300,
		resize = true,
		color = "4e7300",
		config = {
			{ type = "button", key = "togglebutton", text = "Start Fishing", width = 150, height = 20, push = 2, callback = function(self, button)
				-- Note: Buttons always start with the "text = ''" key from above. Once clicked the
				-- function below is called.
				if RAF.goFishState == "stop" then
					self:SetText("Stop Fishing")
					RAF.goFishState = "run"
					RAF.GetHelmet()
					RAF.GetWeapons()
					RAF.FishingTimer()
					RAF.bladeboneHookLock = false
					RAF.applyLureLock = false
					RAF.equipHatLock = false
					RAF.equipPoleLock = false

				elseif RAF.goFishState == "pause" then
					RAF.fishingWindow.elements.togglebutton:SetText("Stop Fishing")
					RAF.goFishState = "run"

				else
					self:SetText("Start Fishing")
					RAF.goFishState = "stop"
					RAF.fishingTimer:Cancel()
					RAF.equipPoleInProgress = false
					SpellStopCasting()
					RAF.ReplaceWeapons()
					RAF.ReplaceHelmet()

				end
			end },
			{ type = 'rule', },

			RAF.BuildBaitDropdown(),
			RAF.BuildHatDropdown(),
			RAF.BuildLureDropdown(),
			RAF.BuildPoleDropdown(),
			{ type = 'rule', },

			{ type = 'checkbox', key = 'pools', text = 'Fish pool if near?', default = false, },
			{ type = 'checkbox', key = 'bladebone', text = 'Use Bladebone Hook?', default = false, },
			{ type = 'checkbox', key = 'printstatus', text = 'Print status messages?', default = false, },
			{ type = 'checkspin', key = 'logout', text = 'Logout after X minutes:', min = 1, max = 600, step = 5, default = 60, },
			{ type = 'rule', },

			{ type = 'header', text = 'Debug', },
			{ type = 'checkspin', key = 'debug', text = 'Debug, set throttle.', min = 0, max = 5, step = .1, default = 0,
			  desc = "Type '/raf2 debug all' to print out all debug messages. Can also filter with '/raf2 debug X' where X is the name of a function.", },
		}
	})
	RAF.fishingWindow.parent:Hide()
end
-- Build the configuration window to set the keys
RAF.FishingWindowBuild()

-- Show Window
function RAF.FishingWindowShow()
	if not RAF.fishingWindow.parent:IsVisible() then
		RAF.FishingWindowBuild()
		RAF.fishingWindow.parent:Show()

	else
		RAF.fishingWindow.parent:Hide()

	end
end

















































----------------------------------------------------------------------------------------------------
-- Update Window
----------------------------------------------------------------------------------------------------
function RAF.FishingWindowUpdate()
	RAF.Debug("RAF.FishingWindowUpdate()1", "entry!")

end


















































-- Slash Command
--[[------------------------------------------------------------------------------------------------
Slash Command
------------------------------------------------------------------------------------------------]]--
SLASH_RAF1 = '/raf2'
function SlashHandler(msg, editbox)
	local command, moretext = msg:match("^(%S*)%s*(.-)$")
	command = string.lower(command)

	if msg == "" then
		RAF.FishingWindowShow()

	elseif command == "debug" then
		if moretext ~= "" then
			RAF.debugThisThing = moretext
			print("Debugging: " .. tostring(RAF.debugThisThing))
		else
			RAF.debugThisThing = nil
			print("Debugging: Off")
		end

	else

	end

end
SlashCmdList["RAF"] = SlashHandler


















































--[[------------------------------------------------------------------------------------------------
TIMER
------------------------------------------------------------------------------------------------]]--
function RAF.FishingTimer()
	RAF.goFishState = "pole"
	RAF.fishingTimer = C_Timer.NewTicker(0.25, function() RAF.GoFish() end, nil)
end