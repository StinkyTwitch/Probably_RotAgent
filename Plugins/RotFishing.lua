--[[------------------------------------------------------------------------------------------------

RotFishing.lua v0.2

RA (Rotation Agent) License
This work is licensed under the Creative Commons Attribution-NonCommercial 4.0 International
License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/.

--------------------------------------------------------------------------------------------------]]
_G["RAF"] = _G["RAF"] or { }

--[[------------------------------------------------------------------------------------------------
Local Variables
------------------------------------------------------------------------------------------------]]--
local bobberDisplayID = 668
local objectBobbing = 0x1E0
local objectCreatorOffset = 0x30
local objectOffset = 0x40





--[[------------------------------------------------------------------------------------------------
General Functions
------------------------------------------------------------------------------------------------]]--
local function Eval(condition, spell)
	if not condition then return false end
	return ProbablyEngine.dsl.parse(condition, spell or '')
end

local function Fetch(key, default)
	return ProbablyEngine.interface.fetchKey('rotfishing_config', key, default)
end

-- http://opentibia.net/topic/109869-code-for-generating-random-numbers-in-normal-distribution/
local function InternalRandom()
  local x1, x2, w, y1, y2
  repeat
	 x1 = 2 * math.random() - 1
	 x2 = 2 * math.random() - 1
	 w = x1 * x1 + x2 * x2
  until (w < 1)

  w = math.sqrt((-2 * math.log(w)) / w)
  y1 = x1 * w
  y2 = x2 * w
  return y1, y2
end

local function PickupPlayerItem(itemID)
	local currentItemID = 0

	-- Look through the player inventory.
	for i = INVSLOT_FIRST_EQUIPPED, INVSLOT_LAST_EQUIPPED do
		currentItemID = GetInventoryItemID("player", i)

		if currentItemID == itemID then
			PickupInventoryItem(i)
			return true
		end
	end

	-- Look through the players bags.
	for i = 1, NUM_BAG_SLOTS do
		for j = 1, GetContainerNumSlots(i) do
			currentItemID = GetContainerItemID(i, j)

			if currentItemID == itemID then
				PickupContainerItem(i, j)
				return true
			end
		end
	end
end

local function Random(min, max, variance, strictMin, strictMax)
	local average = (min + max) / 2

	if variance == nil then
		variance = 2.4
	end

	local escala = (max - average) / variance
	local x = escala * InternalRandom() + average

	if strictMin ~= nil then
		x = math.max(x, strictMin)
	end

	if strictMax ~= nil then
		x = math.min(x, strictMax)
	end

	return(x)
end

local function SpellToName(spellID)
	return tostring(select(1,GetSpellInfo(spellID)))
end

local function GetObjectDisplayID(object)
  return ObjectDescriptor(object, objectOffset, Types.UInt)
end

local function GetObjectGUID(object)
  return tonumber(ObjectDescriptor(object, 0, Types.ULong))
end

local function IsObjectCreatedBy(owner, object)
  return tonumber(ObjectDescriptor(object, objectCreatorOffset, Types.ULong)) == GetObjectGUID(owner)
end





--[[------------------------------------------------------------------------------------------------
Fishing Functions
------------------------------------------------------------------------------------------------]]--
local equipmentSets = { }
local function GetEquipmentSets()
	equipmentSets = { }
	local setsCount = GetNumEquipmentSets()

	if setsCount > 0 then
		for i = 1, setsCount do
			local setName,_,_ = GetEquipmentSetInfo(i)
			equipmentSets[#equipmentSets+1] = { name = setName }
		end
	end
end


local function EquipEquipmentSet()
	if Fetch("equipmentsets", true) == "setnone" then
		return
	end

	if #equipmentSets > 0 then
		for i, v in ipairs(equipmentSets) do
			local key = tostring("set"..i)

			if Fetch("equipmentsets", true) == key then
				UseEquipmentSet(v.name)
			end
		end
	end
end





--[[------------------------------------------------------------------------------------------------
FISHING HATS FUNCTIONS
------------------------------------------------------------------------------------------------]]--
local fishingHats = { }
local function GetFishingHats()
	fishingHats = { }

	local fishingHatsTable = {
		[ 19972] = "19972",  -- Lucky Fishing Hat
		[ 33820] = "33820",  -- Weather-Beaten Fishing Hat
		[ 88710] = "88710",  -- Nat's Hat
		[ 93732] = "93732",  -- Darkmoon Fishing Cap
		[117405] = "117405", -- Nat's Drinking Hat
		[118380] = "118380", -- Hightfish Cap
		[118393] = "118393", -- Tentacled Hat
	}

	local fishingHatFound = false
	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID then
				local itemLink = GetContainerItemLink(bag, slot)
				local itemName = select(1, GetItemInfo(itemLink))

				if fishingHatsTable[tonumber(itemID)] ~= nil  then
					for i, v in ipairs(fishingHats) do
						if v.id == itemID then
							fishingHatFound = true
						end
					end

					if not fishingHatFound then
						fishingHats[#fishingHats+1] = { name = itemName, id = itemID }
					end
				end
			end
		end
	end
end

local function FishingHatEquiped()
	local fishingHatID = GetInventoryItemID("player", INVSLOT_HEAD)

	for k,v in ipairs(fishingHats) do
		if v.id == fishingHatID then
			return true
		end
	end
	return false
end

local function EquipFishingHat()
	if FishingHatEquiped() then
		return
	end

	if #fishingHats > 0 then
		for i, v in ipairs(fishingHats) do
			if Fetch("fishinghats", false) ~= "hatsnone" then
				local key = tostring("hat"..i)

				if Fetch("fishinghats", true) == key then
					PickupPlayerItem(v.id)
					AutoEquipCursorItem()
				end
			end
		end
	end
end





--[[------------------------------------------------------------------------------------------------
FISHING POLES FUNCTIONS
------------------------------------------------------------------------------------------------]]--
local fishingPoles = { }
local function GetFishingPoles()
	fishingPoles = { }

	local fishingPolesTable = {
		[ 44050] = "44050",  -- Mastercraft Kalu'ak Fishing Pole
		[ 25978] = "25978",  -- Seth's Graphite Fishing Pole
		[ 19022] = "19022",  -- Nat Pagle's Extreme Angler FC-5000
		[  6367] = "6367",   -- Big Iron Fishing Pole
		[  6366] = "6366",   -- Darkwood Fishing Pole
		[120163] = "120163", -- Thruk's Fishing Rod
		[ 45858] = "45858",  -- Nat's Lucky Fishing Pole
		[ 19970] = "19970",  -- Arcanite Fishing Pole
		[ 84661] = "84661",  -- Dragon Fishing Pole
		[ 45991] = "45991",  -- Bone Fishing Pole
		[118381] = "118381", -- Ephemeral Fishing Pole
		[ 45992] = "45992",  -- Jeweled Fishing Pole
		[ 46337] = "46337",  -- Staats' Fishing Pole
		[ 12225] = "12225",  -- Blump Family Fishing Pole
		[  6365] = "6365",   -- Strong Fishing Pole
		[116826] = "116826", -- Draenic Fishing Pole
		[ 84660] = "84660",  -- Pandaren Fishing Pole
		[116825] = "116825", -- Savage Fishing Pole
		[  6256] = "6256",   -- Fishing Pole
	}

	local poleFound = false
	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID then
				local itemLink = GetContainerItemLink(bag, slot)
				local itemName = select(1, GetItemInfo(itemLink))

				if fishingPolesTable[tonumber(itemID)] ~= nil  then
					for i, v in ipairs(fishingPoles) do
						if v.id == itemID then
							poleFound = true
						end
					end

					if not poleFound then
						fishingPoles[#fishingPoles+1] = { name = itemName, id = itemID }
					end
				end
			end
		end
	end
end

local function FishingPoleEquiped()
	local fishingPoleID = GetInventoryItemID("player", INVSLOT_MAINHAND)

	for k,v in ipairs(fishingPoles) do
		if v.id == fishingPoleID then
			return true
		end
	end
	return false
end

local function EquipFishingPole()
	if FishingPoleEquiped() then
		return
	end

	if #fishingPoles > 0 then
		for i, v in ipairs(fishingPoles) do
			if Fetch("fishingpoles", false) ~= "polesnone" then
				local key = tostring("pole"..i)

				if Fetch("fishingpoles", true) == key then
					PickupPlayerItem(v.id)
					AutoEquipCursorItem()
				end
			end
		end
	end
end





--[[------------------------------------------------------------------------------------------------
BOBBER FUNCTIONS
------------------------------------------------------------------------------------------------]]--
local function GetBobber()
  local bobber = nil
  local objectCount = GetObjectCount()

  for i = 1, objectCount do
	local object = GetObjectWithIndex(i)

	if GetObjectDisplayID(object) == bobberDisplayID then
	  if IsObjectCreatedBy("player", object) then
		bobber = object
		break
	  end
	end
  end

  return bobber
end


local function FacePool(poolObject)
	local facing = ObjectFacing("Player")
	local x, y, z = ObjectPosition(poolObject)
	FaceDirection(GetAnglesBetweenObjects("player", poolObject), true)
end


local function GetPool()
	local pool = { }
	local objectCount = GetObjectCount()

	for i = 1, objectCount do
		local object = GetObjectWithIndex(i)
		local poolID = select(6, strsplit("-", ObjectGUID(object)))

		if poolID == "229072" -- Abyssal Gulper School
			or poolID == "229073" -- Blackwater Whiptail School
			or poolID == "229069" -- Blind Lake Sturgeon School
			or poolID == "229068" -- Fat Sleeper School
			or poolID == "243324" -- Felmouth Frenzy School
			or poolID == "243325" -- Felmouth Frenzy School
			or poolID == "243354" -- Felmouth Frenzy School
			or poolID == "229070" -- Fire Ammonite School
			or poolID == "229067" -- Jawless Skulker School
			or poolID == "236756" -- Oily Abyssal Gulper School
			or poolID == "237295" -- Oily Sea Scorpion School
			or poolID == "229071" -- Sea Scorpion School
		then
			local distance = GetDistanceBetweenObjects("player", object)

			pool[#pool+1] = {
				guid = ObjectGUID(object),
				distance = distance
			}

		end
	end

	local poolCount = table.getn(pool)
	if poolCount > 0 then
		local nearest = nil
		for i = 1, #pool do
			if not nearest then
				nearest = pool[i]
			end

			if pool[i].distance <= nearest.distance then
				nearest = pool[i]
			end
		end

		return GetObjectWithGUID(nearest.guid)
	else
		return
	end
end


local function IsBobbing()
	local bobbing = ObjectField(GetBobber(),objectBobbing, Types.Short)
	if bobbing == 1 then
		return true
	else
		return false
	end
end


local function BaitFound(baitID)
	baitID = tonumber(baitID)
	local baitFound = false

	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)
			local itemIDType = type(itemID)

			if itemID == baitID then
				print("Bait found " .. baitID .. "")
				return true
			end
		end
	end
	return false
end


local function UseBait()
	if Fetch("bait", true) == "baitnone" then
		return
	end

	local baitBuff = false
	local baitBuffTable = {
		[158038] = "158038", -- Abyssal Gulper Eel Bait
		[158039] = "158039", -- Blackwater Whiptail Bait
		[158035] = "158035", -- Blind Lake Sturgeon Bait
		[158034] = "158034", -- Fat Sleeper Bait
		[158036] = "158036", -- Fire Ammonite Bait
		[158031] = "158031", -- Jawless Skulker Bait
		[158037] = "158037", -- Sea Scorpion Bait
		[188904] = "188904", -- Felmouth Frenzy Bait
	}
	local baitTable = {
		[110293] = "110293", -- Abyssal Gulper Eel Bait
		[110294] = "110294", -- Blackwater Whiptail Bait
		[110290] = "110290", -- Blind Lake Sturgeon Bait
		[110289] = "110289", -- Fat Sleeper Bait
		[110291] = "110291", -- Fire Ammonite Bait
		[110274] = "110274", -- Jawless Skulker Bait
		[110292] = "110292", -- Sea Scorpion Bait
		[128229] = "128229", -- Felmouth Frenzy Bait
	}
	local baitTableIndex = {
		[1] = "110293", -- Abyssal Gulper Eel Bait
		[2] = "110294", -- Blackwater Whiptail Bait
		[3] = "110290", -- Blind Lake Sturgeon Bait
		[4] = "110289", -- Fat Sleeper Bait
		[5] = "110291", -- Fire Ammonite Bait
		[6] = "110274", -- Jawless Skulker Bait
		[7] = "110292", -- Sea Scorpion Bait
	}

	for k,v in pairs(baitBuffTable) do
		local buff = GetSpellInfo(v)
		if UnitBuff("player", buff) then
			baitBuff = true
		end
	end

	if not baitBuff and Fetch("bait", true) == "baitrandom" then
		local baitRandom = math.random(1,7)
		local bait = tonumber(baitTableIndex[baitRandom])

		if BaitFound(baitTable[bait]) then
			UseItemByName(bait)
		end
	elseif not baitBuff and Fetch("bait", true) == "baitzonespecific" then
		local currentZone = GetCurrentMapAreaID()

		if currentZone == 941 then                      -- "Frostfire Ridge"
			if BaitFound(baitTable[110291]) then
				UseItemByName(110291)
			end
		elseif currentZone == 949 then                  -- "Gorgrond"
			if BaitFound(baitTable[110274]) then
				UseItemByName(110274)
			end
		elseif currentZone == 950 then                  -- "Nagrand"
			if BaitFound(baitTable[110289]) then
				UseItemByName(110289)
			end
		elseif currentZone == 947 then                  -- "Shadowmoon Valley"
			if BaitFound(baitTable[110290]) then
				UseItemByName(110290)
			end
		elseif currentZone == 948 then                  -- "Spires of Arak"
			if BaitFound(baitTable[110293]) then
				UseItemByName(110293)
			end
		elseif currentZone == 946 then                  -- "Talador"
			if BaitFound(baitTable[110294]) then
				UseItemByName(110294)
			end
		elseif currentZone == 945 then                  -- "Tanaan Jungle"
			if BaitFound(baitTable[128229]) then
				UseItemByName(128229)
			end
		end

	elseif not baitBuff and Fetch("bait", true) == "abyssalbait" then
		if BaitFound(baitTable[110293]) then
			UseItemByName(110293)
		end
	elseif not baitBuff and Fetch("bait", true) == "blackwaterbait" then
		if BaitFound(baitTable[110294]) then
			UseItemByName(110294)
		end
	elseif not baitBuff and Fetch("bait", true) == "blindlakebait" then
		if BaitFound(baitTable[110290]) then
			UseItemByName(110290)
		end
	elseif not baitBuff and Fetch("bait", true) == "fatbait" then
		if BaitFound(baitTable[110289]) then
			UseItemByName(110289)
		end
	elseif not baitBuff and Fetch("bait", true) == "firebait" then
		if BaitFound(baitTable[110291]) then
			UseItemByName(110291)
		end
	elseif not baitBuff and Fetch("bait", true) == "felmouthbait" then
		if BaitFound(baitTable[128229]) then
			UseItemByName(128229)
		end
	elseif not baitBuff and Fetch("bait", true) == "jawlessbait" then
		if BaitFound(baitTable[110274]) then
			UseItemByName(110274)
		end
	elseif not baitBuff and Fetch("bait", true) == "seabait" then
		if BaitFound(baitTable[110292]) then
			UseItemByName(110292)
		end
	else
		return
	end
end


local fishingLures = { }
local function GetLures()
	fishingLures = { }

	local fishingLuresTable = {
		[  6529] = "6529",   -- Shiny Bauble
		[  6530] = "6530",   -- Nightcrawlers
		[  6532] = "6532",   -- Bright Baubles
		[  6533] = "6533",   -- Aquadynamic Fish Attractor
		[  6811] = "6811",   -- Aquadynamic Fish Lens
		[  7307] = "7307",   -- Flesh Eating Worm
		[ 34861] = "34861",  -- Sharpened Fish Hook
		[ 46006] = "46006",  -- Glow Worm
		[ 62673] = "62673",  -- Feathered Lure
		[ 67404] = "67404",  -- Glass Fishing Bobber
		[ 68049] = "68049",  -- Heat-Treated Spinning Lure
		[118391] = "118391", -- Worm Supreme
		[124674] = "124674", -- Day-Old Darkmoon Doughnut
	}

	local lureFound = false
	for bag = 0,4 do
		for slot = 1,GetContainerNumSlots(bag) do
			local itemID = GetContainerItemID(bag, slot)

			if itemID then
				local itemLink = GetContainerItemLink(bag, slot)
				local itemName = select(1, GetItemInfo(itemLink))

				if fishingLuresTable[tonumber(itemID)] ~= nil  then
					for i, v in ipairs(fishingLures) do
						if v.id == itemID then
							lureFound = true
						end
					end

					if not lureFound then
						fishingLures[#fishingLures+1] = { name = itemName, id = itemID }
					end
				end
			end
		end
	end
end


local lockUseLures = false
local function LockUseLures()
	local fishingPoleEquiped = IsEquippedItemType("Fishing Poles")
	local fishingPoleEnchanted = GetWeaponEnchantInfo()

	if fishingPoleEquiped and fishingPoleEnchanted then
		lockUseLures = false
	end
end


local function UseLures()
	LockUseLures()
	if lockUseLures then
		return
	end

	if #fishingLures > 0 then
		for i, v in ipairs(fishingLures) do
			if Fetch("fishinglures", false) ~= "luresnone" then
				local key = tostring("lure"..i)

				if Fetch("fishinglures", true) == key then
					local fishingPoleEquiped = IsEquippedItemType("Fishing Poles")
					local fishingPoleEnchanted = GetWeaponEnchantInfo()

					if fishingPoleEquiped and not fishingPoleEnchanted then
						lockUseLures = true
						UseItemByName(v.id)
					end
				end
			end
		end
	end
end


local alreadyFishing = false
local alreadyLooting = false
local function GoFish()
	if ProbablyEngine.module.player.combat then
		SpellStopCasting()
		EquipEquipmentSet()
		rotFishingToggle = false
		RAF.rotFishingWindow.elements.togglebutton:SetText("Start Fishing")
		rotFishingTimer:Cancel()
		return
	elseif UnitCastingInfo("player") then
		return
	else
		EquipFishingHat()
		EquipFishingPole()
		UseBait()
		UseLures()

		local bobberObject = GetBobber()
		local poolObject = GetPool()

		local printMessages = false
		if Fetch("print", false) == true then
			printMessages = true
		end

		if bobberObject then
			if IsBobbing() == true then
				local randomNumber = math.abs(Random(50, 300) / 100)

				if not alreadyLooting then
					alreadyLooting = true
					C_Timer.After(randomNumber, function() ObjectInteract(bobberObject); if printMessages then print("Looted in "..randomNumber.."s") end end, nil)
					C_Timer.After(randomNumber+1, function() alreadyLooting = false end, nil)
				end
			elseif Fetch("pools", false) == true and not alreadyFishing and poolObject ~= nil then
				local bobberDistance = GetDistanceBetweenObjects(bobberObject, poolObject)
				local poolDistance = GetDistanceBetweenObjects("player", poolObject)

				if poolDistance <= 25 then
					FacePool(poolObject)

					if bobberDistance >= 3.5 then
						local randomNumber = math.abs(Random(10, 100) / 100)
						alreadyFishing = true
						C_Timer.After(randomNumber, function() CastSpellByName(SpellToName(131474)); if printMessages then print("Fishing delayed for "..randomNumber.."s") end end, nil)
						C_Timer.After(randomNumber+2, function() alreadyFishing = false end, nil)
					end
				end
			end
		else
			if not alreadyFishing then
				local randomNumber = math.abs(Random(50, 300, 1.2, 50, 1000) / 100)
				alreadyFishing = true
				C_Timer.After(randomNumber, function() CastSpellByName(SpellToName(131474)); if printMessages then print("Fishing delayed for "..randomNumber.."s") end end, nil)
				C_Timer.After(randomNumber+2, function() alreadyFishing = false end, nil)
			end
		end
	end
end





--[[------------------------------------------------------------------------------------------------
Timer
------------------------------------------------------------------------------------------------]]--
local function RotFishingTimer()
	rotFishingTimer = C_Timer.NewTicker(0.25, function() GoFish() end, nil)
end





--[[------------------------------------------------------------------------------------------------
Configuration Functions
------------------------------------------------------------------------------------------------]]--
local function BuildEquipmentSetDropdown()
	GetEquipmentSets()
	-- build the primary table
	local elementTable = {
		type = 'dropdown',
		key = 'equipmentsets',
		text = 'Combat Set:',
		width = 150, height = 20, push = 2,
		default = 'setnone'
	}

	-- build the list table
	local equipmentSetsTabel = { }

	table.insert(equipmentSetsTabel, {  key = 'setnone', text = 'None' })

	if #equipmentSets > 0 then
		for i, v in ipairs(equipmentSets) do
			table.insert(equipmentSetsTabel, {  key = 'set' .. i, text = v.name })
		end
	end

	elementTable.list = equipmentSetsTabel

	return elementTable
end


local function BuildFishingHatDropdown()
	GetFishingHats()
	-- build the primary table
	local elementTable = {
		type = 'dropdown',
		key = 'fishinghats',
		text = 'Fishing Hat:',
		width = 150, height = 20, push = 2,
		default = 'hatsnone'
	}

	-- build the element list table
	local fishingHatsTabel = { }

	table.insert(fishingHatsTabel, {  key = 'hatsnone', text = 'None' })

	if #fishingHats > 0 then
		for i, v in ipairs(fishingHats) do
			table.insert(fishingHatsTabel, {  key = 'hat' .. i, text = v.name })
		end
	end

	elementTable.list = fishingHatsTabel

	return elementTable
end


local function BuildFishingPoleDropdown()
	GetFishingPoles()
	-- build the primary table
	local elementTable = {
		type = 'dropdown',
		key = 'fishingpoles',
		text = 'Fishing Pole:',
		width = 150, height = 20, push = 2,
		default = 'polesnone'
	}

	-- build the element list table
	local fishingPolesTabel = { }

	table.insert(fishingPolesTabel, {  key = 'polesnone', text = 'None' })

	if #fishingPoles > 0 then
		for i, v in ipairs(fishingPoles) do
			table.insert(fishingPolesTabel, {  key = 'pole' .. i, text = v.name })
		end
	end

	elementTable.list = fishingPolesTabel

	return elementTable
end


local function BuildFishingLuresDropdown()
	GetLures()
	-- build the primary table
	local elementTable = {
		type = 'dropdown',
		key = 'fishinglures',
		text = 'Fishing Lure:',
		width = 150, height = 20, push = 2,
		default = 'luresnone'
	}

	-- build the element list table
	local fishingLureTable = { }

	table.insert(fishingLureTable, {  key = 'luresnone', text = 'None' })

	if #fishingLures > 0 then
		for i, v in ipairs(fishingLures) do
			table.insert(fishingLureTable, {  key = 'lure' .. i, text = v.name })
		end
	end

	elementTable.list = fishingLureTable

	return elementTable
end


local rotFishingToggle = false
local rotFishingWindow = { }
local function RotFishingWindowBuild()
	RAF.rotFishingWindow = ProbablyEngine.interface.buildGUI({
		key = "rotfishing_config",
		title = 'RotFishing',
		subtitle = 'Fishing Plugin',
		width = 180,
		height = 300,
		resize = true,
		color = "4e7300",
		config = {
			{ type = "button", key = "togglebutton", text = "Start Fishing", width = 150, height = 20, push = 2, callback = function(self, button)
				if not rotFishingToggle then
					self:SetText("Stop Fishing")
					rotFishingToggle = true
					RotFishingTimer()
				else
					self:SetText("Start Fishing")
					rotFishingToggle = false
					rotFishingTimer:Cancel()
					SpellStopCasting()
					EquipEquipmentSet()
				end
			end},
			{ type = 'rule', },
			{ type = 'dropdown', key = 'bait', text = 'Use Bait:', list = {
				{ key = 'baitnone', text = "None",  },
				{ key = 'baitrandom', text = "Random",  },
				{ key = 'baitzonespecific', text = "Zone Specific",  },
				{ key = 'abyssalbait', text = "Abyssal Gulper Eel Bait",  },
				{ key = 'blackwaterbait', text = "Blackwater Whiptail Bait",  },
				{ key = 'blindlakebait', text = "Blind Lake Sturgeon Bait",  },
				{ key = 'fatbait', text = "Fat Sleeper Bait",  },
				{ key = 'firebait', text = "Fire Ammonite Bait",  },
				{ key = 'felmouthbait', text = "Felmouth Frenzy Bait",  },
				{ key = 'jawlessbait', text = "Jawless Skulker Bait",  },
				{ key = 'seabait', text = "Sea Scorpion Bait",  },
			}, default = 'baitnone', },
			BuildFishingLuresDropdown(),
			BuildFishingHatDropdown(),
			BuildFishingPoleDropdown(),
			BuildEquipmentSetDropdown(),
			{ type = 'rule', },
			{ type = 'checkbox', key = 'pools', text = 'Fish pools if near.', default = false, },
			{ type = 'checkbox', key = 'print', text = 'Print status messages?', default = false, },
		}
	})
	RAF.rotFishingWindow.parent:Hide()
end


rotFishingWindowShow = false
local function RotFishingWindowShow()
	RotFishingWindowBuild()
	rotFishingWindowShow = not rotFishingWindowShow

	if rotFishingWindowShow then
		RAF.rotFishingWindow.parent:Show()
	else
		RAF.rotFishingWindow.parent:Hide()
	end
end





--[[------------------------------------------------------------------------------------------------
Slash Command
------------------------------------------------------------------------------------------------]]--
SLASH_ROTFISHING1, SLASH_ROTFISHING2 = '/raf', '/rafish'
local function SlashHandler(msg, editbox)
	RotFishingWindowShow()
end
SlashCmdList["ROTFISHING"] = SlashHandler